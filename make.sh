#!/bin/bash

if [ ! -f config.sh ]; then
	echo 'create config.sh file with the following content:'
	echo 'GUROBIPATH="/path/to/gurobi/main/folder"'
	echo '(without trailing slash)'
	echo '(the dir containing bin/, docs/, examples/ etc.)'
	exit 1;
fi
source ./config.sh

find . -iname '*.class' -exec rm '{}' ';'

javac -cp lib/JavaPlot.jar:lib/commons-collections-3.2.1.jar:lib/commons-configuration-1.10.jar:lib/commons-jxpath-1.3.jar:lib/commons-lang-2.6.jar:lib/commons-logging-1.1.3.jar:$GUROBIPATH/lib/gurobi.jar:src src/de/abbaddie/bommel/*.java src/de/abbaddie/bommel/solver/*.java src/de/abbaddie/bommel/data/*.java src/de/abbaddie/bommel/improver/*.java  src/de/abbaddie/bommel/util/*.java
