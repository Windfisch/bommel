set termoption enhanced
f(x) = a*x+b

b=0.00001
a=0.1

set log x
# set log y

fit f(x) 'benchmark.data' using 4:5 via a,b
plot 'benchmark.data' using 4:5 with points title 'data points'  ,     f(x) with lines title 'fnord'

pause -1

