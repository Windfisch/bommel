#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys, os

class Parser:
	class ParsingError(Exception):
		def __init__(self, line, keyword, lineNr):
			self.line = line
			self.keyword = keyword
			self.lineNr = lineNr
			
		def __str__(self):
			return "Erwartetes Schlüsselwort: " + repr(self.keyword) + ", gefunden: " + repr(self.line) + ", Zeile: " + repr(self.lineNr)
	
	class IntegerError(Exception):
		def __init__(self, line, keyword, lineNr):
			self.line = line
			self.keyword = keyword
			self.lineNr = lineNr
		
		def __str__(self):
			return "Der Wert von '" + repr(self.keyword) + " muss eine ganze Zahl sein, gefunden: " + repr(self.line) + ", Zeile: " + repr(self.lineNr)
	
	class FloatError(Exception):
		def __init__(self, line, keyword, lineNr):
			self.line = line
			self.keyword = keyword
			self.lineNr = lineNr
		
		def __str__(self):
			return "Der Wert von '" + repr(self.keyword) + " muss eine Zahl sein, gefunden: " + repr(self.line) + ", Zeile: " + repr(self.lineNr)
	
	class DoubleAssignmentError(Exception):
		def __init__(self, trainID, trackID):
			self.trainID = trainID
			self.trackID = trackID
		
		def __str__(self):
			return "Dem Zug Nummer '" + repr(self.trainID) + "' auf Strecke Nummer '" + repr(self.trackID) + "' wurde bereits eine Abfahrtszeit zugewiesen"
	
	def checkKeyword(self, line, keyword, lineNr):
		if line != keyword:
			raise self.ParsingError(line, keyword, lineNr)
		return
	
	def checkInt(self, line, keyword, lineNr):
		try:
			int(line)
		except ValueError:
			raise self.IntegerError(line, keyword, lineNr)
		return
	
	def checkFloat(self, line, keyword, lineNr):
		try:
			float(line)
		except ValueError:
			raise self.FloatError(line, keyword, lineNr)
		return


def main(sys):
	# Öffne Lösungsdatei
	try:
		if len(sys.argv) == 2:
			solFile = open(sys.argv[1], 'r')
		else:
			print "Verwendung: python Formatpruefer Loesungsdatei"
			sys.exit(-1) 
	
	except:
		print "Konnte die Loesungsdatei nicht öffnen: " + sys.argv[1]
		sys.exit(-1)
	
		
	content = solFile.read().splitlines()
	
	lineNr = 0
	parser = Parser()
	
	try:
		parser.checkKeyword(content[lineNr], "LOESUNGSWERT", lineNr + 1)
		lineNr += 1
		parser.checkFloat(content[lineNr], "LOESUNGSWERT", lineNr + 1)
		value = float(content[lineNr])
		lineNr += 1
			
		parser.checkKeyword(content[lineNr], "ANZAHL_ZUEGE", lineNr + 1)
		lineNr += 1
		parser.checkInt(content[lineNr], "ANZAHL_ZUEGE", lineNr + 1)
		numberOfTrains = int(content[lineNr])
		lineNr += 1
			
		departureTimes = {}		
		for _i in range(numberOfTrains):
			parser.checkKeyword(content[lineNr], "ZUG_NR", lineNr + 1)
			lineNr += 1
			parser.checkInt(content[lineNr], "ZUG_NR", lineNr + 1)
			trainID = int(content[lineNr])
			lineNr += 1
				
			parser.checkKeyword(content[lineNr], "ANZAHL_STRECKEN", lineNr + 1)
			lineNr += 1
			parser.checkInt(content[lineNr], "ANZAHL_STRECKEN", lineNr + 1)
			numberOfLegs = int(content[lineNr])
			lineNr += 1
				
			for _j in range(numberOfLegs):
				parser.checkKeyword(content[lineNr], "STRECKEN_NR", lineNr + 1)
				lineNr += 1
				parser.checkInt(content[lineNr], "STRECKEN_NR", lineNr + 1)
				trackID = int(content[lineNr])
				lineNr += 1
					
				parser.checkKeyword(content[lineNr], "ABFAHRTSZEIT", lineNr + 1)
				lineNr += 1
				parser.checkInt(content[lineNr], "ABFAHRTSZEIT", lineNr + 1)
				departureTime = int(content[lineNr])
				lineNr += 1
					
				if departureTimes.has_key(trainID) and departureTimes[trainID].has_key(trackID):
					raise parser.DoubleAssignmentError(trainID, trackID)
					
				departureTimes[trainID, trackID] = departureTime
			
		parser.checkKeyword(content[lineNr], "ENDE", lineNr)
	
		print "Die Loesungsdatei ist gültig"
	
	except Exception, e:
		print e
		print "Die Lösungsdatei ist ungültig."
	
	finally:
		solFile.close()


main(sys)
