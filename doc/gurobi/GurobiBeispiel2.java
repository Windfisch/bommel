import gurobi.*;


public class GurobiBeispiel2 {

	public static void main(String[] args) throws GRBException {
		/* Beispiel f�r folgendes LP:
		 * 
		 * min 100*x1 + 199.9*x2 - 5500*x3 - 6100*x4
		 * 
		 * -0.01*x1	-	0.02*x2		+	0.5*x3	+	0.6*x4	<=	0
		 * 100*x1	+	199.9*x2	+	700*x3	+	800*x4	<=	100000
		 * 1*x1		+	1*x2		+	0*x3	+	0*x4	<=	1000
		 * 0*x1		+	0*x2		+	90*x3	+	100*x4	<=	2000
		 * 0*x1		+	0*x2		+	40*x3	+	50*x4	<=	800
		 * 
		 * 
		 */


		//Grundgeruest
		//Umgebung erstellen
		GRBEnv env = new GRBEnv("NAME");
		//Model(Ein Problem) erstellen.
		GRBModel model = new GRBModel(env);
		//Variablen festlegen
		//addVar(1,2,3,4,5)
		//1:= Untere Intervallgrenze
		//2:= Obere Intervallgrenze
		//3:= Faktor in der Zielfunktion (Am besten erst mal 0 Waehlen)
		//4:= Variablen Art z.B. GRB.BINARY oder GRB.CONTINUOUS
		//5:= Variablenname
		GRBVar x1 = model.addVar(0, Double.MAX_VALUE, 0.0, GRB.CONTINUOUS, "x1");
		GRBVar x2 = model.addVar(0, Double.MAX_VALUE, 0.0, GRB.CONTINUOUS, "x2");
		GRBVar x3 = model.addVar(0, Double.MAX_VALUE, 0.0, GRB.CONTINUOUS, "x3");
		GRBVar x4 = model.addVar(0, Double.MAX_VALUE, 0.0, GRB.CONTINUOUS, "x4");
		//Problem akttualisieren
		model.update();

		//Zielfunktion erstellt
		//Expr ist jeweils eine Ungleichung.
		//Es k�nnen nicht mehrere Ungleichung in eine Expression.
		//Variablen mit Faktor 0 muessen nicht mit eingegeben werden.
		GRBLinExpr expr;
		expr = new GRBLinExpr();
		expr.addTerm(100, x1);
		expr.addTerm(199.9, x2);
		expr.addTerm(-5500, x3);
		expr.addTerm(-6100, x4);
		//Ungleichung zu Model hinzufugen
		model.setObjective(expr, GRB.MINIMIZE);


		//Nebenbedingungen
		//Es wird jeweils eine neue Expression benoetigt.
		expr = new GRBLinExpr();

		expr.addTerm(-0.01, x1);
		expr.addTerm(-0.02, x2);
		expr.addTerm(0.5, x3);
		expr.addTerm(0.6, x4);
		model.addConstr(expr, GRB.LESS_EQUAL, 0, "b1");

		expr = new GRBLinExpr();
		expr.addTerm(100, x1);
		expr.addTerm(199.9, x2);
		expr.addTerm(700, x3);
		expr.addTerm(800, x4);
		model.addConstr(expr, GRB.LESS_EQUAL, 100000, "b2");

		expr = new GRBLinExpr();
		expr.addTerm(1, x1);
		expr.addTerm(1, x2);
		model.addConstr(expr, GRB.LESS_EQUAL, 1000, "b3");

		expr = new GRBLinExpr();
		expr.addTerm(90, x3);
		expr.addTerm(100, x4);
		model.addConstr(expr, GRB.LESS_EQUAL, 2000, "b4");

		expr = new GRBLinExpr();
		expr.addTerm(40, x3);
		expr.addTerm(50, x4);
		model.addConstr(expr, GRB.LESS_EQUAL, 800, "b5");


		// Optimize model

		model.optimize();


		System.out.println(x1.get(GRB.StringAttr.VarName)
				+ " " + x1.get(GRB.DoubleAttr.X));
		System.out.println(x2.get(GRB.StringAttr.VarName)
				+ " " + x2.get(GRB.DoubleAttr.X));
		System.out.println(x3.get(GRB.StringAttr.VarName)
				+ " " + x3.get(GRB.DoubleAttr.X));
		System.out.println(x4.get(GRB.StringAttr.VarName)
				+ " " + x4.get(GRB.DoubleAttr.X));


		System.out.println("Obj: " + model.get(GRB.DoubleAttr.ObjVal));


		// Dispose of model and environment
		model.dispose();
		env.dispose();


	}


}

