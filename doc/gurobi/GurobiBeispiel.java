import gurobi.*;


public class GurobiBeispiel {

	public static void main(String[] args) throws GRBException {
		/* Beispiel f�r folgendes LP:
		 * 
		 * min 100*x1 + 199.9*x2 - 5500*x3 - 6100*x4
		 * 
		 * -0.01*x1	-	0.02*x2		+	0.5*x3	+	0.6*x4	<=	0
		 * 100*x1	+	199.9*x2	+	700*x3	+	800*x4	<=	100000
		 * 1*x1		+	1*x2		+	0*x3	+	0*x4	<=	1000
		 * 0*x1		+	0*x2		+	90*x3	+	100*x4	<=	2000
		 * 0*x1		+	0*x2		+	40*x3	+	50*x4	<=	800
		 * 
		 * 
		 */


		//Grundgeruest
		//Umgebung erstellen
		GRBEnv env = new GRBEnv("NAME");
		//Model(Ein Problem) erstellen.
		GRBModel model = new GRBModel(env);
		//Variablen festlegen
		//addVar(1,2,3,4,5)
		//1:= Untere Intervallgrenze
		//2:= Obere Intervallgrenze
		//3:= Faktor in der Zielfunktion (Am besten erst mal 0 Waehlen)
		//4:= Variablen Art z.B. GRB.BINARY oder GRB.CONTINUOUS
		//5:= Variablenname
		GRBVar x1 = model.addVar(0, Double.MAX_VALUE, 0.0, GRB.CONTINUOUS, "x1");
		GRBVar x2 = model.addVar(0, Double.MAX_VALUE, 0.0, GRB.CONTINUOUS, "x2");
		GRBVar x3 = model.addVar(0, Double.MAX_VALUE, 0.0, GRB.CONTINUOUS, "x3");
		GRBVar x4 = model.addVar(0, Double.MAX_VALUE, 0.0, GRB.CONTINUOUS, "x4");
		//Problem akttualisieren
		model.update();

		//Zielfunktion erstellt
		//Expr ist jeweils eine Ungleichung.
		//Es k�nnen nicht mehrere Ungleichung in eine Expression.
		//Variablen mit Faktor 0 muessen nicht mit eingegeben werden.
		GRBLinExpr expr;
		expr = new GRBLinExpr();
		expr.addTerm(100, x1);
		expr.addTerm(199.9, x2);
		expr.addTerm(-5500, x3);
		expr.addTerm(-6100, x4);
		//Ungleichung zu Model hinzufugen
		model.setObjective(expr, GRB.MINIMIZE);


		//Nebenbedingungen
		//Variante mit Arrays.

		double[] coeffs1 = new double[4];
		double[] coeffs2 = new double[4];
		double[] coeffs3 = new double[4];
		double[] coeffs4 = new double[4];
		double[] coeffs5 = new double[4];
		double[] coeffsb = new double[5];
		char[] senses = new char[5];
		String[] namesB = new String[5];
		GRBVar[] var = new GRBVar[4];
		GRBLinExpr[] expres = new GRBLinExpr[5];


		namesB[0] = "b1";
		namesB[1] = "b2";
		namesB[2] = "b3";
		namesB[3] = "b4";
		namesB[4] = "b5";


		senses[0] = GRB.LESS_EQUAL;
		senses[1] = GRB.LESS_EQUAL;
		senses[2] = GRB.LESS_EQUAL;
		senses[3] = GRB.LESS_EQUAL;
		senses[4] = GRB.LESS_EQUAL;

		var[0] = x1;
		var[1] = x2;
		var[2] = x3;
		var[3] = x4;


		coeffs1[0] = -0.01;
		coeffs1[1] = -0.02;
		coeffs1[2] = 0.5;
		coeffs1[3] = 0.6;

		coeffs2[0] = 100;
		coeffs2[1] = 199.9;
		coeffs2[2] = 700;
		coeffs2[3] = 800;

		coeffs3[0] = 1;
		coeffs3[1] = 1;
		coeffs3[2] = 0;
		coeffs3[3] = 0;

		coeffs4[0] = 0;
		coeffs4[1] = 0;
		coeffs4[2] = 90;
		coeffs4[3] = 100;

		coeffs5[0] = 0;
		coeffs5[1] = 0;
		coeffs5[2] = 40;
		coeffs5[3] = 50;


		coeffsb[0] = 0;
		coeffsb[1] = 100000;
		coeffsb[2] = 1000;
		coeffsb[3] = 2000;
		coeffsb[4] = 800;

		expres[0] = new GRBLinExpr();
		expres[0].addTerms(coeffs1, var);
		expres[1] = new GRBLinExpr();
		expres[1].addTerms(coeffs2, var);
		expres[2] = new GRBLinExpr();
		expres[2].addTerms(coeffs3, var);
		expres[3] = new GRBLinExpr();
		expres[3].addTerms(coeffs4, var);
		expres[4] = new GRBLinExpr();
		expres[4].addTerms(coeffs5, var);


		model.addConstrs(expres, senses, coeffsb, namesB);


		// Optimize model

		model.optimize();


		System.out.println(x1.get(GRB.StringAttr.VarName)
				+ " " + x1.get(GRB.DoubleAttr.X));
		System.out.println(x2.get(GRB.StringAttr.VarName)
				+ " " + x2.get(GRB.DoubleAttr.X));
		System.out.println(x3.get(GRB.StringAttr.VarName)
				+ " " + x3.get(GRB.DoubleAttr.X));
		System.out.println(x4.get(GRB.StringAttr.VarName)
				+ " " + x4.get(GRB.DoubleAttr.X));


		System.out.println("Obj: " + model.get(GRB.DoubleAttr.ObjVal));

		// Dispose of model and environment

		model.dispose();
		env.dispose();


	}


}
