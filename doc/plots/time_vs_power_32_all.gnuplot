set termoption enhanced

set xrange [0:600]

plot 'time_vs_power_32_naivemid/time_vs_power.data' using 1:2 with lines smooth csplines title 'NaiveSolver (Mitte)', \
     'time_vs_power_32_morerandom/time_vs_power.data' using 1:2 with lines smooth csplines title 'MoreRandomSolver', \
     'time_vs_power_32/time_vs_power.data' using 1:2 with lines smooth csplines title 'LinearSolver (alt)', \
     'time_vs_power_32_newlinear/time_vs_power.data' using 1:2 with lines smooth csplines title 'LinearSolver (neu)', \
     272.699E6 with lines smooth csplines title 'untere Schranke';

pause -1
