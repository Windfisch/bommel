set termoption enhanced
set arrow from 0,graph(0,0) to 0,graph(1,1) nohead;
set label "10000000000,000000" at 15,graph(0,0.015);
set arrow from 269,graph(0,0) to 269,graph(1,1) nohead;
set label "1000000000000000,000000" at 284,graph(0,0.015);
set arrow from 688,graph(0,0) to 688,graph(1,1) nohead;
set label "100000000000000000000,000000" at 703,graph(0,0.015);
set arrow from 1283,graph(0,0) to 1283,graph(1,1) nohead;
set label "10000000000000000000000000,000000" at 1298,graph(0,0.015);
set arrow from 2164,graph(0,0) to 2164,graph(1,1) nohead;
set label "1000000000000000000000000000000,000000" at 2179,graph(0,0.015);
set arrow from 3195,graph(0,0) to 3195,graph(1,1) nohead;
set label "100000000000000000000000000000000000,000000" at 3210,graph(0,0.015);
plot 'time_vs_power.data' using 1:2 with lines  title 'Leistung' ,\
     39800000 with lines title 'MoreRandom (illegal)' ,\
     12900000 with lines title 'untere Schranke'

pause -1
