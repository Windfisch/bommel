set terminal pdf fontscale 0.25
set output 'compare.pdf'
set termoption enhanced
set key left bottom

set log x

plot 'flex_vs_power.data' using 1:2 with dots lt rgb "#000000"  title 'startloesung', \
     'flex_vs_power.data' using 1:4 with dots lt rgb "#880000"  title '20sec 1', \
     'flex_vs_power.2.data' using 1:4 with dots lt rgb "#FF0000"  title '20sec 2', \
     'flex_vs_power.3.data' using 1:4 with dots lt rgb "#FF8800"  title '20sec 3', \
     'flex_vs_power.4.data' using 1:4 with dots lt rgb "#FF0088"  title '20sec 4', \
     'flex_vs_power.data' using 1:6 with dots lt rgb "#008800"  title '60sec 1', \
     'flex_vs_power.2.data' using 1:6 with dots lt rgb "#00FF00"  title '60sec 2', \
     'flex_vs_power.3.data' using 1:6 with dots lt rgb "#88FF00"  title '60sec 3', \
     'flex_vs_power.4.data' using 1:6 with dots lt rgb "#00FF88"  title '60sec 4', \
     'flex_vs_power.data' using 1:8 with dots lt rgb "#000088"  title '3.5min 1', \
     'flex_vs_power.2.data' using 1:8 with dots lt rgb "#0000FF"  title '3.5min 2', \
     'flex_vs_power.3.data' using 1:8 with dots lt rgb "#0088FF"  title '3.5min 3', \
     'flex_vs_power.4.data' using 1:8 with dots lt rgb "#8800FF"  title '3.5min 4', \
     'flex_vs_power.data' using 1:13 with dots lt rgb "#008888"  title '20min 1', \
     'flex_vs_power.2.data' using 1:13 with dots lt rgb "#00FFFF"  title '20min 2',\
     'flex_vs_power.3.data' using 1:13 with dots lt rgb "#44FFFF"  title '20min 3',\
     'flex_vs_power.4.data' using 1:13 with dots lt rgb "#448888"  title '20min 4',\

