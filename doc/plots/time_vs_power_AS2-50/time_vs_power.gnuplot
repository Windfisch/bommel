set termoption enhanced
set arrow from 0,graph(0,0) to 0,graph(1,1) nohead;
set label "10000000000,000000" at 15,graph(0,0.015);
set arrow from 54,graph(0,0) to 54,graph(1,1) nohead;
set label "1000000000000000,000000" at 69,graph(0,0.015);
set arrow from 97,graph(0,0) to 97,graph(1,1) nohead;
set label "100000000000000000000,000000" at 112,graph(0,0.015);
set arrow from 144,graph(0,0) to 144,graph(1,1) nohead;
set label "10000000000000000000000000,000000" at 159,graph(0,0.015);
set arrow from 183,graph(0,0) to 183,graph(1,1) nohead;
set label "1000000000000000000000000000000,000000" at 198,graph(0,0.015);
set arrow from 228,graph(0,0) to 228,graph(1,1) nohead;
set label "100000000000000000000000000000000000,000000" at 243,graph(0,0.015);
set arrow from 263,graph(0,0) to 263,graph(1,1) nohead;
set label "10000000000000000000000000000000000000000,000000" at 278,graph(0,0.015);
set arrow from 302,graph(0,0) to 302,graph(1,1) nohead;
set label "1000000000000000000000000000000000000000000000,000000" at 317,graph(0,0.015);
set arrow from 348,graph(0,0) to 348,graph(1,1) nohead;
set label "100000000000000000000000000000000000000000000000000,000000" at 363,graph(0,0.015);
set arrow from 443,graph(0,0) to 443,graph(1,1) nohead;
set label "10000000000000000000000000000000000000000000000000000000,000000" at 458,graph(0,0.015);
set arrow from 526,graph(0,0) to 526,graph(1,1) nohead;
set label "1000000000000000000000000000000000000000000000000000000000000,000000" at 541,graph(0,0.015);

plot 'time_vs_power.data' using 1:2 with lines  title 'Leistung' ,\
      32000000 with lines title 'MoreRandom (illegal)' ,\
       8100000 with lines title 'untere Schranke'

pause -1

