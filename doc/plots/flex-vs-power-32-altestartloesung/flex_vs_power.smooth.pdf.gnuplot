set terminal pdf
set output 'flex.smooth.pdf'
set termoption enhanced
set key left bottom

set log x

plot 'flex_vs_power.data' using 1:2 with lines smooth bezier title 'Startloesung', \
     'flex_vs_power.data' using 1:3 with lines smooth bezier title '10sec', \
     'flex_vs_power.data' using 1:4 with lines smooth bezier title '20sec', \
     'flex_vs_power.data' using 1:5 with lines smooth bezier title '40sec', \
     'flex_vs_power.data' using 1:6 with lines smooth bezier title '60sec', \
     'flex_vs_power.data' using 1:7 with lines smooth bezier title '2min', \
     'flex_vs_power.data' using 1:8 with lines smooth bezier title '3.5min', \
     'flex_vs_power.data' using 1:9 with lines smooth bezier title '5min', \
     'flex_vs_power.data' using 1:10 with lines smooth bezier title '7.5min', \
     'flex_vs_power.data' using 1:11 with lines smooth bezier title '10min', \
     'flex_vs_power.data' using 1:12 with lines smooth bezier title '15min', \
     'flex_vs_power.data' using 1:13 with lines smooth bezier title '20min';


