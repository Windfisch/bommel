set termoption enhanced
set key right top

set xrange [-1:8 ]
set grid xtics
set xtics 1

plot 'neigh_vs_power.2-63.data' using 1:2 with histeps title 'initial', \
     'neigh_vs_power.2-63.data' using 1:4 with histeps title '1m', \
     'neigh_vs_power.2-63.data' using 1:5 with histeps title '2m', \
     'neigh_vs_power.2-63.data' using 1:6 with histeps title '5m', \
     'neigh_vs_power.2-63.data' using 1:9 with histeps title '30min';

pause -1

