set termoption enhanced
set arrow from 0,graph(0,0) to 0,graph(1,1) nohead;
set label "10000000000,000000" at 15,graph(0,0.015);
set arrow from 209,graph(0,0) to 209,graph(1,1) nohead;
set label "1000000000000000,000000" at 224,graph(0,0.015);
set arrow from 643,graph(0,0) to 643,graph(1,1) nohead;
set label "100000000000000000000,000000" at 658,graph(0,0.015);
plot 'time_vs_power.data' using 1:2 with lines title 'Leistung';

pause -1
