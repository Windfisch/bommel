set termoption enhanced
set arrow from 0,graph(0,0) to 0,graph(1,1) nohead;
set label "10000000000,000000" at 15,graph(0,0.015);
set arrow from 26,graph(0,0) to 26,graph(1,1) nohead;
set label "1000000000000000,000000" at 41,graph(0,0.015);
set arrow from 148,graph(0,0) to 148,graph(1,1) nohead;
set label "100000000000000000000,000000" at 163,graph(0,0.015);
set arrow from 851,graph(0,0) to 851,graph(1,1) nohead;
set label "10000000000000000000000000,000000" at 866,graph(0,0.015);
plot 'time_vs_power.data' using 1:2 with lines title 'Leistung';

pause -1
