set termoption enhanced
set arrow from 0,graph(0,0) to 0,graph(1,1) nohead;
set label "1E+10" at 12,graph(0,0.032);
set arrow from 94,graph(0,0) to 94,graph(1,1) nohead;
set label "1E+15" at 106,graph(0,0.032);
set arrow from 445,graph(0,0) to 445,graph(1,1) nohead;
set label "1E+20" at 457,graph(0,0.032);
plot 'time_vs_power.data' using 1:2 with lines smooth csplines title 'Leistung';

pause -1
