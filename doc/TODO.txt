DivideAndConquer sollte schon Zwischenergebnisse liefern:
sobald es Metazuege gibt, sollen diese "mittig" platziert werden, das ist die
Zwischenloesung.


(23:42:17) flo: was wir noch tun koennen:
(23:42:29) flo: wenn die "schlauen" improver nicht weiterkommen, dann mal
mehrere randoms drauftun
(23:42:34) flo: und dann mehrere branches haben
(23:42:47) flo: und diese mehrere branches duerfen gerne auch erstmal
schlechter sein
(23:43:21) flo: haben aber eine "lifetime", und bis sie endlich wieder besser
werden als der punkt, an dem sie weggeforkt sind, leben sie nur lifetime-lang.



AS II

selStrategy:
a) alle Trips eines selected Trips rausnehmen
b) keinen
c) nur die n benachbarten
d) keine Trips selektieren, die sowieso keinen negativen Effekt wegen
   Constraints haben:
   z.b. wenn Trip1 am Ende seiner Abfahrszeit schedult ist, und Trip2 
   fuer den Anfang seiner Abfahrtszeit, und Trip2 selected wird.
   Dann brauche wir Trip1 nicht selecten, weil er sich nicht negativ
   auswirkt.
