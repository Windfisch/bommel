/*
 * Copyright (c) 2014, Florian Jung, Benjamin Miller, Johannes Thürauf, David Wägner
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package de.abbaddie.bommel.solver;

import de.abbaddie.bommel.data.*;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * <p>Ein heuristischer Solver, der einfach alle Fahrten an ihre früheste oder späteste Abfahrtsmöglichkeit abfahren
 * lässt.<br />
 * Er dient vor allem dem informativen Vergleich mit den anderen heuristischen Lösern.</p>
 * <p/>
 * <p>Dieser Löser ist AS2-fähig.</p>
 */
public class NaiveSolver implements Solver {
	private Instance instance;
	private boolean begin;

	/**
	 * Erstellt einen {@link NaiveSolver}, der noch konfiguriert werden sollte.
	 */
	public NaiveSolver() {

	}

	/**
	 * Erstellt einen {@link NaiveSolver} mit der gegebenen Probleminstanz und der Angabe, ob der früheste oder
	 * späteste Zeitpunkt gewählt werden soll.
	 *
	 * @param instance Probleminstanz
	 * @param begin    Anfang oder Ende?
	 */
	public NaiveSolver(Instance instance, boolean begin) {
		this.instance = instance;
		this.begin = begin;
	}

	@Override
	public void setInstance(Instance instance) {
		this.instance = instance;
	}

	/**
	 * Setzt, ob der Anfang oder das Ende gewählt werden soll, aus der Konfiguration.
	 *
	 * @param begin <code>true</code> für Anfang und <code>false</code> für Ende
	 */
	public void setBegin(String begin) {
		this.begin = Boolean.parseBoolean(begin);
	}

	@Override
	public TripPlanning solve(List<TripInformation> trips) {
		TripPlanning sol = new MutableTripPlanning(instance);
		Set<Train> trains = new HashSet<>();

		for (TripInformation t : trips) {
			RealTrip trip = (RealTrip)t.getTrip();
			Train train = trip.getTrain();

			if (trains.add(train)) {
				int limitNext = begin ? 0 : (instance.getSchedulingInterval() - 1);

				List<Trip> innerTrips = train.getTrips();
				for (int i = begin ? 0 : innerTrips.size() - 1; i < innerTrips.size() && i >= 0; i += begin ? 1 : -1) {
					Trip innerTrip = innerTrips.get(i);

					if (!begin) limitNext -= innerTrip.getOccupancyTime();

					int departure;
					if (begin) departure = Math.max(limitNext, innerTrip.getAllowedDepartureEarliest());
					else departure = Math.min(limitNext, innerTrip.getAllowedDepartureLatest());

					sol = sol.withDeparture(innerTrip, departure);

					limitNext = departure + (begin ? innerTrip.getOccupancyTime() : 0);
				}
			}
		}

		return sol;
	}

	@Override
	public String toString() {
		return "NaiveSolver";
	}
}
