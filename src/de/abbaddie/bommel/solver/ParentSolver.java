/*
 * Copyright (c) 2014, Florian Jung, Benjamin Miller, Johannes Thürauf, David Wägner
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package de.abbaddie.bommel.solver;

import de.abbaddie.bommel.data.Instance;

/**
 * Ein Löser, welcher einen anderen Löser enthält. Wird für das Einlesen der Konfiguration genutzt.
 */
public interface ParentSolver extends Solver {
	/**
	 * Setzt den Löser, der für die kleineren Teilprobleme genutzt werden soll.
	 *
	 * @param solver Teilproblemlöser
	 */
	public void setChildSolver(Solver solver);

	/**
	 * Setzt die Probleminstanz, zu der die Lösung erstellt werden soll.<br />
	 * Wir mindestens für die Erstellung der {@link de.abbaddie.bommel.data.TripPlanning} benötigt, ggf. noch für
	 * weitere Informationen.<br />
	 * Reicht die Instanz an den Teilproblemlöser, sofern bereits gesetzt, weiter.
	 *
	 * @param instance Probleminstanz
	 */
	@Override
	public void setInstance(Instance instance);
}
