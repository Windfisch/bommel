/*
 * Copyright (c) 2014, Florian Jung, Benjamin Miller, Johannes Thürauf, David Wägner
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package de.abbaddie.bommel.solver;

import de.abbaddie.bommel.data.*;
import gurobi.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>Ein Löser, welcher eine MIP-Modellierung und Gurobi nutzt, und sowohl heuristisch als auch optimal lösen kann.<br />
 * Das MIP-Modell ist in der Ausarbeitung näher beschrieben.<br />
 * Um Gurobi nutzen zu können, muss auf dem ausführenden System Gurobi installiert und lizenziert sein. Die Gurobi-Java-
 * Bibliothek wird unter <code>lib/gurobi.jar</code> erwartet. (Sonst wird dieser Löser nicht funktionieren!)<br />
 * Es wird normalerweise versucht, optimal zu lösen. Es kann aber auch ein Zeitlimit gesetzt werden, dann wird die bis
 * dahin beste gefundene (nicht notwendigerweise optimale) Lösung zurückgeben. <br />
 * Wird immer über die gleiche Menge an Zügen gelöst (z.B. in den Verbesserern, aber bspw. nicht in
 * {@link LinearSolver}), kann <code>optimizeInstance</code> gesetzt werden, wodurch jeweils der alte Wert als Cutoff
 * übergeben wird.<br />
 * Es kann eine Startlösung übergeben werden.<br />
 * Wird das Maximum mehrmals im selben Slot (Kongruenzklasse) angenommen, wird versucht, die anderen Slots zu ignorieren
 * und das MIP damit klein zu halten. (Es ist sichergestellt, dass dadurch keine schlechtere Lösung entsteht.)</p>
 * <p/>
 * <p>Dieser Löser ist AS2-fähig.</p>
 */
public class GurobiSolver implements ReusingSolver {
	/**
	 * Zahl der Schritte, in denen der Slotmodus Erfolg gehabt haben hätte müssen, bevor er tatsächlich aktiviert wird.
	 */
	public static final int WARMUP_LIMIT = 4;

	private Instance instance;
	private GRBEnv env;
	private int lastMaxSlot = -1;
	private int warmup = 0;
	private float lastMax = 0;
	private boolean output;
	private int timeLimit = 3600;
	private double heuristics = 0.5;
	private boolean optimizeInstance;
	private TripPlanning reuse;

	/**
	 * Erstellt einen {@link GurobiSolver} mit Standardeinstellungen, d.h. keine Optimierung gegen die Instanz,
	 * Ausgabe und eine Zeitbeschränkung von einer Stunde.
	 */
	public GurobiSolver() {
		try {
			env = new GRBEnv();
			env.set(GRB.DoubleParam.TimeLimit, timeLimit);
		} catch (GRBException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Erstellt einen {@link GurobiSolver} mit genaueren Einstellungen. Zeitbeschränkung ist eine Stunde.
	 */
	public GurobiSolver(Instance instance, boolean output, boolean optimizeInstance) {
		this();

		this.instance = instance;
		this.optimizeInstance = optimizeInstance;
		setOutput(output);
	}

	/**
	 * Erstellt einen {@link GurobiSolver}. Zeitbeschränkung ist eine Stunde, es findet keine Optimierung gegen die
	 * Instanz statt.
	 */
	public GurobiSolver(Instance instance, boolean output) {
		this(instance, output, false);
	}

	@Override
	public void setSolution(TripPlanning planning) {
		this.reuse = planning;
	}

	/**
	 * Setzt die Zeitbeschränkung aus der Konfiguration.
	 *
	 * @param timeLimit Zeitbeschränkung als Zeichenkette
	 */
	public void setTimeLimit(String timeLimit) {
		this.timeLimit = Integer.parseInt(timeLimit);
	}

	/**
	 * Setzt, ob gegen die Instanz optimiert wird, aus der Konfiguration.
	 *
	 * @param optimizeInstance Instanzoptimierung
	 */
	public void setOptimizeInstance(String optimizeInstance) {
		this.optimizeInstance = Boolean.parseBoolean(optimizeInstance);
	}

	/**
	 * Setzt den Anteil an Heuristik, den Gurobi verwende soll, aus der Konfiguration.
	 *
	 * @param heuristics Heuristikanteil als Zeichenkette
	 */
	public void setHeuristics(String heuristics) {
		this.heuristics = Double.parseDouble(heuristics);
	}

	/**
	 * Setzt, ob eine Ausgabe erfolgen soll, aus der Konfiguration
	 *
	 * @param show Ausgabe
	 */
	public void setOutput(String show) {
		setOutput(Boolean.parseBoolean(show));
	}

	/**
	 * Setzt, ob eine Ausgabe erfolgen soll.
	 *
	 * @param show Ausgabe
	 */
	public void setOutput(boolean show) {
		output = show;
		try {
			env.set(GRB.IntParam.OutputFlag, show ? 1 : 0);
		} catch (GRBException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void setInstance(Instance instance) {
		this.instance = instance;
		lastMaxSlot = -1;
		warmup = 0;
		lastMax = 0;

		try {
			env = new GRBEnv();
			env.set(GRB.DoubleParam.TimeLimit, timeLimit);
		} catch (GRBException e) {
			e.printStackTrace();
		}
		setOutput(output);
	}

	/**
	 * Setzt, ob bzgl. aller Züge einer Instanz optimiert wird.
	 */
	public void setOptimizeInstance(boolean optimizeInstance) {
		this.optimizeInstance = optimizeInstance;
	}

	/**
	 * Löst das Problem.<br />
	 *
	 * @param tripInfos Planungsmöglichkeiten
	 * @return Lösung. Kann <code>null</code> sein.
	 */
	@Override
	public TripPlanning solve(List<TripInformation> tripInfos) {
		// Schalte ggf. in einen „Slotmodus“: Erfahrungsgemäß können Spitzen oft auch bei wechselnden
		//  Maximalpositionen im selben Slot sein (bspw. 11,9 Sek nach Minutenanfang). Nutze dieses Wissen aus,
		//  um zunächst alle anderen Slots wegzulassen (spart 99,8% der Zeilen). Falls das nicht klappt muss
		//  eben doch komplett durchgerechnet werden.
		TripPlanning sol;
		try {
			sol = solveHelp(tripInfos, lastMaxSlot != -1 && warmup > WARMUP_LIMIT);
			if (sol == null) {
				warmup = 0;
				sol = solveHelp(tripInfos, false);
			}
		} catch (InfeasibleException e) {
			// tja, dann gibts halt keine Verbesserung.
			return null;
		}
		return sol;
	}

	/**
	 * Löst das Problem, ggf. im Slotmodus.<br />
	 * Wird der Slotmodus verwendet, und wird erkannt, dass das neue Maximum außerhalb unseres Slots liegt, wird
	 * <code>null</code> zurückgegeben. Ist Gurobi der Meinung, dass das Problem nicht lösbar ist, wird eine
	 * Ausnahme ausgelöst.<br />
	 * Wird in der Zeitbeschränkung keine Lösung gefunden, wird <code>null</code> zurückgegeben.
	 *
	 * @param tripInfos   Planungsmöglichkeiten
	 * @param slottedMode Slotmodus
	 * @return Lösung. Kann <code>null</code> sein.
	 * @throws InfeasibleException
	 */
	public TripPlanning solveHelp(List<TripInformation> tripInfos, boolean slottedMode) throws InfeasibleException {
		int tripCount = tripInfos.size();
		PowerProfile[] unplannedTripsProf = getPowerProfiles(tripInfos);

		int grbStatus = -1;
		GRBModel model = null;
		try {
			// Gurobi initialisieren
			model = new GRBModel(env);

			// Generiere die tripNumberMap
			// Wird beim Aufstellen der Constraints benötigt, da dort Referenzen zwischen Zügen benötigt werden
			Map<Trip, Integer> tripNumberMap = new HashMap<>(tripInfos.size());
			for (int i = 0; i < tripCount; i++)
				tripNumberMap.put(tripInfos.get(i).getTrip(), i);

			// AS2: Sammle alle Constraints
			// Ist bei AS1 eine leere Liste
			List<Constraint> constraints = new ArrayList<>();
			for (int i = 0; i < tripCount; i++) {
				Trip trip1 = tripInfos.get(i).getTrip();
				for (int j = i + 1; j < tripCount; j++) {
					Trip trip2 = tripInfos.get(j).getTrip();

					List<Constraint> cs = trip1.constraints(trip2);
					constraints.addAll(cs);
				}
			}

			// Ausgabe
			if (output) {
				System.err.println("Gurobi hat " + constraints.size() + " Constraints.");
			}

			// Variablen in Gurobi eingeben: Binäre Variablen für Schattenfahrten ein/aus
			// Die erste Dimension stellt die Menge der Fahrten dar, die zweite die Menge der Schattenfahrten für
			//  eine Fahrt
			GRBVar[][] varTrips = new GRBVar[tripCount][];

			int count = 0;
			int col = -1;
			// Einzelne Fahrten durchprobieren
			for (int i = 0; i < tripCount; i++) {
				int[] departures = tripInfos.get(i).getConsideredDepartures();
				Trip currTrip = tripInfos.get(i).getTrip();
				int departuresCount = departures.length;
				varTrips[i] = new GRBVar[departuresCount];

				for (int j = 0; j < departuresCount; j++) {
					col++;
					int departure = departures[j];

					// Verwerfe ggf. Abfahrtszeiten
					// Z.B. Fahrt1 trifft um früestens 600 ein, dann können alle Schattenfahrten von Fahrt2 mit
					// Abfahrt vor 600 verworfen werden.
					boolean skip = false;
					for (Constraint constraint : constraints) {
						if (constraint.getRightTrip() == currTrip) {
							if (!constraint.fulfillable(tripInfos.get(tripNumberMap.get(constraint.getLeftTrip())), departure)) {
								skip = true;
								break;
							}
						} else if (constraint.getLeftTrip() == currTrip) {
							if (!constraint.fulfillable(departure, tripInfos.get(tripNumberMap.get(constraint.getRightTrip())))) {
								skip = true;
								break;
							}
						}
					}

					if (skip) {
						unplannedTripsProf[col] = null;
					} else {
						varTrips[i][j] = model.addVar(0, 1, 0.0, GRB.BINARY, "x" + count);
						count++;
					}
				}
			}

			// Ausgabe
			if (output) {
				System.out.println("Streiche " + (unplannedTripsProf.length - count) + "/" + unplannedTripsProf.length + " Abfahrtsmöglichkeiten");
			}

			// Variablen in Gurobi eingeben: Zielvariable zur Minimierung
			GRBVar varMaxPow = model.addVar(-GRB.INFINITY, GRB.INFINITY, 0.0, GRB.CONTINUOUS, "m");

			// Variablen in Gurobi eingeben: Gurobi anstubbsen
			model.update();

			// Zielfunktion
			GRBLinExpr expr = new GRBLinExpr();
			expr.addTerm(1, varMaxPow);
			model.setObjective(expr, GRB.MINIMIZE);

			// Berechne „maximal“ mögliches Minimum.
			// Daraufhin können wir alle Zeilen weglassen, deren „maximal“ mögliches Maximum darunter liegt (wird garantiert getoppt)
			float maxMin = Float.NEGATIVE_INFINITY;
			float[] maxs = new float[instance.getSchedulingInterval()];

			for (int row = slottedMode ? lastMaxSlot : 0; row < instance.getSchedulingInterval(); row += slottedMode ? 600 : 1) {
				col = 0;

				float rowMax = 0;
				for (TripInformation tripInfo : tripInfos) {
					int departuresCount = tripInfo.getConsideredDepartures().length;

					// Pro Trip ist genau ein Schattentrip aktiv
					// => Minimalfall: Jeweils der Schattentrip mit geringstem Wert.
					// => Maximalfall: Jeweils der Schattentrip mit höchstem Wert.
					float tripMin = Float.POSITIVE_INFINITY;
					float tripMax = Float.NEGATIVE_INFINITY;
					for (int j = 0; j < departuresCount; j++) {
						if (unplannedTripsProf[col] != null) {
							float tmpPowerAt = unplannedTripsProf[col].getPowerAt(row);

							maxs[row] += tmpPowerAt;
							tripMin = Math.min(tripMin, tmpPowerAt);
							tripMax = Math.max(tripMax, tmpPowerAt);

						}
						col++;
					}
					rowMax += tripMin;
					maxs[row] += tripMax;
				}
				maxMin = Math.max(maxMin, rowMax);
			}

			// Nun alle Zeilen in das Gleichungssystem eingeben.
			// Lasse Zeilen weg, deren Maximum unter vorhin ermittelter Grenze liegt
			int usedRows = 0;
			for (int row = slottedMode ? lastMaxSlot : 0; row < instance.getSchedulingInterval(); row += slottedMode ? 600 : 1) {
				if (maxs[row] > maxMin) {
					expr = new GRBLinExpr();
					col = 0;

					for (int i = 0; i < tripCount; i++) {
						int departuresCount = tripInfos.get(i).getConsideredDepartures().length;

						for (int j = 0; j < departuresCount; j++) {
							if (unplannedTripsProf[col] != null) {
								float tmpPowerAt = unplannedTripsProf[col].getPowerAt(row);

								if (tmpPowerAt != 0) {
									expr.addTerm(tmpPowerAt, varTrips[i][j]);
								}

							}
							col++;
						}
					}
					model.addConstr(expr, GRB.LESS_EQUAL, varMaxPow, "m");
					usedRows++;
				}
			}

			// Ausgabe
			if (output) {
				System.err.println("Übergebe Gurobi " + usedRows + "/" + instance.getSchedulingInterval() + " Zeilen");
			}

			// Constraints AS1: Genau eine Schattenfahrt pro Streckenfahrt
			for (int i = 0; i < tripCount; i++) {
				int departuresCount = tripInfos.get(i).getConsideredDepartures().length;
				expr = new GRBLinExpr();

				for (int j = 0; j < departuresCount; j++) {
					if (varTrips[i][j] != null) {
						expr.addTerm(1, varTrips[i][j]);
					}
				}
				model.addConstr(expr, GRB.EQUAL, 1, "trip" + i);
			}

			// Constraints AS2: Abfahrt mit ausreichend Abstand nach vorheriger Fahrt
			for (Constraint constraint : constraints) {
				int i = tripNumberMap.get(constraint.getRightTrip());
				int j = tripNumberMap.get(constraint.getLeftTrip());
				// j ist der Vorgaengertrip von i.

				TripInformation info = tripInfos.get(i);
				TripInformation beforeTripInfo = tripInfos.get(j);

				// Modelliere jetzt das Constraint:
				// cons.leftTrip.departure + cons.leftOffset <= cons.rightTrip.departure + cons.rightOffset
				// \__ =beforeTripInfo __/                      \_____ =info ____/
				expr = new GRBLinExpr();
				for (int k = 0; k < beforeTripInfo.getConsideredDepartures().length; k++) {
					if (varTrips[j][k] != null) {
						expr.addTerm(beforeTripInfo.getConsideredDepartures()[k] + constraint.leftOffset(), varTrips[j][k]);
					}
				}
				for (int k = 0; k < info.getConsideredDepartures().length; k++) {
					if (varTrips[i][k] != null) {
						expr.addTerm(-(info.getConsideredDepartures()[k] + constraint.rightOffset()), varTrips[i][k]);
					}
				}
				model.addConstr(expr, GRB.LESS_EQUAL, 0, "AS2-Constraint-Trip" + i);
			}

			// Constraint/Cutoff: Altes Maximum
			// Da uns eh nur Lösungen interessieren, bei denen das Maximum nicht höher ist als das alte,
			// können wir das als Constraint/Cutoff mitgeben
			TripPlanning currentSolution = instance.getCurrentSolution();
			if (optimizeInstance && currentSolution != null) {
				if (lastMax == 0) {
					lastMax = instance.getCurrentSolution().getPowerProfileMaximumOnly().getPowerAt(instance.getCurrentSolution().getPowerProfileMaximumOnly().getMaximumTime());
				}
				expr = new GRBLinExpr();
				expr.addTerm(1, varMaxPow);
				model.addConstr(expr, GRB.LESS_EQUAL, lastMax, "maxConstraint");
				env.set(GRB.DoubleParam.Cutoff, lastMax);
			}


			if ((optimizeInstance && instance.getCurrentSolution() != null) || reuse != null) {
				TripPlanning usedSolution = instance.getCurrentSolution();
				if (usedSolution == null || reuse != null) {
					usedSolution = reuse;
				}

				for (int i = 0; i < tripCount; i++) {
					int[] departures = tripInfos.get(i).getConsideredDepartures();
					Trip currTrip = tripInfos.get(i).getTrip();
					int departuresCount = departures.length;
					int currDeparture = usedSolution.getDeparture(currTrip);

					for (int j = 0; j < departuresCount; j++) {
						int departure = departures[j];

						if (varTrips[i][j] != null) {
							boolean set = (currTrip instanceof SuperTrip) ? (j == 0) : (departure == currDeparture);
							varTrips[i][j].set(GRB.DoubleAttr.Start, set ? 1 : 0);
						}
					}
				}
			}

			// Weitere Optimierungen am Gurobilösungsverfahren
			// Es sind hier einige Werte auskommentiert, die wir ausprobiert haben, aber nicht so viel Erfolg brachten.
			model.getEnv().set(GRB.IntParam.Method, 3);
			model.getEnv().set(GRB.IntParam.MIPFocus, 1);
			//model.getEnv().set(GRB.IntParam.BranchDir, -1);
			//model.getEnv().set(GRB.IntParam.ConcurrentMIPJobs, 10);
			//model.getEnv().set(GRB.IntParam.ConcurrentMIP, 2);
			//model.getEnv().set(GRB.IntParam.Disconnected, 0);
			model.getEnv().set(GRB.IntParam.Presolve, 2);
			model.getEnv().set(GRB.DoubleParam.Heuristics, heuristics);
			//model.getEnv().set(GRB.DoubleParam.ImproveStartGap, 0.01);
			//model.getEnv().set(GRB.DoubleParam.ImproveStartTime, (int) timeLimit/3);
			//model.getEnv().set(GRB.IntParam.MIQCPMethod, 1);
			//model.getEnv().set(GRB.IntParam.NodeMethod, 0);
			//model.getEnv().set(GRB.IntParam.RINS, 5);
			//model.getEnv().set(GRB.IntParam.SubMIPNodes, 600);
			//model.getEnv().set(GRB.IntParam.Symmetry, 2);
			//model.getEnv().set(GRB.IntParam.VarBranch, -1);
			//model.getEnv().set(GRB.IntParam.Cuts, 1);

			// Wir haben das LP vollständig modelliert.
			// Jetzt lassen wir Gurobi es mal lösen.
			model.optimize();

			// Evtl. Ausgabe, falls wir keine Optimallösung hbaen
			grbStatus = model.get(GRB.IntAttr.Status);
			if (grbStatus == GRB.Status.TIME_LIMIT) {
				// Das bedeutet notwendigerweise nur, dass die Lösung nicht optimal ist! 
				System.err.println("Gurobi scheitert am Zeitlimit.");
			}
			if (grbStatus != GRB.Status.OPTIMAL && grbStatus != GRB.Status.TIME_LIMIT) {
				// Scheitert vermutlich am MaxConstraint/Cutoff.
				throw new InfeasibleException();
			}

			// Abfahrten auslesen
			TripPlanning sol = getSolution(tripInfos, varTrips);

			// Sloterfolg prüfen
			int maxTime = sol.getPowerProfileMaximumOnly().getMaximumTime();
			float maxPower = sol.getPowerProfileMaximumOnly().getPowerAt(maxTime);
			if (maxPower < lastMax || lastMax == 0) {
				lastMax = maxPower;
			}
			int maxTimeSlot = maxTime % 600;
			if (slottedMode && maxTimeSlot != lastMaxSlot) {
				// Slotmodus war nicht erfolgreich
				sol = null;
			}
			if (!slottedMode) {
				// Falls der Slotmodus nicht aktiviert war, aber das gewesen wäre, warmup hochzählen.
				if (maxTimeSlot == lastMaxSlot) {
					warmup++;
				} else {
					warmup = 0;
				}
			}

			lastMaxSlot = maxTimeSlot;
			return sol;
		} catch (GRBException e) {
			// Darf bei Zeitüberschreitung vorkommen
			if (grbStatus != GRB.Status.TIME_LIMIT) {
				e.printStackTrace();
				return null;
			} else {
				throw new InfeasibleException();
			}
		} finally {
			// Speicherfreigabe
			if (model != null) {
				try {
					model.dispose();
				} catch (Exception ignored) {

				}
			}
		}
	}

	/**
	 * Gibt die Leistungsprofile zurück, welche in die Matrix geschrieben werden sollen.
	 * Die Matrix ergibt sich dann als „Aneinanderreihung“ der Leistungsprofile, wenn man diese als Spaltenvektoren betrachtet.
	 *
	 * @param tripInfos Planungsinformationen
	 * @return Matrix
	 */
	private PowerProfile[] getPowerProfiles(List<TripInformation> tripInfos) {
		int tripSize = 0;
		for (TripInformation tripInfo : tripInfos) {
			tripSize += tripInfo.getConsideredDepartures().length;
		}

		// unplannedTripsProf als zweidmensionales Array (wie varTrips?)
		PowerProfile[] unplannedTripsProf = new PowerProfile[tripSize];

		// Fülle unplannedTripsProf gemäß oben ein.
		int count = 0;
		for (TripInformation tripInfo : tripInfos) {
			int[] departures = tripInfo.getConsideredDepartures();

			for (int departure : departures) {
				unplannedTripsProf[count++] = tripInfo.getTrip().getPowerProfile(departure);
			}
		}
		return unplannedTripsProf;
	}

	/**
	 * Gibt die Lösung zurück, nachdem Gurobi optimiert hat.
	 *
	 * @param tripInfos Planungsmöglichkeiten
	 * @param varTrips  Gurobivariablen für die Schattenfahrten
	 * @return Lösung
	 * @throws gurobi.GRBException Wenn es keine Lösung gibt oder diese Lösung nicht gelesen werden kann
	 */
	private TripPlanning getSolution(List<TripInformation> tripInfos, GRBVar[][] varTrips) throws GRBException {
		int tripCount = tripInfos.size();
		TripPlanning sol = new MutableTripPlanning(instance);

		for (int i = 0; i < tripCount; i++) {
			TripInformation tripInfo = tripInfos.get(i);
			Trip trip = tripInfo.getTrip();
			int[] departures = tripInfo.getConsideredDepartures();

			int foundCount = 0;
			for (int j = 0; j < departures.length; j++) {
				if (varTrips[i][j] != null && varTrips[i][j].get(GRB.DoubleAttr.X) > 0.5) {
					int departure = departures[j];

					sol = sol.withDeparture(trip, departure);

					foundCount++;
				}
			}

			if (foundCount != 1) {
				System.err.println("ERROR in GurobiSolver.java: Trip " + trip + " (" + i + "/" + tripInfos.size() + ") hat " + foundCount + " aktivierte Schattentrips.");
			}
		}
		return sol;
	}

	@Override
	public String toString() {
		return "GurobiSolver" + (warmup >= WARMUP_LIMIT ? "[hot]" : "[cold]");
	}

	/**
	 * Wird ausgelöst, wenn Gurobi meldet, dass das Problem nicht lösbar ist.
	 */
	private static class InfeasibleException extends Exception {
	}
}
