/*
 * Copyright (c) 2014, Florian Jung, Benjamin Miller, Johannes Thürauf, David Wägner
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package de.abbaddie.bommel.solver;

import de.abbaddie.bommel.Bommel;
import de.abbaddie.bommel.data.*;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>Ein heuristischer Löser nach dem Teile-und-Herrsche-Prinzip.<br />
 * Die Streckenfahrten werden erst in Gruppen einer vorgegebenen Größe unterteilt, diese
 * Gruppen werden dann mit einem anderen Löser nach Möglichkeit optimal gelöst. Danach wird jede Gruppe
 * gemäß ihrer Lösung als ein {@link de.abbaddie.bommel.data.SuperTrip} betrachtet; mit diesen zusammengefassten Fahrten
 * wird genauso verfahren, bis nur noch einer (welcher die Lösung enthält) übrigbleibt.<br />
 * Die Laufzeit beträgt O(n*log(n)).
 * </p>
 * 
 * <p>Dieser Löser ist nicht AS2-fähig.</p>
 * 
 * <p>Schneller, besser und AS2-fähig ist hingegen {@link LinearSolver}.</p>
 *
 * @see DivisionStrategy
 */
public class DivideAndConquerSolver implements ParentSolver {
	private Instance instance;
	private Solver smallsolver;
	private int groupSize;
	private DivisionStrategy divisionStrategy;

	/**
	 * Erstellt einen {@link DivideAndConquerSolver} mit Standardeinstellungen, d.h. Gruppengröße 2 und Aufteilung
	 * nach Abfahrtszeitsortierung.
	 *
	 * @see OrderedDivisionStrategy
	 */
	public DivideAndConquerSolver() {
		groupSize = 2;
		divisionStrategy = new OrderedDivisionStrategy();
	}

	/**
	 * Erstellt einen {@link DivideAndConquerSolver} mit speziellen Einstellungen.
	 *
	 * @param instance         Probleminstanz
	 * @param smallsolver      Löser, welcher für Teilprobleme zu nutzen ist.
	 * @param groupSize        Gruppengröße
	 * @param divisionStrategy Aufteilungsstrategie
	 */
	public DivideAndConquerSolver(Instance instance, Solver smallsolver, int groupSize, DivisionStrategy divisionStrategy) {
		this.instance = instance;

		if (groupSize < 2)
			throw new IllegalArgumentException("groupSize must be greater or equal than two");

		this.smallsolver = smallsolver;
		this.groupSize = groupSize;
		this.divisionStrategy = divisionStrategy;
	}

	@Override
	public void setChildSolver(Solver solver) {
		smallsolver = solver;
	}

	@Override
	public void setInstance(Instance instance) {
		this.instance = instance;
		if(smallsolver != null) {
			smallsolver.setInstance(instance);
		}
	}

	/**
	 * Löst das Problem.<br />
	 * Dafür werden die Fahrten der gesetzten Gruppengröße entsprechend aufgeteilt und diese Gruppen mit dem
	 * Teilproblemlöser gelöst. Die sich daraus ergebenden zusammengefassten Fahrten werden dann genauso behandelt
	 * (rekursiv). D.h. die Rekursion erfolgt im Aufteilungsbaum von unten nach oben.
	 *
	 * @param tripInfos Planungsmöglichkeiten
	 * @return Planung
	 */
	@Override
	public TripPlanning solve(List<TripInformation> tripInfos) {
		List<Trip> trips = TripInformation.asTripList(tripInfos);

		if (trips.size() < groupSize) {
			return smallsolver.solve(tripInfos);
		}

		// Teile
		List<Subproblem> subproblems = divisionStrategy.divide(instance, trips, groupSize);
		List<Trip> supertrips = new ArrayList<>();

		Bommel.debug("\n=========================\nin solve():");
		Bommel.debug("divided " + trips.size() + " trips into " + subproblems.size() + " groups (with ~" + groupSize + " members each)");
		for (Subproblem subproblem : subproblems)
			subproblem.dump();

		assert (subproblems.size() > 1);

		// Löse die einzelnen Teile
		for (Subproblem subproblem : subproblems) {
			subproblem.solve(smallsolver);
			//Bommel.debug("Subproblem-Planning: " + subproblem.getPlanning().toString());
			supertrips.add(subproblem.getSuperTrip());
		}


		// Und herrsche: Die Anzahl der zu lösenden SuperTrips ist jetzt kleiner geworden.
		// Steige rekursiv herab und löse diese SuperTrips.
		TripPlanning planning = new MutableTripPlanning(instance);
		TripPlanning superTripPlanning;

		superTripPlanning = solve(TripInformation.asTripInformationList(supertrips));

		for (TripPlanning.Entry entry : superTripPlanning) {
			SuperTrip trip = (SuperTrip)entry.getTrip();
			planning = trip.getPlanningWithOffset(planning, entry.getDeparture());
		}

		return planning;
	}


	@Override
	public String toString() {
		return "DivideAndConquerSolver(" + groupSize + ")";
	}
}
