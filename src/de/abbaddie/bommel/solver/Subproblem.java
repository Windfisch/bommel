/*
 * Copyright (c) 2014, Florian Jung, Benjamin Miller, Johannes Thürauf, David Wägner
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package de.abbaddie.bommel.solver;

import de.abbaddie.bommel.Bommel;
import de.abbaddie.bommel.data.*;

import java.util.List;

/**
 * <p>Ein Unterproblem, wie es im Teile-und-Herrsche-Algorithmus verwendet wird.<br />
 * Es besteht aus einer Liste der Fahrten dieses Problems und speichert nach dem Lösendie Lösung dieses
 * Teilproblems.</p>
 */
public class Subproblem {
	private List<Trip> trips;
	private TripPlanning planning;
	private boolean isSolved;

	/**
	 * Erstellt ein {@link Subproblem} mit den übergebenen Daten.
	 *
	 * @param instance Probleminstanz
	 * @param trips    Fahrten dieses Teilproblems
	 */
	public Subproblem(Instance instance, List<Trip> trips) {
		this.trips = trips;
		planning = new LinkedTripPlanning(instance);
		isSolved = false;
	}

	/**
	 * Löst das Teilproblem mit dem übergebenen Löser.
	 *
	 * @param solver Löser
	 */
	public void solve(Solver solver) {
		planning = solver.solve(TripInformation.asTripInformationList(trips));
		isSolved = true;
	}

	/**
	 * Gibt eine zusammengefasste Fahrt zurück, welche die Fahrten dieses Teilproblems der Lösung entsprechend
	 * zusammenfasst.
	 *
	 * @return Zusammengefasste Fahrt
	 */
	public SuperTrip getSuperTrip() {
		if (!isSolved)
			throw new IllegalStateException("Subproblem must be solved before one can getSuperTrip()!");

		return new SuperTrip(trips, planning);
	}

	/**
	 * Gibt die Lösung dieses Teilproblems zurück.
	 *
	 * @return Teilproblemlösung
	 */
	public TripPlanning getPlanning() {
		return planning;
	}

	/**
	 * Gibt Informationen aus, wenn Debugging aktiviert ist.
	 *
	 * @see de.abbaddie.bommel.Bommel#debug(String)
	 */
	public void dump() {
		Bommel.debug("Subproblem #" + this + ":");
		if (trips == null)
			Bommel.debug("  trips is null!");
		else
			for (Trip trip : trips)
				Bommel.debug("  -> trip #" + trip);
	}
}
