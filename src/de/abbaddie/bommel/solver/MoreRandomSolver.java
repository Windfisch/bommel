/*
 * Copyright (c) 2014, Florian Jung, Benjamin Miller, Johannes Thürauf, David Wägner
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package de.abbaddie.bommel.solver;

import de.abbaddie.bommel.data.Instance;
import de.abbaddie.bommel.data.TripInformation;
import de.abbaddie.bommel.data.TripPlanning;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * <p>Ein heuristischer Löser, welcher mehrmals „würfelt“ und das beste Ergebnis wählt.<br />
 * Es entstehen erstaunlich gute Ergebnisse, bessere als typischerweise von {@link DivideAndConquerSolver}, aber
 * schlechtere als von {@link LinearSolver}.</p>
 * <p/>
 * <p>Dieser Löser ist nicht AS2-fähig.</p>
 *
 * @see RandomSolver
 */
// TODO: count konfigurierbar machen
public class MoreRandomSolver implements Solver {
	private Instance instance;

	private int count;
	private TripPlanning bestPlanning;
	private float bestValue = Float.POSITIVE_INFINITY;

	private ExecutorService exec = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());

	/**
	 * Erzeugt einen neuen {@link MoreRandomSolver} mit 1000 Versuchen.
	 */
	public MoreRandomSolver() {
		count = 1000;
	}

	/**
	 * Erzeugt einen neuen {@link MoreRandomSolver} mit der gegebenen Zahl an Versuchen.
	 *
	 * @param count Anzahl der Versuche
	 */
	public MoreRandomSolver(int count) {
		this.count = count;
	}

	@Override
	public void setInstance(Instance instance) {
		this.instance = instance;
	}

	@Override
	public TripPlanning solve(List<TripInformation> trips) {
		bestPlanning = null;
		bestValue = Float.POSITIVE_INFINITY;

		List<Future<?>> futures = new ArrayList<>(count);
		for (int i = 0; i < count; i++) {
			futures.add(exec.submit(new SolveJob(trips)));
		}
		for (Future<?> future : futures) {
			try {
				future.get();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		return bestPlanning;
	}

	/**
	 * Ein einzelner Lösungsvorgang, der in einem Thread ausgeführt wird.
	 */
	private class SolveJob implements Runnable {
		private List<TripInformation> trips;
		private RandomSolver solver;

		private SolveJob(List<TripInformation> trips) {
			this.solver = new RandomSolver();
			this.solver.setInstance(instance);

			this.trips = trips;
		}

		@Override
		public void run() {
			TripPlanning planning = solver.solve(trips);

			int maxTime = planning.getPowerProfileMaximumOnly().getMaximumTime();
			float maxValue = planning.getPowerProfileMaximumOnly().getPowerAt(maxTime);

			synchronized (MoreRandomSolver.this) {
				if (maxValue < bestValue) {
					bestPlanning = planning;
					bestValue = maxValue;
				}
			}
		}
	}
}
