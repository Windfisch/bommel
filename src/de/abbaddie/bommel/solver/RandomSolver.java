/*
 * Copyright (c) 2014, Florian Jung, Benjamin Miller, Johannes Thürauf, David Wägner
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package de.abbaddie.bommel.solver;

import de.abbaddie.bommel.data.Instance;
import de.abbaddie.bommel.data.MutableTripPlanning;
import de.abbaddie.bommel.data.TripInformation;
import de.abbaddie.bommel.data.TripPlanning;

import java.util.List;
import java.util.Random;

/**
 * <p>Ein heuristischer Löser, , welcher die Abfahrtszeiten zufällig (aber gültig) wählt.</p>
 * <p/>
 * <p>Dieser Löser ist nicht AS2-fähig.</p>
 *
 * @see de.abbaddie.bommel.solver.MoreRandomSolver
 */
public class RandomSolver implements Solver {
	private Instance instance;

	@Override
	public void setInstance(Instance instance) {
		this.instance = instance;
	}

	@Override
	public TripPlanning solve(List<TripInformation> trips) {
		TripPlanning sol = new MutableTripPlanning(instance);
		Random r = new Random();

		for (TripInformation t : trips) {
			int[] allowed = t.getTrip().getAllowedDepartures();
			int idx = r.nextInt(allowed.length);
			sol = sol.withDeparture(t.getTrip(), allowed[idx]);
		}

		return sol;
	}

	@Override
	public String toString() {
		return "RandomSolver";
	}
}
