/*
 * Copyright (c) 2014, Florian Jung, Benjamin Miller, Johannes Thürauf, David Wägner
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package de.abbaddie.bommel.solver;

import de.abbaddie.bommel.data.*;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>Ein optimal-lösender Löser, der alle Möglichkeiten durchprobiert.
 * Berechnungsprinzip ist das folgende: Nehme den ersten Zug und probiere alle Abfahrtsmöglichkeiten durch,
 * und führe das rekursiv für den Rest weiter. („Backtracking“).</p>
 * <p>Dieser Löser ist nicht AS2-fähig!</p>
 * <p>Schneller, auch optimal-lösend und AS2-fähig ist hingegeben {@link GurobiSolver}.</p>
 */
public class BruteforceSolver implements Solver {
	private boolean tryBest;
	private Instance instance;

	/**
	 * Erstellt einen normalen {@link BruteforceSolver}.
	 */
	public BruteforceSolver() {
		tryBest = true;
	}


	/**
	 * Erstellt einen {@link BruteforceSolver}, der, je nach Parameter, auch nach der schlechtesten Lösung suchen kann.
	 *
	 * @param tryBest Soll nach der besten Lösung gesucht werden?
	 */
	public BruteforceSolver(boolean tryBest) {
		this.tryBest = tryBest;
	}

	@Override
	public void setInstance(Instance instance) {
		this.instance = instance;
	}

	/**
	 * Löst das Problem. <br />
	 * <p/>
	 * Für die untersten Aufrufe im Entscheidungsbaum wird nur
	 * {@link de.abbaddie.bommel.data.TripPlanning#getPowerProfileMaximumOnly()} benötigt, deswegen werden die Fahrten
	 * zunächst so umsortiert, dass sich möglichst wenige innere Knoten ergeben (d.h. der Verzweigungsgrad unten
	 * möglichst hoch ist).
	 *
	 * @param tripInfos Planungsmöglichkeiten
	 * @return Lösung
	 */
	@Override
	public TripPlanning solve(List<TripInformation> tripInfos) {
		List<Trip> trips = TripInformation.asTripList(tripInfos);

		// Ziehe den Trip mit der größten „Freiheit“ nach hinten.
		List<TripInformation> newUnplanned = new ArrayList<>(trips.size());
		int maxLength = -1;
		TripInformation maxTrip = null;
		for (TripInformation tripInfo : tripInfos) {
			int length = tripInfo.getConsideredDepartures().length;
			if (length > maxLength) {
				maxLength = length;
				maxTrip = tripInfo;
			}
		}
		for (TripInformation tripInfo : tripInfos) {
			if (tripInfo != maxTrip) {
				newUnplanned.add(tripInfo);
			}
		}
		newUnplanned.add(maxTrip);

		return solveHelp(newUnplanned, new LinkedTripPlanning(instance));
	}

	/**
	 * Löst das Problem rekursiv.<br />
	 * Der Parameter enthält als Planung genau die Fahrtenplanung für die Streckenfahrten oberhalb in der
	 * Hierarchie.
	 *
	 * @param unplanned Fahrten, welche noch zu Planen sind.
	 * @param planning  Teilplanung für die bisherigen Züge
	 * @return Um die vorher ungeplanten Züge ergänzte Planung
	 */
	private TripPlanning solveHelp(List<TripInformation> unplanned, TripPlanning planning) {
		if (unplanned.isEmpty()) {
			return planning;
		}
		TripInformation ourTripInfo = unplanned.get(0);
		List<TripInformation> otherTrips = unplanned.subList(1, unplanned.size());

		TripPlanning bestPlanning = null;
		// Verworfener Optimierungsansatz: tryBest = true annehmen (?: rausschmeißen) (bringt nix)
		double bestMaxPower = tryBest ? Double.POSITIVE_INFINITY : -1;
		for (int departure : ourTripInfo.getConsideredDepartures()) {
			planning = planning.withDeparture(ourTripInfo.getTrip(), departure);
			// Verworfener Optimierungsansatz: Nächste Zeile in extra lokaler Variable speichern (bringt nix)
			planning = solveHelp(otherTrips, planning);

			// TripPlanning wird sich ggf. um die rekursive Erstellung der richtigen PowerProfiles kümmern.
			// Eigentlich müssen so mehr Objekte erstellt werden, aber es so ists schneller.
			//BinaryPowerProfile prof = unplanned.size() == 1 ? planning.getPowerProfileMaximumOnly() : planning.getPowerProfile();
			PowerProfile prof = planning.getPowerProfileMaximumOnly();
			int ourMaxTime = prof.getMaximumTime();
			double ourMaxPower = prof.getPowerAt(ourMaxTime);
			if (tryBest ? (ourMaxPower < bestMaxPower) : (ourMaxPower > bestMaxPower)) {
				bestPlanning = planning;
				bestMaxPower = ourMaxPower;
			}
		}

		return bestPlanning;
	}

	@Override
	public String toString() {
		return "BruteforceSolver";
	}
}
