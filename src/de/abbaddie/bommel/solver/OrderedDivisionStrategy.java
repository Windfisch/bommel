/*
 * Copyright (c) 2014, Florian Jung, Benjamin Miller, Johannes Thürauf, David Wägner
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package de.abbaddie.bommel.solver;

import de.abbaddie.bommel.Bommel;
import de.abbaddie.bommel.data.Instance;
import de.abbaddie.bommel.data.Trip;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * <p>Die erste und bislang einzige implementierte Aufteilungsstrategie für
 * {@link de.abbaddie.bommel.solver.DivideAndConquerSolver}.<br />
 * Die Züge werden nach ihren Abfahrtszeiten sortiert und dann werden nacheinander Züge in die Gruppen entnommen.<br />
 * Es wird versucht, die Gruppengröße möglichst gleich zu halten, d.h. 10 Züge mit Gruppengröße 4 wird auf Gruppen der
 * Größe 4, 3 und 3 aufgeteilt.</p>
 *
 * @see DivideAndConquerSolver
 */
public class OrderedDivisionStrategy implements DivisionStrategy {
	@Override
	public List<Subproblem> divide(Instance instance, List<Trip> trips, int groupSize) {
		int nTrips = trips.size();
		int nShortGroups;
		if (nTrips % groupSize == 0)
			nShortGroups = 0;
		else
			nShortGroups = groupSize - (nTrips % groupSize);
		int nLongGroups = (nTrips + groupSize - 1) / groupSize - nShortGroups;

		Bommel.debug("divide " + trips.size() + " trips into groups of " + groupSize);
		Bommel.debug("will need " + nLongGroups + " normal groups and " + nShortGroups + " short groups");

		if (nLongGroups < 0)
			return divide(instance, trips, groupSize - 1);

		List<Trip> sortedTrips = new ArrayList<Trip>(trips);
		Collections.sort(sortedTrips, new Comparator<Trip>() {
			public int compare(Trip first, Trip second) {
				return (first.getAllowedDepartureEarliest() + first.getAllowedDepartureLatest()) / 2 -
						(second.getAllowedDepartureEarliest() + second.getAllowedDepartureLatest()) / 2;
			}
		});

		int i = 0;
		List<Trip> currTripGroup = new ArrayList<>();
		List<Subproblem> subproblems = new ArrayList<>();

		for (Trip iterTrip : sortedTrips) {
			currTripGroup.add(iterTrip);

			i++;
			if (i >= groupSize) {
				i = 0;
				subproblems.add(new Subproblem(instance, currTripGroup));
				currTripGroup = new ArrayList<>();

				if (subproblems.size() == nLongGroups) {
					groupSize = groupSize - 1;
				}
			}
		}

		return subproblems;
	}
}
