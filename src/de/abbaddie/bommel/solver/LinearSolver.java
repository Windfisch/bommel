/*
 * Copyright (c) 2014, Florian Jung, Benjamin Miller, Johannes Thürauf, David Wägner
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package de.abbaddie.bommel.solver;

import de.abbaddie.bommel.data.*;

import java.math.BigInteger;
import java.util.*;

/**
 * <p>Ein heuristische Löser, welcher die lineare Heuristik anwendet.<br />
 * Es wird dabei ein Greedy-Algorithmus verwendet, welcher mit einer leeren Prälösung beginnt und nach und nach Züge
 * hinzufügt, sodass der sich ergebende Zwischenstand minimal ist.<br />
 * Es werden in jedem Schritt möglichst viele Fahrten hinzugefügt, aber (normalerweise) höchstens so viele, dass die
 * gesetzte maximale Flexibilität nicht überschritten wird. Ausnahmen können sich ergeben, wenn ein Zug aus so vielen
 * Fahrten besteht, dass allein diese die Flexibiltität überschreiten, da immer alle Fahrten eines Zuges in einem
 * Schritt bearbeitet werden. <br />
 * Die Laufzeit beträgt O(n*M), wobei M das Maximum der Flexibilität aller Fahrten eines Zuges ist. Typischerweise
 * wird die Zahl der Fahrten pro Zug und die Zahl der Abfahrtsmöglichkeiten pro Fahrt beschränkt sein, weswegen M als
 * beschränkt angenommen werden kann und sich O(n) ergibt. (Bei AS1 sowieso)<br /></p>
 * <p/>
 * <p>Dieser Löser ist AS2-fähig.</p>
 */
public class LinearSolver implements ParentSolver {
	private Instance instance;
	private BigInteger maxFlexibility;
	private Solver smallSolver;

	/**
	 * Erstellt einen neuen {@link LinearSolver}. Dieser sollte noch über die set-Methoden konfiguriert werden.
	 */
	public LinearSolver() {

	}

	/**
	 * Erstellt einen neuen {@link LinearSolver} mit einer gegebenen Probleminstanz.
	 * Als Teilproblemlöser wird {@link GurobiSolver} ohne Ausgabe verwendet.
	 * Die Flexibilitätsobergrenze wird auf 10^30 gesetzt.
	 */
	public LinearSolver(Instance instance) {
		this.instance = instance;
		this.smallSolver = new GurobiSolver(instance, false, false);
		this.maxFlexibility = BigInteger.TEN.pow(30);
	}

	@Override
	public void setInstance(Instance instance) {
		this.instance = instance;
		if(smallSolver != null) {
			smallSolver.setInstance(instance);
		}
	}

	@Override
	public void setChildSolver(Solver solver) {
		this.smallSolver = solver;
	}

	/**
	 * Setzt die Flexibilitätsschranke für die einzelnen Schritte.
	 *
	 * @param maxFlexibility Maximale Flexibilität
	 */
	public void setFlexibility(String maxFlexibility) {
		this.maxFlexibility = BigInteger.TEN.pow(Integer.parseInt(maxFlexibility, 10));
	}

	@Override
	public TripPlanning solve(List<TripInformation> trips) {
		// Sortiere zunächst die Abfahrten nach Flexibilität (und, falls die Flex. gleich ist, kommen die Fahrten
		//  die weiter am Rand liegen, später, dies ist erfahrungsgemäß ganz gut)
		List<TripInformation> sorted = new ArrayList<>(trips);
		final int mid = instance.getSchedulingInterval() / 2;
		Collections.sort(sorted, new Comparator<TripInformation>() {
			@Override
			public int compare(TripInformation trip, TripInformation trip2) {
				int depdiff = trip.getConsideredDepartures().length - trip2.getConsideredDepartures().length;
				if (depdiff != 0) return depdiff;

				return Math.abs(mid - trip.getConsideredDeparturesMid())
						- Math.abs(mid - trip2.getConsideredDeparturesMid());
			}
		});

		// Enthält jeweils die Lösung aus dem vorherigen Schritt
		TripPlanning planning = new LinkedTripPlanning(instance);

		ListIterator<TripInformation> iterator = sorted.listIterator();
		Set<Trip> doneTrips = new HashSet<>();
		SuperTrip superTrip = new SuperTrip(Collections.<Trip>emptyList(), new MutableTripPlanning(instance));
		// Diese äußere Schleife durchläuft die einzelnen Schritte
		while (iterator.hasNext()) {
			BigInteger currFlexibility = BigInteger.ONE;
			List<TripInformation> selectedTrips = new ArrayList<>();

			// Probiere in diesem einzelnen Schritt nach und nach Züge durch und füge hinzu, solange sie
			//  Flexibilitätsgrenze nicht überschreiten (aber mindestens einen, und ggf. darüber, falls es zu viele
			//  Fahrten eines Zuges gibt)
			while (iterator.hasNext()) {
				TripInformation oneTripInfo = iterator.next();

				// Bereits bearbeitet (vermutlich durch eine andere Fahrt des gleichen Zuges)
				if (doneTrips.contains(oneTripInfo.getTrip())) {
					continue;
				}

				// Suche alle Fahrten des aktuellen Zuges raus (für AS2)
				BigInteger ourFlexibility = BigInteger.ONE;
				List<TripInformation> toAdd;
				if (oneTripInfo.getTrip() instanceof RealTrip) {
					toAdd = TripInformation.asTripInformationList(((RealTrip)oneTripInfo.getTrip()).getTrain().getTrips());
					for (TripInformation info : toAdd) {
						ourFlexibility = ourFlexibility.multiply(BigInteger.valueOf(info.getConsideredDepartures().length));
					}
				} else {
					toAdd = Arrays.asList(oneTripInfo);
				}

				// Ggfs. hinzufügen
				BigInteger newFlexibility = currFlexibility.multiply(ourFlexibility);
				if (newFlexibility.compareTo(maxFlexibility) > 0 && !selectedTrips.isEmpty()) {
					// Das wäre jetzt zu viel des Guten
					iterator.previous();
					break;
				} else {
					currFlexibility = newFlexibility;
					selectedTrips.addAll(toAdd);
					for (TripInformation tripInfo : toAdd) {
						doneTrips.add(tripInfo.getTrip());
					}
				}
			}

			// Füge den SuperTrip mit consideredDepartures=[0] hinzu, d.h. dessen
			// Position ist fixiert und darf nicht angefasst werden.
			List<TripInformation> selectedTripsWithSuperTrip = new ArrayList<>(selectedTrips);
			selectedTripsWithSuperTrip.add(new TripInformation(superTrip, 0));

			// Lösen.
			TripPlanning smallSolution = smallSolver.solve(selectedTripsWithSuperTrip);

			// Speichere die bisher bearbeiteten Züge in einem SuperTrip, welcher im nächsten Schritt wiederverwendet
			//  wird
			superTrip = new SuperTrip(TripInformation.asTripList(selectedTripsWithSuperTrip), smallSolution);

			// Speichere die Abfahrtszeiten der einzelnen Fahrten in der Planung
			for (TripInformation tripInformation : selectedTrips) {
				planning = planning.withDeparture(tripInformation.getTrip(), smallSolution.getDeparture(tripInformation.getTrip()));
			}

			// Fortschrittsanzeige
			System.out.print("LinearSolver: " + (doneTrips.size() * 100 / trips.size()) + "% (" + doneTrips.size() + "/" + trips.size() + ")\r");
		}

		// Fortschrittsanzeige
		System.out.println("LinearSolver: " + (doneTrips.size() * 100 / trips.size()) + "% (" + doneTrips.size() + "/" + trips.size() + ")");

		return planning;
	}
}
