/*
 * Copyright (c) 2014, Florian Jung, Benjamin Miller, Johannes Thürauf, David Wägner
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package de.abbaddie.bommel.solver;

import de.abbaddie.bommel.data.Instance;
import de.abbaddie.bommel.data.TripInformation;
import de.abbaddie.bommel.data.TripPlanning;
import de.abbaddie.bommel.improver.ComplexImprover;

import java.math.BigInteger;
import java.util.List;

/**
 * <p>Ruft die Nachiteration auf.<br />
 * <p/>
 * Enthält keine eigene Logik, alles was bei der Nachiteration passiert befindet sich im <i>improver</i>-Paket.
 * Aufgerufen wird davon zunächst {@link de.abbaddie.bommel.improver.ComplexImprover}.
 * <br />
 * Diese Klasse wird v.a. zum Starten benötigt.</p>
 * <p/>
 * <p>Dieser Löser ist AS2-fähig.</p>
 *
 * @see de.abbaddie.bommel.improver.ComplexImprover
 */
public class ImproveSolver implements ParentSolver, ReusingSolver {
	private Instance instance;
	private TripPlanning reuse;
	private Solver solver;
	private BigInteger startFlex;
	private BigInteger maxFlex;
	private BigInteger flexStep;
	private int maxSkipLevel = 10;

	@Override
	public void setSolution(TripPlanning planning) {
		this.reuse = planning;
	}

	@Override
	public void setInstance(Instance instance) {
		this.instance = instance;
	}

	@Override
	public void setChildSolver(Solver solver) {
		this.solver = solver;
	}

	@Override
	public TripPlanning solve(List<TripInformation> trips) {
		ComplexImprover improver = new ComplexImprover(instance, solver);
		improver.setStartFlexibility(startFlex);
		improver.setEndFlexibility(maxFlex);
		improver.setMaxSkipLevel(maxSkipLevel);
		improver.setFlexibilityStep(flexStep);
		return improver.improve(instance.getTrips(), reuse);
	}

	/**
	 * Setzt die Anfangsflexibilität aus der Konfiguration.
	 *
	 * @param string Anfangsflexibilität
	 * @see de.abbaddie.bommel.improver.ComplexImprover#setStartFlexibility(java.math.BigInteger)
	 */
	public void setStartFlexibility(String string) {
		startFlex = BigInteger.TEN.pow(Integer.parseInt(string));
	}

	/**
	 * Setzt Endflexibilität aus der Konfiguration.
	 *
	 * @param string Endflexibilität
	 * @see de.abbaddie.bommel.improver.ComplexImprover#setEndFlexibility(java.math.BigInteger)
	 */
	public void setMaxFlexibility(String string) {
		maxFlex = BigInteger.TEN.pow(Integer.parseInt(string));
	}

	/**
	 * Setzt das maximale Übersprunglevel aus der Konfiguration.
	 *
	 * @param string Maximales Übersprunglevel
	 * @see de.abbaddie.bommel.improver.ComplexImprover#setMaxSkipLevel(int)
	 * @see de.abbaddie.bommel.improver.SelectionStrategy#setSkipLevel(int)
	 */
	public void setMaxSkipLevel(String string) {
		maxSkipLevel = Integer.parseInt(string);
	}
	
	/**
	 * Setzt den Flexibilität, um die pro Schitt erhöht wird.
	 *
	 * @param string Flexibilitätserhöhung
	 * @see de.abbaddie.bommel.improver.ComplexImprover#setFlexibilityStep(java.math.BigInteger)    
	 */
	public void setFlexibilityStep(String string) {
		this.flexStep = BigInteger.TEN.pow(Integer.parseInt(string));
	}
}
