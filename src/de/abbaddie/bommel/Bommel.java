/*
 * Copyright (c) 2014, Florian Jung, Benjamin Miller, Johannes Thürauf, David Wägner
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package de.abbaddie.bommel;

import de.abbaddie.bommel.data.*;
import de.abbaddie.bommel.solver.NaiveSolver;
import de.abbaddie.bommel.solver.ReusingSolver;
import de.abbaddie.bommel.solver.Solver;
import de.abbaddie.bommel.util.*;

import java.util.Arrays;

/**
 * Startet das Programm.
 */
public class Bommel {
	private static boolean debug;

	/**
	 * Java-Hauptmethode.
	 * @param args Kommandozeilenparameter
	 * @throws Exception Bei falschen Aufrufparametern   
	 */
	public static void main(String[] args) throws Exception {
		// Debug-Flag setzen
		debug = Arrays.asList(args).contains("--debug");

		// Zusatzfunktionen, die parametergesteuert irgendwelche Zusatzsachen machen, aber nicht lösen. 
		TestUtils.maybeDoTest(args);
		PlotUtils.maybeGenPlots(args);
		BenchmarkUtils.maybeDoBenchmark(args);

		// Instanz einlesen, Rausschreibeteil initialisieren, damit die Lösung nachher in die richtige Datei kommt
		InstParser parser = new InstParser(FilenameUtil.instanceFileName(args));
		parser.parse();
		Instance instance = parser.getInstance();
		SolutionWriter writer = new SolutionWriter(instance, parser);

		// Versuche, die Startlösung zu holen
		TripPlanning firstSolution = null;
		String solutionFileName = FilenameUtil.getFirstSolutionFileName(args);
		if (solutionFileName != null) {
			try {
				firstSolution = parser.readSolution(solutionFileName);
			} catch (Exception ignored) {

			}
		}

		// Ggf. warten (um Debugger/Profiler einzubinden oder so)
		maybeWait(args);

		// Starte Kommandozeileninterface und Autospeichern
		new Cli(instance, writer).start();
		new AutoSaver(writer).start();

		// Hole den Löser, zeige Vergleichswerte und berechne die Lösung
		Solver solver = ConfigurationHelper.getSolver(instance, firstSolution, args[0]);

		showNaiveComparison(instance);
		maybeShowFirstSolution(solver, firstSolution);

		TripPlanning finalSolution = solve(instance, solver);
		instance.setCurrentSolution(finalSolution);

		// Zeige das Endergebnis und speichere es
		printFinalSolution(finalSolution);

		writer.writeCurrentSolution();
	}

	/**
	 * Zeige vor dem eigentlichen Lösen die naiven Lösungen, zum Vergleich.
	 */
	private static void showNaiveComparison(Instance instance) {
		NaiveSolver naiveSolver1 = new NaiveSolver(instance, true);
		TripPlanning naiveSolution1 = naiveSolver1.solve(TripInformation.asTripInformationList(instance.getTrips()));
		int maxTimeNaive1 = naiveSolution1.getPowerProfile().getMaximumTime();
		double maxPowerNaive1 = naiveSolution1.getPowerProfile().getPowerAt(maxTimeNaive1);

		NaiveSolver naiveSolver3 = new NaiveSolver(instance, false);
		TripPlanning naiveSolution3 = naiveSolver3.solve(TripInformation.asTripInformationList(instance.getTrips()));
		int maxTimeNaive3 = naiveSolution3.getPowerProfile().getMaximumTime();
		double maxPowerNaive3 = naiveSolution3.getPowerProfile().getPowerAt(maxTimeNaive3);

		System.out.printf("Alle Fahrten ab Intervallanfang/-ende: %.3fMW/%.3fMW\n", maxPowerNaive1 / 1_000_000, maxPowerNaive3 / 1_000_000);
	}

	/**
	 * Wenn wir eine Startlösung haben und die verwendet wird, zeigen wir sie an.
	 *
	 * @param firstSolution Startlösung, darf <code>null</code> sein
	 * @param solver        Zu verwendender Löser
	 */
	private static void maybeShowFirstSolution(Solver solver, TripPlanning firstSolution) {
		if (firstSolution != null && solver instanceof ReusingSolver) {
			System.out.printf("Startlösung: %.3fMW\n", firstSolution.getPowerProfileMaximumOnly().getPowerAt(firstSolution.getPowerProfileMaximumOnly().getMaximumTime()) / 1_000_000);
		}
	}

	/**
	 * Löst das Problem.
	 *
	 * @param instance Probleminstanz.
	 * @param solver   Zu verwendender Löser
	 * @return Optimale oder nichtoptimale Lösung
	 */
	private static TripPlanning solve(Instance instance, Solver solver) {
		long start = System.nanoTime();

		TripPlanning solution = solver.solve(TripInformation.asTripInformationList(instance.getTrips()));
		solution = new MutableTripPlanning(solution);

		long end = System.nanoTime();

		System.out.println("Berechnungsdauer: " + Math.round((end - start) / 1_000_000) + "ms");
		System.out.println();
		return solution;
	}

	/**
	 * Zeigt manche Abfahrtszeiten sowie die maximale Leistungsaufnahme an.
	 *
	 * @param solution Ergebnis, z.B. aus {@link #solve(Instance, Solver)}
	 */
	private static void printFinalSolution(TripPlanning solution) {
		for (int i = 0; i < Math.min(5, solution.getInstance().getTrains().size()); i++) {
			String hints = "";
			Train train = solution.getInstance().getTrains().get(i);

			for (int j = 0; j < train.getTrips().size(); j++) {
				Trip trip = train.getTrips().get(j);

				int departure = solution.getDeparture(trip);
				if (departure < trip.getAllowedDepartureEarliest()) {
					hints += " [Abfahrt zu früh  um " + Integer.toString(trip.getAllowedDepartureEarliest() - departure) + "]";
				}
				if (departure > trip.getAllowedDepartureLatest()) {
					hints += " [Abfahrt zu spät um " + Integer.toString(departure - trip.getAllowedDepartureLatest()) + "]";
				}
				if (j > 0) {
					Trip before = train.getTrips().get(j - 1);
					int arrivalPlus = solution.getDeparture(before) + before.getOccupancyTime();
					if (arrivalPlus > departure) {
						hints += " [Überschneidung mit vorhergehender Fahrt um " + Integer.toString(arrivalPlus - departure) + "]";
					}
				}
				System.out.println("Abfahrt Zug #" + ((Train)train).getId() + "/" + j + ": Nach " + departure / 600 + " Minuten." + hints);
			}

		}
		if (solution.getInstance().getTrips().size() > 5) {
			System.out.println("... und Abfahrten weiterer " + (solution.getInstance().getTrains().size() - 5) + " Züge.");
		}

		System.out.println();

		int maxTime = solution.getPowerProfile().getMaximumTime();
		double maxPower = solution.getPowerProfile().getPowerAt(maxTime);

		System.out.printf("Maximum wird nach %.1fs erreicht mit %.3fMW.\n", (double)maxTime / 10, maxPower / 1_000_000);
		InstanceEstimator.showAverageIntervalPosition(solution);
	}

	/**
	 * Wartet ggf. eine gewisse Zeitspanne.
	 *
	 * @param args Kommandozeilenparameter
	 * @throws Exception Bei falscher Parametersyntax
	 */
	private static void maybeWait(String[] args) throws Exception {
		for (int i = 0; i < args.length; i++) {
			if ("--wait".equals(args[i])) {
				if (i == args.length - 1) {
					throw new Exception("--wait erwartet noch eine Zahl als Wartezeit");
				}
				long ms = Long.parseLong(args[i + 1]);

				System.out.println("Starte in " + ms + "ms…");
				Thread.sleep(ms);
				System.out.println("Starte…");
			}
		}
	}

	/**
	 * Gibt eine Debug-Nachricht über <i>syserr</i> aus, wenn Bommel mit <i>--debug</i> gestartet wurde
	 *
	 * @param message Debug-Nachricht
	 */
	public static void debug(String message) {
		if (debug) System.err.println(message);
	}
}
