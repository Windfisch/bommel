/*
 * Copyright (c) 2014, Florian Jung, Benjamin Miller, Johannes Thürauf, David Wägner
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package de.abbaddie.bommel.util;

import de.abbaddie.bommel.data.Instance;
import de.abbaddie.bommel.data.MutableTripPlanning;
import de.abbaddie.bommel.data.TripInformation;
import de.abbaddie.bommel.data.TripPlanning;
import de.abbaddie.bommel.improver.ComplexImprover;
import de.abbaddie.bommel.solver.GurobiSolver;
import de.abbaddie.bommel.solver.LinearSolver;
import de.abbaddie.bommel.solver.Solver;

import java.io.PrintWriter;
import java.math.BigInteger;
import java.util.Arrays;

/**
 * Hilfsmethoden für das Plotten.<br />
 * Das betrifft das Plotten mit Hilfe der Aufrufparameter <code>--plot-flex-vs-power</code>,
 * <code>--plot-time-vs-power</code> und <code>--plot-neigh-vs-power</code>, <b>nicht</b> das Plotten aus der
 * Kommandozeilenschnittstelle von Bommel heraus, welche in {@link Cli} behandelt wird.
 *
 * @see Cli
 */
public class PlotUtils {
	/**
	 * Führt je nach Kommandozeilenparameter einen Plotvorgang aus und beendet anschließend das Programm.
	 *
	 * @param args Kommandozeilenparameter
	 * @throws Exception Fehler bei der Ausführung
	 */
	public static void maybeGenPlots(String[] args) throws Exception {
		if (Arrays.asList(args).contains("--plot-flex-vs-power")) {
			genFlexVsPowerGraph(FilenameUtil.instanceFileName(args));
			System.exit(0);
		}

		if (Arrays.asList(args).contains("--plot-time-vs-power")) {
			genTimeVsPowerGraph(FilenameUtil.instanceFileName(args));
			System.exit(0);
		}

		if (Arrays.asList(args).contains("--plot-neigh-vs-power")) {
			genNeighboursVsPowerGraph(FilenameUtil.instanceFileName(args));
			System.exit(0);
		}

	}

	private static void genFlexVsPowerGraph(String inst_file) throws Exception {
		PrintWriter writer = new PrintWriter("flex_vs_power.data");
		writer.println("# using instance " + inst_file);
		writer.printf("flexibility\tinitial\tafter 10s\tafter 20s\tafter 40s\tafter 60s\tafter 2m\tafter 3.5m\tafter 5m\tafter 7.5m\tafter 10m\tafter 15min\tafter 20min\n\n");

		int[] timeLimits = {0, 10, 20, 40, 60, 120, 210, 300, 450, 600, 900, 1200};

		// Instanz parsen
		InstParser parser = new InstParser(inst_file);
		parser.parse();
		Instance instance = parser.getInstance();

		// Lösung errechnen
		Solver solver = new LinearSolver(instance);
		TripPlanning initialSolution;
		initialSolution = solver.solve(TripInformation.asTripInformationList(instance.getTrips()));
		int maxTime2 = initialSolution.getPowerProfile().getMaximumTime();
		double maxPowerInitial = initialSolution.getPowerProfile().getPowerAt(maxTime2);

		for (int exp = 2; exp < 110 * 2; exp++) {
			TripPlanning solution = new MutableTripPlanning(initialSolution);

			BigInteger flex;
			if (exp % 2 == 0)
				flex = BigInteger.valueOf(10).pow(exp / 2);
			else
				flex = BigInteger.valueOf(10).pow((exp - 1) / 2).multiply(BigInteger.valueOf(3));

			System.err.println("flex = " + flex.toString());


			writer.printf("%s\t%f", flex.toString(), maxPowerInitial);
			writer.flush();

			// Lösung Verbessern
			ComplexImprover serious = new ComplexImprover(instance, new GurobiSolver());
			serious.setFixedFlexibility(flex);

			long startTime = System.nanoTime();
			double oldMaxPower = maxPowerInitial;
			for (int i = 1; i < timeLimits.length; i++) {
				System.err.println("\t" + i);

				int timeElapsed = (int)((System.nanoTime() - startTime) / 1000000000);
				int timeToRun = timeLimits[i] - timeElapsed;

				if (timeToRun > 0)
					solution = serious.improveWithTimeLimit(instance.getTrips(), solution, timeToRun);
				else // kann eigentlich nicht passieren, weil i unten so lange inkrementiert wird, bis timeLimits[++i] >= elapsed ist
					System.err.println("skipping timeslot because timeToRun <= 0! this should never happen!");

				int timeElapsed2 = (int)((System.nanoTime() - startTime) / 1000000000);
				int j;
				for (j = i + 1; j < timeLimits.length; j++)
					if (timeLimits[j] >= timeElapsed2)
						break;
				// j ist jetzt auf dem ersten timeLimits-Eintrag, der >= elapsed ist (abgesehen von i selbst im Falle eines vorzeitigen Endes des Improvers)
				j--;
				// j ist jetzt auf dem letzten timeLimits-Eintrag, der < elapsed ist, mindestens aber i.
				// assert(j >= i); assert(j < timeLimits.length);
				for (; i < j; i++) // falls j > i, rücke vor und trage das alte Ergebnis ein. falls j = i (Normalfall), tue nichts.
				{
					System.err.println("skipping a timeslot");
					writer.printf("\t%f", oldMaxPower);
				}

				int maxTime = solution.getPowerProfile().getMaximumTime();
				double maxPower = solution.getPowerProfile().getPowerAt(maxTime);

				writer.printf("\t%f", maxPower);
				writer.flush();

				oldMaxPower = maxPower;
			}
			writer.printf("\n");
			writer.flush();
		}


		writer.close();
	}

	private static void genNeighboursVsPowerGraph(String inst_file) throws Exception {
		PrintWriter writer = new PrintWriter("neigh_vs_power.data");
		writer.println("# using instance " + inst_file);
		writer.printf("flexibility\tinitial\tafter 30s\tafter 60s\tafter 2m\tafter 5m\tafter 10m\tafter 15min\tafter 30min\n\n");

		int[] timeLimits = {0, 30, 60, 120, 300, 600, 900, 1800};

		// Instanz parsen
		InstParser parser = new InstParser(inst_file);
		parser.parse();
		Instance instance = parser.getInstance();

		// Lösung errechnen
		Solver solver = new LinearSolver(instance);
		TripPlanning initialSolution;
		initialSolution = solver.solve(TripInformation.asTripInformationList(instance.getTrips()));
		int maxTime2 = initialSolution.getPowerProfile().getMaximumTime();
		double maxPowerInitial = initialSolution.getPowerProfile().getPowerAt(maxTime2);

		for (int neigh = 0; neigh < 20; neigh++) {
			TripPlanning solution = new MutableTripPlanning(initialSolution);

			System.err.println("neigh = " + neigh);

			writer.printf("%d\t%f", neigh, maxPowerInitial);
			writer.flush();

			// Lösung Verbessern
			ComplexImprover serious = new ComplexImprover(instance, new GurobiSolver(), neigh);

			long startTime = System.nanoTime();
			double oldMaxPower = maxPowerInitial;
			for (int i = 1; i < timeLimits.length; i++) {
				System.err.println("\t" + i);

				int timeElapsed = (int)((System.nanoTime() - startTime) / 1000000000);
				int timeToRun = timeLimits[i] - timeElapsed;

				if (timeToRun > 0)
					solution = serious.improveWithTimeLimit(instance.getTrips(), solution, timeToRun);
				else // kann eigentlich nicht passieren, weil i unten so lange inkrementiert wird, bis timeLimits[++i] >= elapsed ist
					System.err.println("skipping timeslot because timeToRun <= 0! this should never happen!");

				int timeElapsed2 = (int)((System.nanoTime() - startTime) / 1000000000);
				int j;
				for (j = i + 1; j < timeLimits.length; j++)
					if (timeLimits[j] >= timeElapsed2)
						break;
				// j ist jetzt auf dem ersten timeLimits-Eintrag, der >= elapsed ist (abgesehen von i selbst im Falle eines vorzeitigen Endes des Improvers)
				j--;
				// j ist jetzt auf dem letzten timeLimits-Eintrag, der < elapsed ist, mindestens aber i.
				// assert(j >= i); assert(j < timeLimits.length);
				for (; i < j; i++) // falls j > i, rücke vor und trage das alte Ergebnis ein. falls j = i (Normalfall), tue nichts.
				{
					System.err.println("skipping a timeslot");
					writer.printf("\t%f", oldMaxPower);
				}

				int maxTime = solution.getPowerProfile().getMaximumTime();
				double maxPower = solution.getPowerProfile().getPowerAt(maxTime);

				writer.printf("\t%f", maxPower);
				writer.flush();

				oldMaxPower = maxPower;
			}
			writer.printf("\n");
			writer.flush();
		}


		writer.close();
	}

	private static void genTimeVsPowerGraph(String inst_file) throws Exception {
		PrintWriter otherwriter = new PrintWriter("time_vs_power.gnuplot");
		otherwriter.println("set termoption enhanced");

		PrintWriter writer = new PrintWriter("time_vs_power.data");
		writer.println("# using instance " + inst_file);
		writer.printf("time\tpower\n\n");

		// Instanz parsen
		InstParser parser = new InstParser(inst_file);
		parser.parse();
		Instance instance = parser.getInstance();

		// Lösung errechnen
		Solver solver = new LinearSolver(instance);
		TripPlanning solution;
		solution = solver.solve(TripInformation.asTripInformationList(instance.getTrips()));
		solution = new MutableTripPlanning(solution);

		ComplexImprover improver = new ComplexImprover(instance, new GurobiSolver());

		long startTime = System.nanoTime();

		BigInteger oldFlex = BigInteger.valueOf(0);

		while (true) {
			int timeElapsed = (int)((System.nanoTime() - startTime) / 1000000000);

			BigInteger currFlex = improver.getCurrentFlexibility();
			if (oldFlex.compareTo(currFlex) != 0) {
				otherwriter.printf("set arrow from %d,graph(0,0) to %d,graph(1,1) nohead;\n", timeElapsed, timeElapsed);
				otherwriter.printf("set label \"%f\" at %d,graph(0,0.015);\n", currFlex.doubleValue(), timeElapsed + 15);
				otherwriter.flush();

				oldFlex = currFlex;
			}

			int maxTime = solution.getPowerProfile().getMaximumTime();
			double maxPower = solution.getPowerProfile().getPowerAt(maxTime);

			writer.println(timeElapsed + "\t" + maxPower);
			writer.flush();

			if (timeElapsed > 600)
				break;

			solution = improver.improveWithTimeLimit(instance.getTrips(), solution, 1);
		}

		otherwriter.println("plot 'time_vs_power.data' using 1:2 with lines title 'Leistung';");
		otherwriter.println("");
		otherwriter.println("pause -1");

		otherwriter.close();
		writer.close();
	}
}
