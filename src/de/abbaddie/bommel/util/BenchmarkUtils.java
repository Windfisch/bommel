/*
 * Copyright (c) 2014, Florian Jung, Benjamin Miller, Johannes Thürauf, David Wägner
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package de.abbaddie.bommel.util;


import de.abbaddie.bommel.data.Instance;
import de.abbaddie.bommel.data.TripInformation;
import de.abbaddie.bommel.solver.GurobiSolver;
import de.abbaddie.bommel.solver.Solver;

import java.io.PrintWriter;
import java.math.BigInteger;
import java.util.Arrays;
import java.util.Locale;

/**
 * Hilfsmethoden für das Benchmarking.
 */
public class BenchmarkUtils {
	/**
	 * Führt je nach Kommandozeilenparameter das Benchmarking aus und beendet anschließend das Programm.
	 *
	 * @param args Kommandozeilenparameter
	 * @throws Exception Fehler bei der Ausführung
	 */
	public static void maybeDoBenchmark(String[] args) throws Exception {
		if (shouldDoBenchmark(args)) {
			doBenchmark();
			System.exit(1);
		}
	}

	private static boolean shouldDoBenchmark(String[] args) throws Exception {
		return Arrays.asList(args).contains("--benchmark");
	}

	private static void doBenchmark() throws Exception {
		System.out.println("Benchmark wird durchgeführt...");

		PrintWriter writer = new PrintWriter("benchmark.data");
		writer.printf("inst_no\tn_trips\tcomplexity\tcompl*len\ttime\n\n");

		for (int inst_no : (new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14})) {
			doBenchmarkOnInstance(inst_no, writer);
		}

		writer.close();
	}

	private static void doBenchmarkOnInstance(int instanceNumber, PrintWriter writer) throws Exception {
		// Instanz parsen
		InstParser parser = new InstParser(FilenameUtil.instanceFileName(String.valueOf(instanceNumber)));
		parser.parse();
		Instance instance = parser.getInstance();

		Solver solver = new GurobiSolver(instance, false);
		long begin = System.nanoTime();
		solver.solve(TripInformation.asTripInformationList(instance.getTrips()));
		long end = System.nanoTime();

		writer.printf(Locale.ENGLISH, "%d\t%d\t%f\t%f\t%f\n", instanceNumber, instance.getTrips().size(), InstanceEstimator.getComplexity(instance).doubleValue(), InstanceEstimator.getComplexity(instance).multiply(BigInteger.valueOf(instance.getSchedulingInterval())).doubleValue(), (end - begin) / 1000000000.0);
		writer.flush();
	}
}
