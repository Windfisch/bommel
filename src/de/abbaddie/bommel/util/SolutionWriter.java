/*
 * Copyright (c) 2014, Florian Jung, Benjamin Miller, Johannes Thürauf, David Wägner
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package de.abbaddie.bommel.util;

import de.abbaddie.bommel.data.Instance;
import de.abbaddie.bommel.data.MutableTripPlanning;
import de.abbaddie.bommel.data.TripPlanning;

import java.io.IOException;

/**
 * Wird genutzt, um Lösungen zu schreiben.
 */
public class SolutionWriter {
	private Instance instance;
	private InstParser parser;

	/**
	 * Erstellt einen neuen {@link SolutionWriter} mit den gegebnen Daten.
	 *
	 * @param instance Probleminstanz
	 * @param parser   Parser
	 */
	public SolutionWriter(Instance instance, InstParser parser) {
		this.instance = instance;
		this.parser = parser;
	}

	/**
	 * Gibt die zugeordnete Probleminstanz zurück.
	 * 
	 * @return Probleminstanz
	 */
	public Instance getInstance() {
		return instance;
	}

	/**
	 * Überprüft die Gültigkeit der aktuellen Lösung und schreibt die Lösung ggf. mit Zeitstempel. Anschließend erfolgt
	 * eine Ausgabe.
	 *
	 * @param withTimestamp Mit Zeitstempel oder ohne?
	 */
	public void writeCurrentSolution(boolean withTimestamp) {
		try {
			TripPlanning sol = new MutableTripPlanning(instance.getCurrentSolution());
			boolean valid = instance.verifyPlanning(sol);

			if (!valid) {
				System.err.println("Schreibe aktuelle Lösung NICHT, da sie ungültig ist.");
			} else {
				parser.writeSolution(sol, withTimestamp);
				System.err.println("Rausschreiben erfolgreich.");
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Überprüft die Gültigkeit der aktuellen Lösung und schreibt die Lösung gegebenenfalls. Anschließend erfolgt eine
	 * Ausgabe.
	 */
	public void writeCurrentSolution() {
		try {
			TripPlanning sol = new MutableTripPlanning(instance.getCurrentSolution());
			boolean valid = instance.verifyPlanning(sol);

			if (!valid) {
				System.err.println("Schreibe aktuelle Lösung NICHT, da sie ungültig ist.");
			} else {
				parser.writeSolution(sol);
				System.err.println("Rausschreiben erfolgreich.");
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
