/*
 * Copyright (c) 2014, Florian Jung, Benjamin Miller, Johannes Thürauf, David Wägner
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package de.abbaddie.bommel.util;

/**
 * Kümmert sich um das automatische Speichern der Lösung alle fünf Minuten.
 */
public class AutoSaver implements Runnable {
	// TODO: Konfigurierbarmachen
	private static final int SAVE_PERIOD = 5;

	private SolutionWriter writer;

	/**
	 * Erstellt einen neuen {@link AutoSaver} mit der gegebenen Speichermöglichkeit.<br />
	 * Nicht vergessen {@link #start()} aufzurufen!
	 *
	 * @param writer Speicherer
	 */
	public AutoSaver(SolutionWriter writer) {
		this.writer = writer;
	}

	/**
	 * Startet in einem neuen Thread.
	 */
	public void start() {
		Thread t = new Thread(this);
		t.setDaemon(true);
		t.setName("AutoSaver");
		t.start();
	}

	/**
	 * Startet im aktuellen Thread.
	 */
	@Override
	public void run() {
		long lastSave = System.currentTimeMillis();

		//noinspection InfiniteLoopStatement
		while (true) {
			try {
				if (writer.getInstance().getCurrentSolution() != null && System.currentTimeMillis() > lastSave + SAVE_PERIOD * 1000) {
					lastSave = System.currentTimeMillis();
					writer.writeCurrentSolution(true);
				}
				Thread.sleep(1000);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}
