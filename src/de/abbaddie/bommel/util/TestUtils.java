/*
 * Copyright (c) 2014, Florian Jung, Benjamin Miller, Johannes Thürauf, David Wägner
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package de.abbaddie.bommel.util;


import de.abbaddie.bommel.data.Instance;
import de.abbaddie.bommel.data.Trip;
import de.abbaddie.bommel.data.TripInformation;
import de.abbaddie.bommel.data.TripPlanning;
import de.abbaddie.bommel.improver.*;
import de.abbaddie.bommel.solver.*;

import java.io.File;
import java.math.BigInteger;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * Hilfsmethoden für das Testen.
 */
public class TestUtils {
	private static boolean selftest = false;
	private static Set<String> testsToExecute = new HashSet<String>();
	private static boolean genSelftest = false;

	/**
	 * Führt je nach Kommandozeilenparameter einen Test aus und beendet anschließend das Programm.
	 *
	 * @param args Kommandozeilenparameter
	 * @throws Exception Fehler bei der Ausführung
	 */
	public static void maybeDoTest(String[] args) throws Exception {
		setSelftest(args);
		setGenerateSelftest(args);

		if (selftest) {
			doSelftest();
			System.exit(1);
		}
	}

	private static void setSelftest(String[] args) throws Exception {
		for (int i = 0; i < args.length; i++) {
			if ("--test".equals(args[i])) {
				for (; i < args.length; i++)
					if (!args[i].startsWith("-"))
						testsToExecute.add(args[i]);

				selftest = true;
				return;
			}
		}
	}

	private static void setGenerateSelftest(String[] args) throws Exception {
		boolean safety = Arrays.asList(args).contains("--yes-i-have-verified-that-this-version-is-bugfree");
		boolean genSelftest_ = false;

		for (int i = 0; i < args.length; i++) {
			if ("--generate-testfiles".equals(args[i])) {
				for (; i < args.length; i++)
					if (!args[i].startsWith("-"))
						testsToExecute.add(args[i]);

				genSelftest_ = true;
				break;
			}
		}

		if (genSelftest_) {
			if (safety) {
				genSelftest = true;
				selftest = true;
			} else {
				System.err.println("Wurde angewiesen, die Vergleichsergebnisse für den Selbsttest mit neuen zu überschreiben; breche ab.");
				System.err.println("Stelle zuerst sicher, dass diese Version von Bommel *absolut* fehlerfrei arbeitet.");
				System.err.println("Dann übergebe zusätzlich das Flag --yes-i-have-verified-that-this-version-is-bugfree");
				System.exit(1);
			}
		}
	}

	private static void doSelftest() throws Exception {
		System.out.println("Selbsttest wird durchgeführt...");

		boolean success = true;

		success &= doTestSolvers(genSelftest);

		success &= doTestImprovers(genSelftest);


		System.out.println("\n");
		if (success)
			System.out.println("### alles ok :) ###\n");
		else
			System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n" +
					"!!!  Irgendwas lief schief!  !!!\n" +
					"!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n");
	}


	private static boolean doTestImprovers(boolean overwriteExpectedResults) throws Exception {
		boolean success = true;

		success &= doTestImprover(new SimpleImprover(null, new MostInfluenceSelectionStrategy(BigInteger.valueOf(10).pow(10)), new GurobiSolver(null, false)), new String[]{"2-21", "2-31", "2-40", "2-53", "15", "20", "25", "32"}, "improver/simple_mostinfluence_10", overwriteExpectedResults);
		success &= doTestImprover(new SimpleImprover(null, new SurroundingNegativePeakSelectionStrategy(BigInteger.valueOf(10).pow(10)), new GurobiSolver(null, false)), new String[]{"2-22", "2-33", "2-40", "2-50", "16", "21", "24", "32"}, "improver/simple_surroundingnegpeak_10", overwriteExpectedResults);
		success &= doTestImprover(new SimpleImprover(null, new MostFlexibleSelectionStrategy(BigInteger.valueOf(10).pow(10)), new GurobiSolver(null, false)), new String[]{"2-20", "2-32", "2-40", "2-51", "15", "19", "25", "32"}, "improver/simple_mostflexible_10", overwriteExpectedResults);
		success &= doTestImprover(new SimpleImprover(null, new LongestTripSelectionStrategy(BigInteger.valueOf(10).pow(10)), new GurobiSolver(null, false)), new String[]{"2-19", "2-33", "2-40", "2-52", "17", "22", "24", "32"}, "improver/simple_longesttrip_10", overwriteExpectedResults);
		success &= doTestImprover(new SimpleImprover(null, new MostExtremeTripSelectionStrategy(BigInteger.valueOf(10).pow(10)), new GurobiSolver(null, false)), new String[]{"2-20", "2-30", "2-40", "2-49", "14", "20", "25", "32"}, "improver/simple_mostextreme_10", overwriteExpectedResults);
		success &= doTestImproverHalf(new RandomImprover(null, new GurobiSolver(), BigInteger.valueOf(10).pow(10)), new String[]{"2-21", "2-29", "2-48", "15", "20", "25", "32"}, "improver/random_10");

		return success;
	}

	private static boolean doTestImproverHalf(Improver improver, String[] instances, String testName) throws Exception {
		if (!(testsToExecute.isEmpty() || testsToExecute.contains(testName) || testsToExecute.contains("improvers"))) {
			System.err.println("Überspringe halben Improver-Test '" + testName + "'.");
			return true;
		}
		System.err.println("Halber Improver-Test '" + testName + "' wird durchgeführt...");

		boolean success = true;

		for (String inst_no : instances) {
			InstParser parser = new InstParser(FilenameUtil.instanceFileName(inst_no));
			parser.parse();
			Instance instance = parser.getInstance();
			improver.setInstance(instance);

			TripPlanning originalPlanning;
			String solName = "selftest/startsolutions/lin" + inst_no + ".sol";
			File tmpFile = new File(solName);
			if (tmpFile.exists()) {
				System.err.println("using cached start solution");
				originalPlanning = parser.readSolution(solName);
			} else {
				Solver solver = new LinearSolver(instance);
				originalPlanning = solver.solve(TripInformation.asTripInformationList(instance.getTrips()));
				parser.writeSolution(originalPlanning, solName);
			}
			double originalMax = originalPlanning.getPowerProfile().getPowerAt(originalPlanning.getPowerProfile().getMaximumTime());

			if (!checkIfPlanningIsLegal(testName, inst_no, instance, originalPlanning, false)) {
				success = false;
				// does not happen[tm]
				System.err.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n" +
						"!!!!! Solver failed, expect breakage !!!!!\n" +
						"!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
			}


			TripPlanning actualPlanning = improver.improve(instance.getTrips(), originalPlanning);
			double actualMax = actualPlanning.getPowerProfile().getPowerAt(actualPlanning.getPowerProfile().getMaximumTime());

			if (originalMax < actualMax * 0.9999) {
				success = false;
				System.err.println("in Test '" + testName + "', Instanz " + inst_no + ": Improver hat das Ergebnis VERSCHLECHTERT! (" + actualMax + " vs. " + originalMax + ")");
			}

			success &= checkIfPlanningIsLegal(testName, inst_no, instance, actualPlanning);
		}

		return success;
	}

	private static boolean doTestImprover(Improver improver, String[] instances, String testName, boolean overwriteExpectedResult) throws Exception {
		if (!(testsToExecute.isEmpty() || testsToExecute.contains(testName) || testsToExecute.contains("improvers"))) {
			System.err.println("Überspringe Improver-Test '" + testName + "'.");
			return true;
		}
		System.err.println("Improver-Test '" + testName + "' wird durchgeführt...");

		boolean success = true;

		for (String inst_no : instances) {
			InstParser parser = new InstParser(FilenameUtil.instanceFileName(inst_no));
			parser.parse();
			Instance instance = parser.getInstance();
			improver.setInstance(instance);


			TripPlanning originalPlanning;
			String solName = "selftest/startsolutions/lin" + inst_no + ".sol";
			File tmpFile = new File(solName);
			if (tmpFile.exists()) {
				System.err.println("using cached start solution");
				originalPlanning = parser.readSolution(solName);
			} else {
				Solver solver = new LinearSolver(instance);
				originalPlanning = solver.solve(TripInformation.asTripInformationList(instance.getTrips()));
				parser.writeSolution(originalPlanning, solName);
			}
			double originalMax = originalPlanning.getPowerProfile().getPowerAt(originalPlanning.getPowerProfile().getMaximumTime());

			if (!checkIfPlanningIsLegal(testName, inst_no, instance, originalPlanning, false)) {
				// does not happen[tm]
				success = false;
				System.err.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n" +
						"!!!!! Solver failed, expect breakage !!!!!\n" +
						"!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
			}


			TripPlanning actualPlanning = improver.improve(instance.getTrips(), originalPlanning);
			double actualMax = actualPlanning.getPowerProfile().getPowerAt(actualPlanning.getPowerProfile().getMaximumTime());

			if (!overwriteExpectedResult) {
				TripPlanning expectedPlanning = parser.readSolution("selftest/" + testName + "/Instanz" + inst_no + ".sol");
				double expectedMax = expectedPlanning.getPowerProfile().getPowerAt(expectedPlanning.getPowerProfile().getMaximumTime());

				// Vergleiche die beiden Plannings
				for (Trip trip : instance.getTrips()) {
					int expectedDeparture = expectedPlanning.getDeparture(trip);
					int actualDeparture = actualPlanning.getDeparture(trip);

					if (expectedDeparture != actualDeparture) {
						success = false;
						System.err.println("in Test '" + testName + "', Instanz " + inst_no + ", bei Trip '" + trip + "': Errechnete Lösung unterscheidet sich von gespeicherter Lösung! (Differenz ist " + ((actualMax - expectedMax) / expectedMax) + "%)");
						break;
					}
				}

				if (expectedMax < actualMax * 0.9999) {
					success = false;
					System.err.println("in Test '" + testName + "', Instanz " + inst_no + ": Errechnetes Max ist SCHLECHTER als erwartet! (" + actualMax + " vs. " + expectedMax + ")");
				}
				if (expectedMax > actualMax * 1.0001) {
					success = false;
					System.err.println("in Test '" + testName + "', Instanz " + inst_no + ": Errechnetes Max ist BESSER als erwartet :) (" + actualMax + " vs. " + expectedMax + ")");
				}
			}

			if (originalMax < actualMax * 0.9999) {
				success = false;
				System.err.println("in Test '" + testName + "', Instanz " + inst_no + ": Improver hat das Ergebnis VERSCHLECHTERT! (" + actualMax + " vs. " + originalMax + ")");
			}

			success &= checkIfPlanningIsLegal(testName, inst_no, instance, actualPlanning);

			if (overwriteExpectedResult) {
				parser.writeSolution(actualPlanning, "selftest/" + testName + "/Instanz" + inst_no + ".sol");
			}
		}

		return success;
	}

	private static boolean doTestSolverHalf(Solver solver, String[] instances, String testName) throws Exception {
		if (!(testsToExecute.isEmpty() || testsToExecute.contains(testName) || testsToExecute.contains("solvers"))) {
			System.err.println("Überspringe halben Solver-Test '" + testName + "'.");
			return true;
		}
		System.err.println("Halber Solver-Test '" + testName + "' wird durchgeführt...");

		boolean success = true;
		for (String inst_no : instances) {
			InstParser parser = new InstParser(FilenameUtil.instanceFileName(inst_no));
			parser.parse();
			Instance instance = parser.getInstance();
			solver.setInstance(instance);

			TripPlanning actualPlanning = solver.solve(TripInformation.asTripInformationList(instance.getTrips()));

			success &= checkIfPlanningIsLegal(testName, inst_no, instance, actualPlanning);
		}

		return success;
	}


	private static boolean doTestSolvers(boolean overwriteExpectedResults) throws Exception {
		boolean success;

		success = doTestSolver(new BruteforceSolver(), new String[]{"1", "2", "3", "4"}, "solver/bruteforce", overwriteExpectedResults);
		success &= doTestSolver(new DivideAndConquerSolver(null, new BruteforceSolver(), 4, new OrderedDivisionStrategy()), new String[]{"1", "2", "3", "4", "5", "10", "15", "19"}, "solver/div_brute_ordered_4", overwriteExpectedResults);
		success &= doTestSolver(new LinearSolver(null), new String[]{"2-1", "2-2", "2-5", "2-10", "2-20", "2-30", "2-50", "2-61", "1", "2", "3", "4", "7", "13", "17", "23", "32"}, "solver/linear", overwriteExpectedResults);
		success &= doTestSolver(new GurobiSolver(null, false), new String[]{"2-1", "2-2", "2-4", "2-7", "2-10", "1", "2", "3", "4", "5", "10"}, "solver/gurobi", overwriteExpectedResults);
		success &= doTestSolver(new NaiveSolver(null, true), new String[]{"1", "2", "3", "11", "20", "32"}, "solver/naive0", overwriteExpectedResults);
		//success &= doTestSolver(new NaiveSolver(null, 0.42), new String[]{"1","2","3","12","21","32"}, "solver/naive42", overwriteExpectedResults);
		success &= doTestSolver(new NaiveSolver(null, false), new String[]{"1", "2", "3", "13", "22", "31"}, "solver/naive100", overwriteExpectedResults);
		success &= doTestSolverHalf(new RandomSolver(), new String[]{"1", "4", "8", "14", "17", "23", "24", "27"}, "solver/random");
		success &= doTestSolverHalf(new MoreRandomSolver(), new String[]{"1", "6", "7", "9", "16", "18", "32"}, "solver/morerandom");

		return success;
	}

	private static boolean doTestSolver(Solver solver, String[] instances, String testName, boolean overwriteExpectedResult) throws Exception {
		if (!(testsToExecute.isEmpty() || testsToExecute.contains(testName) || testsToExecute.contains("solvers"))) {
			System.err.println("Überspringe Solver-Test '" + testName + "'.");
			return true;
		}
		System.err.println("Solver-Test '" + testName + "' wird durchgeführt...");

		boolean success = true;
		for (String inst_no : instances) {
			InstParser parser = new InstParser(FilenameUtil.instanceFileName(inst_no));
			parser.parse();
			Instance instance = parser.getInstance();
			solver.setInstance(instance);

			TripPlanning actualPlanning = solver.solve(TripInformation.asTripInformationList(instance.getTrips()));
			if (actualPlanning == null) {
				success = false;
				System.err.println("in Test '" + testName + "', Instanz " + inst_no + ", Zurückgegebene Lösung ist null!");
				continue;
			}

			double actualMax = actualPlanning.getPowerProfile().getPowerAt(actualPlanning.getPowerProfile().getMaximumTime());

			if (!overwriteExpectedResult) {
				TripPlanning expectedPlanning = parser.readSolution("selftest/" + testName + "/Instanz" + inst_no + ".sol");
				double expectedMax = expectedPlanning.getPowerProfile().getPowerAt(expectedPlanning.getPowerProfile().getMaximumTime());

				// Vergleiche die beiden Plannings
				for (Trip trip : instance.getTrips()) {
					int expectedDeparture = expectedPlanning.getDeparture(trip);
					int actualDeparture = actualPlanning.getDeparture(trip);

					if (expectedDeparture != actualDeparture) {
						success = false;
						System.err.println("in Test '" + testName + "', Instanz " + inst_no + ", bei Trip '" + trip + "': Errechnete Lösung unterscheidet sich von gespeicherter Lösung! (Differenz ist " + ((actualMax - expectedMax) / expectedMax * 100) + "%)");
						break;
					}
				}

				if (expectedMax < actualMax * 0.9999) {
					success = false;
					System.err.println("in Test '" + testName + "', Instanz " + inst_no + ": Errechnetes Max ist SCHLECHTER als erwartet! (" + actualMax + " vs. " + expectedMax + ")");
				}
				if (expectedMax > actualMax * 1.0001) {
					success = false;
					System.err.println("in Test '" + testName + "', Instanz " + inst_no + ": Errechnetes Max ist BESSER als erwartet :) (" + actualMax + " vs. " + expectedMax + ")");
				}
			}

			success &= checkIfPlanningIsLegal(testName, inst_no, instance, actualPlanning);

			if (overwriteExpectedResult) {
				parser.writeSolution(actualPlanning, "selftest/" + testName + "/Instanz" + inst_no + ".sol");
			}
		}

		return success;
	}

	private static boolean checkIfPlanningIsLegal(String testName, String inst_no, Instance instance, TripPlanning actualPlanning) {
		return checkIfPlanningIsLegal(testName, inst_no, instance, actualPlanning, true);
	}

	private static boolean checkIfPlanningIsLegal(String testName, String inst_no, Instance instance, TripPlanning actualPlanning, boolean output) {
		return instance.verifyPlanning(actualPlanning, "in Test '" + testName + "', Instanz " + inst_no + ": ", output, 2);
	}
}
