/*
 * Copyright (c) 2014, Florian Jung, Benjamin Miller, Johannes Thürauf, David Wägner
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package de.abbaddie.bommel.util;

import com.panayotis.gnuplot.JavaPlot;
import com.panayotis.gnuplot.dataset.DataSet;
import com.panayotis.gnuplot.plot.DataSetPlot;
import com.panayotis.gnuplot.style.NamedPlotColor;
import com.panayotis.gnuplot.style.PlotColor;
import com.panayotis.gnuplot.style.PlotStyle;
import com.panayotis.gnuplot.style.Style;
import de.abbaddie.bommel.data.*;
import de.abbaddie.bommel.solver.NaiveSolver;

import java.util.Arrays;
import java.util.Scanner;

/**
 * Kümmert sich darum, im normalen Bommel-Betrieb Benutzereingaben aufzunehmen und durchzuführen.<br />
 * Cli = Command Line Interface = Kommandozeilenschnittstelle.<br />
 * Es stehen folgende Kommandos zur Verfügung:<ul>
 * <li><code>a</code>/<code>analyse</code>/<code>analyze</code>: Gibt gewisse Metriken für das Problem aus,</li>
 * <li><code>exit</code>: Beendet Bommel ohne zu Speichern,</li>
 * <li><code>p</code>/<code>plot</code>: Plottet die aktuelle Lösung im Vergleich mit der Startlösung,</li>
 * <li><code>c NR</code>/<code>characteristic NR</code>: Zeigt die Majorante für den Slot NR,</li>
 * <li><code>w</code>/<code>write</code>: Speichert die aktuelle Lösung und</li>
 * <li><code>plotmax</code>/<code>plotmin</code>: Plottet eine vollständige Majorante/Minorante im Vergleich zur
 * aktuellen Lösung (ist sehr berechnungsaufwändig und kann etwas dauern!).</li>
 * </ul>
 */
public class Cli implements Runnable {
	private Instance instance;
	private SolutionWriter writer;

	/**
	 * Erstellt ein neues {@link Cli}-Objekt mit den gegebenen Daten.
	 *
	 * @param instance Probleminstanz
	 * @param writer   Speichermöglichkeit
	 */
	public Cli(Instance instance, SolutionWriter writer) {
		this.instance = instance;
		this.writer = writer;
	}

	/**
	 * Startet die Cli in einem neuen Thread.
	 */
	public void start() {
		Thread t = new Thread(this);
		t.setDaemon(true);
		t.setName("Cli");
		t.start();
	}

	/**
	 * Startet die Cli im aktuellen Thread.
	 */
	@Override
	public void run() {
		Scanner s = new Scanner(System.in);
		Plotter p = new Plotter();

		while (true) {
			try {
				Thread.sleep(10);
				if (s.hasNext()) {
					switch (s.next()) {
						case "a":
						case "analyse":
						case "analyze":
							InstanceEstimator.estimate(instance);
							break;
						case "exit":
							System.exit(0);
							break;
						case "plot":
						case "p":
							p.plot();
							break;
						case "c":
						case "characteristic":
							int slot = s.nextInt(10);
							showCharacteristic(instance.getCurrentSolution(), slot);
							break;
						case "write":
						case "w":
							writer.writeCurrentSolution();
							break;
						case "plotmax":
							plot(getMaxProfile(instance));
							break;
						case "plotmin":
							plot(getMinProfile(instance));
							break;
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	private static float[] getMaxProfile(Instance instance) {
		float[] maxima = new float[instance.getSchedulingInterval()];

		for (int i = 0; i < instance.getSchedulingInterval(); i++) {
			for (Trip t : instance.getTrips()) {
				float bestThisTrip = Float.NEGATIVE_INFINITY;

				for (int departure : t.getAllowedDepartures()) {
					float ourPower = t.getPowerProfile(departure).getPowerAt(i);
					bestThisTrip = Math.max(bestThisTrip, ourPower);
				}

				maxima[i] += bestThisTrip;
			}
		}

		return maxima;
	}

	private static float[] getMinProfile(Instance instance) {
		float[] maxima = new float[instance.getSchedulingInterval()];

		for (int i = 0; i < instance.getSchedulingInterval(); i++) {
			for (Trip t : instance.getTrips()) {
				float bestThisTrip = Float.POSITIVE_INFINITY;

				for (int departure : t.getAllowedDepartures()) {
					float ourPower = t.getPowerProfile(departure).getPowerAt(i);
					bestThisTrip = Math.min(bestThisTrip, ourPower);
				}

				maxima[i] += bestThisTrip;
			}
		}

		return maxima;
	}

	private void showCharacteristic(TripPlanning sol, int slot) {
		float[] values = new float[instance.getSchedulingInterval() / 600];
		float max = Float.NEGATIVE_INFINITY;

		for (int i = slot; i < instance.getSchedulingInterval(); i += 600) {
			values[i / 600] = sol.getPowerProfile().getPowerAt(i);
			max = Math.max(max, values[i / 600]);
		}

		System.err.println("Charakteristik Slot #" + slot + ": " + Arrays.toString(values) + " = " + max);
	}

	private void plot(float[] profile) {
		JavaPlot plotter = new JavaPlot();

		DataSet set = new BommelDataSet(profile);

		DataSetPlot plot = new DataSetPlot(set);

		PlotStyle style = new PlotStyle();
		style.setStyle(Style.LINES);
		style.setLineType(NamedPlotColor.GREEN);
		plot.setPlotStyle(style);

		plotter.addPlot(plot);

		plotter.addPlot(getPlot(instance.getCurrentSolution(), NamedPlotColor.RED));

		plotter.plot();
	}

	private static DataSetPlot getPlot(TripPlanning planning, PlotColor color) {
		PowerProfile prof = planning.getPowerProfile();
		float[] array = prof.getArray();

		DataSet set = new BommelDataSet(array);
		DataSetPlot plot = new DataSetPlot(set);
		PlotStyle style = new PlotStyle();
		style.setStyle(Style.LINES);
		style.setLineType(color);
		plot.setPlotStyle(style);

		return plot;
	}

	private class Plotter {
		private JavaPlot plotter;
		private DataSetPlot first;

		public void plot() {
			if (instance.getCurrentSolution() == null) return;

			plotter = new JavaPlot();

			PowerProfile prof = instance.getCurrentSolution().getPowerProfile();

			if (first == null) {
				TripPlanning sol = new NaiveSolver(instance, true).solve(TripInformation.asTripInformationList(instance.getTrips()));
				first = getPlot(sol, NamedPlotColor.BLACK);
				first.setTitle("Naive Lösung");
			}
			plotter.addPlot(first);
			DataSetPlot plot = getPlot(instance.getCurrentSolution(), NamedPlotColor.RED);
			plot.setTitle("Bommel-Lösung");
			plotter.addPlot(plot);

			int maxTime = prof.getMaximumTime();
			double maxVal = prof.getPowerAt(maxTime);
			// mehrere Linien gehen leider nicht =(
			plotter.set("arrow", "from " + maxTime + ",-1000000000 to " + maxTime + "," + maxVal + " nohead lc rgb 'cyan'");
			plotter.set("xlabel", "yo");

			plotter.plot();
		}
	}

	private static class BommelDataSet implements DataSet {
		private float[] array;

		protected BommelDataSet(float[] array) {
			this.array = array;
		}

		@Override
		public int size() {
			return array.length;
		}

		@Override
		public int getDimensions() {
			return 1;
		}

		@Override
		public String getPointValue(int point, int dimension) {
			return Float.toString(array[point]);
		}
	}
}
