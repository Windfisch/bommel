/*
 * Copyright (c) 2014, Florian Jung, Benjamin Miller, Johannes Thürauf, David Wägner
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package de.abbaddie.bommel.util;

import de.abbaddie.bommel.data.*;

import java.math.BigInteger;
import java.util.Arrays;

/**
 * Zeigt verschiedene Kennzahlen an.
 */
public class InstanceEstimator {
	/**
	 * Führt die Schätzung und Ausgabe durch.
	 *
	 * @param instance Probleminstanz.
	 */
	public static void estimate(Instance instance) {
		TripPlanning planning = new MutableTripPlanning(instance);
		int totalFlexibility = 0;
		double combinations = 1;
		int lowestFlexibility = Integer.MAX_VALUE, highestFlexibility = 0;

		long totalTripTime = 0;
		int shortestTripTime = Integer.MAX_VALUE, longestTripTime = 0;

		for (Trip trip : instance.getTrips()) {
			planning = planning.withDeparture(trip, trip.getAllowedDepartureEarliest());
			int flexbility = trip.getAllowedDepartures().length;
			totalFlexibility += flexbility;
			combinations *= flexbility;
			lowestFlexibility = Math.min(lowestFlexibility, flexbility);
			highestFlexibility = Math.max(highestFlexibility, flexbility);

			totalTripTime += trip.getOccupancyTime();
			shortestTripTime = Math.min(shortestTripTime, trip.getOccupancyTime());
			longestTripTime = Math.max(longestTripTime, trip.getOccupancyTime());
		}

		Trip all = new SuperTrip(instance.getTrips(), planning);

		int relevantEnd = instance.getSchedulingInterval() / 600 * 600;
		float usedPower = 0;
		float[] usedPowerSlots = new float[600];

		PowerProfile prof = all.getPowerProfile(all.getAllowedDepartureEarliest());
		for (int i = prof.getStart(); i < prof.getEnd(); i++) {
			usedPower += prof.getPowerAt(i);
			if (i < relevantEnd) {
				usedPowerSlots[i % 600] += prof.getPowerAt(i);
			}
		}
		int usedPowerSlot = -1;
		float usedPowerSlotValue = 0;
		for (int i = 0; i < 600; i++) {
			if (usedPowerSlots[i] > usedPowerSlotValue) {
				usedPowerSlotValue = usedPowerSlots[i];
				usedPowerSlot = i;
			}
		}

		float usedPowerAvg = usedPower / instance.getSchedulingInterval();
		float usedPowerAvgSlot = usedPowerSlotValue * 600 / relevantEnd;

		float[] slotMaxima = getSlotMaxima(instance, usedPowerSlot);
		float[] slotMinima = getSlotMinima(instance, usedPowerSlot);
		float usedPowerSlotValueWithoutLower = usedPowerSlotValue;
		int usedPowerSlotsWithoutLower = relevantEnd;
		for (float slotMaximum : slotMaxima) {
			if (slotMaximum < usedPowerAvgSlot) {
				usedPowerSlotValueWithoutLower -= slotMaximum;
				usedPowerSlotsWithoutLower -= 600;
			}
		}
		float usedPowerAvgSlotWithoutLower = usedPowerSlotValueWithoutLower * 600 / usedPowerSlotsWithoutLower;

		System.out.println();
		System.out.println("Ausbaustufe " + (instance.getLevel() == Instance.ExtensionLevel.AS1 ? "1" : "2"));
		System.out.println("Anzahl Züge/Streckenfahrten: " + instance.getTrains().size() + "/" + instance.getTrips().size());
		System.out.printf("Untere Schranke #1: %.3fMW (Durchschnittslinie)\n", usedPowerAvg / 1_000_000);
		System.out.printf("Untere Schranke #2: %.3fMW (Durchschnittslinie in Slot #%d)\n", usedPowerAvgSlot / 1_000_000, usedPowerSlot);
		System.out.printf("Untere Schranke #3: %.3fMW (Durchschnittslinie in Slot #%d, Berücksichtigung unterdurchschnittlicher Werte)\n", usedPowerAvgSlotWithoutLower / 1_000_000, usedPowerSlot);
		System.out.printf("Maximalcharakteristik Slot #%d: " + Arrays.toString(slotMaxima) + "\n", usedPowerSlot);
		System.out.printf("Minimalcharakteristik Slot #%d: " + Arrays.toString(slotMinima) + "\n", usedPowerSlot);
		System.out.printf("Durchschnittliche Flexibilität: %.1f Abfahrtsmöglichkeiten (Min/Max: %d/%d)\n", (double)totalFlexibility / instance.getTrips().size(), lowestFlexibility, highestFlexibility);
		System.out.printf("Durchschnittliche Belegungszeit: %.1f Minuten (Min/Max: %d/%d)\n", (double)totalTripTime / instance.getTrips().size() / 600, shortestTripTime / 600, longestTripTime / 600);
		System.out.println("Planungslänge: " + (instance.getSchedulingInterval() - 1) / 600 + " Minuten (= " + instance.getSchedulingInterval() + " Bommelzeit)");
		System.out.printf("Planungsmöglichkeiten: %.3e (Ohne Einschränkungen)\n", combinations);
		System.out.println();
	}


	private static float[] getSlotMaxima(Instance instance, int slot) {
		int minutes = instance.getSchedulingInterval() / 600;
		float[] maxima = new float[minutes];

		for (int i = slot; i < instance.getSchedulingInterval(); i += 600) {
			for (Trip t : instance.getTrips()) {
				float bestThisTrip = Float.NEGATIVE_INFINITY;

				for (int departure : t.getAllowedDepartures()) {
					float ourPower = t.getPowerProfile(departure).getPowerAt(i);
					bestThisTrip = Math.max(bestThisTrip, ourPower);
				}

				maxima[i / 600] += bestThisTrip;
			}
		}

		return maxima;
	}

	private static float[] getSlotMinima(Instance instance, int slot) {
		int minutes = instance.getSchedulingInterval() / 600;
		float[] minima = new float[minutes];

		for (int i = slot; i < instance.getSchedulingInterval(); i += 600) {
			for (Trip t : instance.getTrips()) {
				float bestThisTrip = Float.POSITIVE_INFINITY;

				for (int departure : t.getAllowedDepartures()) {
					float ourPower = t.getPowerProfile(departure).getPowerAt(i);
					bestThisTrip = Math.min(bestThisTrip, ourPower);
				}

				minima[i / 600] += bestThisTrip;
			}
		}

		return minima;
	}

	/**
	 * Berechnet das arithmetische Mittel und Standardabweichung der Abfahrtszeiten in ihrem jeweiligen Intervall.
	 * Zeigt das Ergebnis anschließend an.
	 *
	 * @param solution Lösung
	 */
	public static void showAverageIntervalPosition(TripPlanning solution) {
		double sum = 0, mean = 0, varsum = 0;
		for (TripPlanning.Entry entry : solution) {
			Trip trip = entry.getTrip();
			if (trip.getAllowedDepartureEarliest() == trip.getAllowedDepartureLatest()) sum += 0.5;
			else
				sum += (double)(entry.getDeparture() - trip.getAllowedDepartureEarliest()) / (trip.getAllowedDepartureLatest() - trip.getAllowedDepartureEarliest());
		}
		mean = sum / solution.getInstance().getTrips().size();

		for (TripPlanning.Entry entry : solution) {
			Trip trip = entry.getTrip();
			if (trip.getAllowedDepartureEarliest() == trip.getAllowedDepartureLatest())
				varsum += (mean - 0.5) * (mean - 0.5);
			else {
				double tmp = ((double)(entry.getDeparture() - trip.getAllowedDepartureEarliest()) / (trip.getAllowedDepartureLatest() - trip.getAllowedDepartureEarliest())) - mean;
				varsum += tmp * tmp;
			}
		}
		System.out.printf("Durchschnittliche Abfahrtsposition im Intervall: %.1f%%, Standardabweichung: %.3f\n", mean * 100, Math.sqrt(varsum / solution.getInstance().getTrips().size()));
	}

	/**
	 * Berechnet die Zahl der Abfahrtsmöglichkeiten (ohne Beachtung von Einschränkungen).
	 *
	 * @param instance Probleminstanz
	 * @return Flexibilität
	 */
	public static BigInteger getComplexity(Instance instance) {
		BigInteger complexity = BigInteger.ONE;

		for (Trip trip : instance.getTrips())
			complexity = complexity.multiply(BigInteger.valueOf(trip.getAllowedDepartures().length));

		return complexity;
	}
}
