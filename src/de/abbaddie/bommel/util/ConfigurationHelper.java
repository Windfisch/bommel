/*
 * Copyright (c) 2014, Florian Jung, Benjamin Miller, Johannes Thürauf, David Wägner
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package de.abbaddie.bommel.util;

import de.abbaddie.bommel.data.Instance;
import de.abbaddie.bommel.data.TripPlanning;
import de.abbaddie.bommel.solver.ParentSolver;
import de.abbaddie.bommel.solver.ReusingSolver;
import de.abbaddie.bommel.solver.Solver;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.HierarchicalConfiguration;
import org.apache.commons.configuration.SubnodeConfiguration;
import org.apache.commons.configuration.XMLConfiguration;
import org.apache.commons.configuration.tree.xpath.XPathExpressionEngine;

import java.lang.reflect.Method;
import java.util.Iterator;

/**
 * <p>Dient dazu, die Konfiguration einzulesen.<br />
 * Die Konfiguration befindet sich in <code>bommel.xml</code>; ihr Format ist in <code>bommel.xsd</code>
 * spezifiziert.<br />
 * Wird Bommel mit <code>./bommel BLA 42</code> aufgerufen, wird in der Konfigurationsdatei ein Block
 * <code>&lt;solver id=&quot;BLA&quot;&gt;...&lt;/solver&gt;</code> gesucht und dieser genutzt.</p>
 * 
 * <p>Jeder solver-Abschnitt besteht aus folgenden Teilen:
 * <ul>
 * <li><i>class</i>: Der volle Klassenname. <i>de.abbaddie.bommel.solver.</i> vorne weg kann weggelassen werden.</li>
 * <li><i>properties</i> (optional): Eine Liste von Eigenschaften. Für jeden Eintrag
 * <code>&lt;foo&gt;bar&lt;/foo&gt;</code> wird im Löser dann eine Methode <code>setFoo(String arg0)</code> gesucht und
 * mit "bar" aufgerufen.</li>
 * <li><i>child-solver</i> (optional): Darf nur für Klassen vorhanden sein, die
 * {@link de.abbaddie.bommel.solver.ParentSolver} implementieren, es wird dann
 * {@link de.abbaddie.bommel.solver.ParentSolver#setChildSolver(de.abbaddie.bommel.solver.Solver)} aufgerufen. Dieser
 * Abschnitt ist wieder ein Löser-Abschnitt nach dem selben Aufbau; das Ganze ist also rekursiv.</li>
 * <li><i>reuse</i> (optional): Darf nur für Klassen vorhanden sein, die
 * {@link de.abbaddie.bommel.solver.ReusingSolver} implementieren, es wird dann
 * {@link de.abbaddie.bommel.solver.ReusingSolver#setSolution(de.abbaddie.bommel.data.TripPlanning)} aufgerufen mit
 * einer Startlösung, die neben der Instanzdatei gesucht wird (also gleicher Dateiname, bis auf <code>.sol</code> statt
 * <code>.inst</code>)</li>
 * </ul>
 * </p>
 */
public class ConfigurationHelper {
	private static HierarchicalConfiguration root;

	static {
		try {
			root = new XMLConfiguration("bommel.xml");
			root.setExpressionEngine(new XPathExpressionEngine());
		} catch (ConfigurationException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Konfiguriert einen Löser entsprechend der Konfiguration aus der Datei.
	 *
	 * @param instance      Probleminstanz
	 * @param firstSolution Startlösung, falls vorhanden; sonst <code>null</code>.
	 * @param name          Name (im obigen Beispiel <code>BLA</code>)
	 * @return Löser
	 */
	public static Solver getSolver(Instance instance, TripPlanning firstSolution, String name) {
		SubnodeConfiguration config = root.configurationAt("//solver[@id='" + name + "']");
		return getSolver(instance, firstSolution, config);
	}

	/**
	 * Konfiguriert einen Löser mit dem gegebenen Konfigurationsblock
	 *
	 * @param instance      Probleminstanz
	 * @param firstSolution Startlösung, falls vorhanden; sonst <code>null</code>.
	 * @param config        Konfigurationsabschnitt
	 * @return Löser
	 */
	@SuppressWarnings("unchecked")
	private static Solver getSolver(Instance instance, TripPlanning firstSolution, SubnodeConfiguration config) {
		try {
			String className = config.getString("class");
			if (!className.contains(".")) {
				className = "de.abbaddie.bommel.solver." + className;
			}
			Class<? extends Solver> clazz = (Class<? extends Solver>)Class.forName(className);
			Solver solver = clazz.newInstance();
			solver.setInstance(instance);

			if (solver instanceof ParentSolver) {
				SubnodeConfiguration childConfig = config.configurationAt("child-solver");
				Solver child = getSolver(instance, firstSolution, childConfig);
				((ParentSolver)solver).setChildSolver(child);
			}
			if (solver instanceof ReusingSolver && config.containsKey("reuse")) {
				((ReusingSolver)solver).setSolution(firstSolution);
			}

			try {
				SubnodeConfiguration propertiesConfig = config.configurationAt("properties");
				Iterator<String> keys = propertiesConfig.getKeys();
				while (keys.hasNext()) {
					String key = keys.next();
					String value = propertiesConfig.getString(key);

					if (!"class".equals(key)) {
						String setterName = "set" + ucfirst(key);
						Method m = clazz.getMethod(setterName, String.class);
						m.invoke(solver, value);
					}
				}
			} catch (IllegalArgumentException e) {
				//
			}

			return solver;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * Macht den ersten Buchstaben des Strings groß.
	 *
	 * @param s String
	 * @return String mit erstem Buchstaben als Großbuchstsaben.
	 */
	private static String ucfirst(String s) {
		return Character.toUpperCase(s.charAt(0)) + s.substring(1);
	}
}
