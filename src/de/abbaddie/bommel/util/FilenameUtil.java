/*
 * Copyright (c) 2014, Florian Jung, Benjamin Miller, Johannes Thürauf, David Wägner
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package de.abbaddie.bommel.util;

import java.util.Arrays;

/**
 * Kümmert sich darum, dass Dateinamen einheitlich behandelt werden.
 */
public class FilenameUtil {
	/**
	 * Gibt den Instanzdateinamen aus den Kommandozeilenparametern zurück.<br />
	 * Es wird der zweite Parameter verwendet. Ist dieser eine Zahl oder beginnt mit »2-«, wird dieser verwendet um eine
	 * Datei <code>instanzen/InstanzNR.inst</code> zu laden.
	 *
	 * @param args Kommandozeilenparameter
	 * @return Dateiname
	 */
	public static String instanceFileName(String[] args) {
		if (args.length <= 1) return "instanzen/Instanz1.inst";

		try {
			String instanceNo = (args[1].startsWith("2-") ? "2-" : "") + Integer.parseInt(args[1].substring(args[1].startsWith("2-") ? 2 : 0), 10);
			return instanceFileName(instanceNo);
		} catch (NumberFormatException e) {
			return args[1];
		}
	}

	/**
	 * Gibt den Instanzdateinamen für die gegebene Instanznummer zurück.
	 *
	 * @param instanceNo NR
	 * @return instanzen/InstanzNR.inst
	 */
	public static String instanceFileName(String instanceNo) {
		return "instanzen/Instanz" + instanceNo + ".inst";
	}

	/**
	 * Gibt den Lösungsdateinamen für die gegebene Instanznummer zurück.
	 *
	 * @param instanceNo NR
	 * @return instanzen/InstanzNR.sol
	 */
	public static String solutionFileName(String instanceNo) {
		return "instanzen/Instanz" + instanceNo + ".sol";
	}

	/**
	 * Gibt den Startlösungsdateinamen für die gegebenen Kommandozeilenparameter zurück.
	 *
	 * @param args Kommandozeilenparameter
	 * @return Dateiname falls Startlösung verwendet werden wird, sonst <code>null</code>.
	 */
	// TODO: if nötig?
	public static String getFirstSolutionFileName(String[] args) {
		if (Arrays.asList(args).contains("--reuse") || "improve".equals(args[0])) {
			return instanceFileName(args).replace(".inst", ".sol");
		}
		return null;
	}
}
