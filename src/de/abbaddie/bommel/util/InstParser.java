/*
 * Copyright (c) 2014, Florian Jung, Benjamin Miller, Johannes Thürauf, David Wägner
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package de.abbaddie.bommel.util;

import de.abbaddie.bommel.data.*;

import java.io.*;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Parst die .inst-Dateien und schreibt ggf. Ergebnisse raus.
 */
public class InstParser {
	private String fileName;
	private Instance instance;

	/**
	 * Erstellt einen neuen Parser für den Dateinamen.
	 *
	 * @param fileName Dateiname
	 * @throws FileNotFoundException
	 */
	public InstParser(String fileName) throws FileNotFoundException {
		this.fileName = fileName;
	}

	/**
	 * Liest eine Instanz aus einer .inst-Datei.
	 *
	 * @throws IOException Falls die Datei nicht wie erwartet formatiert ist.
	 */
	public void parse() throws IOException {
		// Erstelle das Scanner-Objekt, welches die Datei in Teile aufteilt.
		try (BommelScanner scanner = new BommelScanner(fileName)) {
			// Erstelle das Objekt, welches die zentralen Daten der Instanz speichert.
			instance = new Instance();

			// Temporaere Struktur, um die Zuege richtig den Strecken zuzuordnen.
			Map<Integer, Section> sections = new HashMap<>();

			// Planungsintervall. Beachte: Wir speichern in 0,1s-Schritten, also alles mal 10,
			//  und ein weiterer Wert da das Intervall abgeschlossen ist.
			//  Außerdem mal 60, da Angabe in Minuten.
			assertToken(scanner, "PLANUNGSINTERVALL");
			int readSchedulingInterval = scanner.nextInt();
			int ourSchedulingInterval = readSchedulingInterval * 60 * 10 + 1;
			instance.setSchedulingInterval(ourSchedulingInterval);

			// Anzahl der Züge.
			assertToken(scanner, "ANZAHL_ZUEGE");
			int numberOfTrains = scanner.nextInt();

			char[] spinningBars = {'|', '/', '-', '\\'};
			int spinningBar = 0;

			// Bearbeite nun die einzelnen Züge.
			for (int i = 0; i < numberOfTrains; i++) {
				if (i % 100 == 0) {
					spinningBar = (spinningBar + 1) % 4;
					System.out.printf("Lese Züge %c %3d%% (%d/%d)\r", spinningBars[spinningBar], (100 * i / numberOfTrains), i, numberOfTrains); // Fortschrittsindikator
				}


				Train train = new Train();
				instance.addTrain(train);

				// Zugnummer.
				assertToken(scanner, "ZUG_NR");
				int trainId = scanner.nextInt();
				train.setId(trainId);

				// Anzahl der Strecken für diesen Zug.
				assertToken(scanner, "ANZAHL_STRECKEN");
				int numberOfTrainSections = scanner.nextInt();

				for (int j = 0; j < numberOfTrainSections; j++) {
					// Streckennummer.
					assertToken(scanner, "STRECKEN_NR");
					int sectionId = scanner.nextInt();

					// Zunächst: Versuche die Strecke anhand der Nummer zu finden.
					//  Falls das fehlschlägt: Erstelle eine neue Strecke, setze ihre Nummer und mache sie für spätere
					//  Züge auffindbar.
					Section section = sections.get(sectionId);
					if (section == null) {
						section = new Section();
						section.setId(sectionId);
						sections.put(sectionId, section);
						instance.addSection(section);
					}

					// Erstelle nun eine Streckenfahrt und ordne sie dem Zug zu.
					RealTrip trip = new RealTrip();
					trip.setSection(section);
					trip.setTrain(train);
					train.addTrip(trip);
					section.addTrip(trip);
					instance.addTrip(trip);

					// Fahrtzeit. Auch hier wieder die Umrechnung auf bommel-Zeit beachten!
					// Hier speziell: Gegebene Angabe ist in Minuten, nicht Sekunden!
					// Wird erst später für das Leistungsprofil benötigt und nicht direkt im Zug gespeichert.
					assertToken(scanner, "FAHRZEIT");
					int readTripTime = scanner.nextInt();
					int ourTripTime = readTripTime * 60 * 10 + 1;

					// Mindesthaltezeit. Ist in AS1 nicht angegeben; dann wirft assertToken eine WrongTokenException
					try {
						assertToken(scanner, "MINDESTHALTEZEIT");
						int readDwell = scanner.nextInt();
						int ourDwell = readDwell * 10 * 60;
						trip.setDwell(ourDwell);

						instance.setLevel(Instance.ExtensionLevel.AS2);
					} catch (WrongTokenException e) {
						// AS1
						scanner.rewind();
					}

					// Früheste Abfahrtszeit. Umrechnung auf bommel-Zeit.
					assertToken(scanner, "FRUEHESTE_ABFAHRT");
					int readAllowedDepartureTimeEarliest = scanner.nextInt();
					int ourAllowedDepartureTimeEarliest = readAllowedDepartureTimeEarliest * 10 * 60;
					trip.setAllowedDepartureEarliest(ourAllowedDepartureTimeEarliest);

					// Späteste Abfahrtszeit. Umrechnung auf bommel-Zeit.
					assertToken(scanner, "SPAETESTE_ABFAHRT");
					int readAllowedDepartureTimeLatest = scanner.nextInt();
					int ourAllowedDepartureTimeLatest = readAllowedDepartureTimeLatest * 10 * 60;
					trip.setAllowedDepartureLatest(ourAllowedDepartureTimeLatest);

					// Lese das Leistungsprofil.
					assertToken(scanner, "LEISTUNGSPROFIL");
					float[] profileArray = new float[ourTripTime];
					int time = 0;
					for (; time < ourTripTime; time++) {
						// Zeit (haben wir selbst errechnet, ist also egal)
						scanner.nextDouble();

						// Leistungswert
						float power = (float)scanner.nextDouble();
						profileArray[time] = power;
					}
					trip.setRawPowerProfile(new RawPowerProfile(profileArray));
				}
			}
			System.out.printf("Lese Züge %c %3d%% (%d/%d)\n", ' ', 100, numberOfTrains, numberOfTrains);

			assertToken(scanner, "ENDE");
		}
	}

	/**
	 * Liest eine Lösung von einer .sol-Datei
	 *
	 * @param filename  Dateiname
	 * @return Lösung   
	 * @throws IOException Falls die Datei nicht wie erwartet formatiert ist.
	 */
	public TripPlanning readSolution(String filename) throws IOException {
		// Erstelle das Scanner-Objekt, welches die Datei in Teile aufteilt.
		try (BommelScanner scanner = new BommelScanner(filename)) {
			TripPlanning planning = new MutableTripPlanning(instance);

			assertToken(scanner, "LOESUNGSWERT");
			scanner.nextDouble(); // der Loesungswert interessiert uns nicht, den koennen wir selbst berechnen falls gewuenscht.

			// Anzahl der Züge.
			assertToken(scanner, "ANZAHL_ZUEGE");
			int numberOfTrains = scanner.nextInt();

			// Bearbeite nun die einzelnen Züge.
			for (int i = 0; i < numberOfTrains; i++) {
				// Zugnummer.
				assertToken(scanner, "ZUG_NR");
				int trainId = scanner.nextInt();
				Train train = instance.getTrainById(trainId);

				// Anzahl der Strecken für diesen Zug.
				assertToken(scanner, "ANZAHL_STRECKEN");
				int numberOfTrainSections = scanner.nextInt();

				for (int j = 0; j < numberOfTrainSections; j++) {
					// Streckennummer.
					assertToken(scanner, "STRECKEN_NR");
					int sectionId = scanner.nextInt();

					// Zunächst: Versuche die Strecke anhand der Nummer zu finden.
					//  Falls das fehlschlägt: Erstelle eine neue Strecke, setze ihre Nummer und mache sie für spätere
					//  Züge auffindbar.
					Section section = instance.getSectionById(sectionId);
					if (section == null)
						throw new IOException("Die angegebene STRECKEN_NR (" + Integer.toString(sectionId) + ") ist unbekannt!");

					RealTrip trip = instance.getTripByTrainAndSection(train, section);
					if (trip == null)
						throw new IOException("Konnte die zugehörige Fahrt zu Section#" + Integer.toString(sectionId) + ", Train#" + Integer.toString(trainId) + " nicht finden!");

					assertToken(scanner, "ABFAHRTSZEIT");
					int readTripDeparture = scanner.nextInt();
					int ourTripDeparture = readTripDeparture * 60 * 10; // umrechnung von Minuten auf bommel-Zeit

					planning = planning.withDeparture(trip, ourTripDeparture);
				}
			}

			assertToken(scanner, "ENDE");

			return planning;
		}
	}

	/**
	 * Schreibt die Lösung in eine Datei, deren Name sich am Instanzdateinamen orientiert (ohne Zeitstempel).
	 *
	 * @param planning Lösung
	 * @throws IOException Fehler beim Schreiben
	 */
	public void writeSolution(TripPlanning planning) throws IOException {
		writeSolution(planning, false);
	}

	/**
	 * Schreibt die Lösung in eine Datei, deren Name sich am Instanzdateinamen orientiert und mit ggf. Zeitstempel
	 * versehen wird.
	 *
	 * @param planning      Lösung
	 * @param withTimestamp Mit Zeitstempel oder nicht?
	 * @throws IOException Fehler beim Schreiben
	 */
	public void writeSolution(TripPlanning planning, boolean withTimestamp) throws IOException {
		String replace = ".sol";
		if (withTimestamp) {
			Format formatter = new SimpleDateFormat(".yyyy-MM-dd'T'HH:mm:ss");
			String timestamp = formatter.format(new Date(System.currentTimeMillis()));
			replace = timestamp + replace;
		}
		String solFileName = fileName.replaceFirst("\\.inst$", replace);
		writeSolution(planning, solFileName);
	}

	/**
	 * Schreibt die Lösung in die gegebene Datei.
	 *
	 * @param planning    Lösung
	 * @param solFileName Lösungsdateiname
	 * @throws IOException Fehler beim Schreiben
	 */
	public void writeSolution(TripPlanning planning, String solFileName) throws IOException {
		try (PrintWriter writer = new PrintWriter(new BufferedWriter(new FileWriter(solFileName)))) {
			writer.println("LOESUNGSWERT");
			int maxTime = planning.getPowerProfile().getMaximumTime();
			double maxPower = planning.getPowerProfile().getPowerAt(maxTime);
			writer.println(maxPower);

			writer.println("ANZAHL_ZUEGE");
			List<Train> trains = instance.getTrains();
			writer.println(trains.size());

			for (Train train : trains) {
				writer.println("ZUG_NR");
				writer.println(train.getId());

				writer.println("ANZAHL_STRECKEN");
				List<Trip> trips = train.getTrips();
				writer.println(trips.size());

				for (Trip trip_ : trips) {
					RealTrip trip = (RealTrip)trip_;
					writer.println("STRECKEN_NR");
					writer.println(trip.getSection().getId());

					writer.println("ABFAHRTSZEIT");
					writer.println(planning.getDeparture(trip) / 600);
				}
			}

			writer.println("ENDE");
		}
	}

	/**
	 * Gibt die geparste Instanz zurück.<br />
	 * ACHTUNG: {@code parse()} vorher aufrufen!
	 *
	 * @return Die Instanz.
	 */
	public Instance getInstance() {
		return instance;
	}

	/**
	 * Hilfsmethode, um sicherzustellen, dass da jetzt tatsächlich das erwartete Schlüsselwort in der Datei kommt.
	 *
	 * @param scanner Scanner-Objekt
	 * @param token Erwartetes Schlüsselwort
	 * @throws IOException Falls das Schlüsselwort nicht kam.
	 */
	public void assertToken(BommelScanner scanner, String token) throws IOException {
		String next = scanner.next();
		if (!next.equals(token)) {
			throw new WrongTokenException("Falsch formatierte .inst-Datei! Erwartet: '" + token + "', gefunden: '" + next + "'.");
		}
	}

	/**
	 * Wird geworfen, wenn ein unerwarter Token aufgetaucht ist.
	 */
	public static class WrongTokenException extends IOException {
		private WrongTokenException(String message) {
			super(message);
		}
	}

	/**
	 * Abgespeckte, schnelle Version anstelle Java-{@link java.util.Scanner}s.
	 */
	public static class BommelScanner implements AutoCloseable {
		private BufferedReader br;
		private String temp;
		private String previous;

		/**
		 * Erstellt einen neuen {@link BommelScanner}.
		 * 
		 * @param file Dateiname
		 * @throws FileNotFoundException Falls die Datei nicht gefunden wurde
		 */
		public BommelScanner(String file) throws FileNotFoundException {
			br = new BufferedReader(new FileReader(file));
		}

		/**
		 * Schließt die Datei.
		 * 
		 * @throws IOException Fehler beim Schließen
		 */
		public void close() throws IOException {
			br.close();
		}

		/**
		 * Liest die nächsten Zeichen.
		 * 
		 * @return Zeichenkette
		 * @throws IOException Fehler beim Lesen
		 */
		public String next() throws IOException {
			String line;

			if (temp != null) {
				line = temp;
				temp = null;
			} else {
				line = br.readLine();
			}

			if (line == null) {
				br.close();
				throw new EOFException();
			}

			int idx = line.indexOf(',');
			if (idx != -1) {
				temp = line.substring(idx + 1);
				line = line.substring(0, idx);
			}

			previous = line;

			return line;
		}

		/**
		 * Gibt die nächste Zeichenkette als int-Zahl zurück.
		 * 
		 * @return Zahl Fehler beim Lesen
		 */
		public int nextInt() throws IOException {
			return Integer.parseInt(next());
		}

		/**
		 * Gibt die nächste Zeichenkette als double-Zahl zurück.
		 *
		 * @return Zahl Fehler beim Lesen
		 */
		public double nextDouble() throws IOException {
			return Double.parseDouble(next());
		}

		/**
		 * Stellt den Leser um einen Schritt zurück.
		 */
		public void rewind() {
			temp = previous;
		}
	}
}
