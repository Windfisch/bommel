/*
 * Copyright (c) 2014, Florian Jung, Benjamin Miller, Johannes Thürauf, David Wägner
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package de.abbaddie.bommel.improver;

import de.abbaddie.bommel.data.*;

import java.math.BigInteger;
import java.util.*;

/**
 * Auswahlstrategie, welche Fahrten auswählt, welche am meisten Einfluss auf den aktuell höchsten Peak und in der Nähe
 * liegende ähnlich hohe Peaks haben.
 *
 * @see MostInfluenceSelectionStrategy
 * @see MovingPeakSelectionStrategy
 */
public class MultiPeakInfluenceSelectionStrategy extends BatchingSelectionStrategyAdapter {
	private List<PeakInformation> peaks;

	public MultiPeakInfluenceSelectionStrategy(BigInteger maxFlexibility) {
		super(maxFlexibility);
	}

	public MultiPeakInfluenceSelectionStrategy(BigInteger maxFlexibility, int maxNeighbours) {
		super(maxFlexibility, maxNeighbours);
	}

	@Override
	protected void prepare(List<Trip> trips, TripPlanning planning) {
		selectPeaks(instance, planning);
	}

	private void selectPeaks(Instance instance, TripPlanning planning) {
		PowerProfile prof = planning.getPowerProfile();
		int minutes = instance.getSchedulingInterval() / 600;

		peaks = new ArrayList<>(minutes);

		// Peaks suchen
		float absoluteMaxVal = 0;
		for (int minute = 0; minute < minutes; minute++) {
			int maxTime = minute * 600;
			float maxVal = prof.getPowerAt(maxTime);

			for (int i = minute * 600, j = 0; j < 600; i++, j++) {
				float ourVal = prof.getPowerAt(i);

				if (ourVal > maxVal) {
					maxTime = i;
					maxVal = ourVal;
				}
				if (ourVal > absoluteMaxVal) {
					absoluteMaxVal = ourVal;
				}
			}
			peaks.add(new PeakInformation(minute, maxTime, maxVal));
		}

		// Berücksichtige nur Peaks mit weniger als 10% bzw. 10MW Differenz zum Hauptpeak
		for (Iterator<PeakInformation> iter = peaks.iterator(); iter.hasNext(); ) {
			PeakInformation elem = iter.next();
			if (elem.value < absoluteMaxVal * 0.9 && absoluteMaxVal - elem.value >= 10_000_000) {
				iter.remove();
			}
		}
	}

	private List<Trip> selectPeakTrips(TripPlanning planning, Collection<Trip> not) {
		for (PeakInformation peak : peaks) {
			peak.setTopTrip(null);
			peak.setTopTripVal(0);
		}

		// Jeweils einen Trip zum Peak suchen
		for (TripPlanning.Entry entry : planning) {
			Trip trip = entry.getTrip();

			if (!not.contains(trip)) {
				int departure = entry.getDeparture();

				for (PeakInformation peak : peaks) {
					float ourVal = trip.getPowerProfile(departure).getPowerAt(peak.time);
					if (ourVal > peak.topTripVal) {
						peak.setTopTrip(trip);
						peak.setTopTripVal(ourVal);
					}
				}
			}
		}

		// Nach Peakwert sortieren
		Collections.sort(peaks);

		Set<Trip> peakTripsSet = new LinkedHashSet<>(peaks.size());
		for (PeakInformation peak : peaks) {
			if (peak.topTrip != null) {
				peakTripsSet.add(peak.topTrip);
			}
		}
		return new ArrayList<>(peakTripsSet);
	}

	@Override
	protected List<TripInformation> getNextBatch(TripPlanning planning, Collection<Trip> not) {
		return TripInformation.asTripInformationList(selectPeakTrips(planning, not));
	}

	protected static class PeakInformation implements Comparable<PeakInformation> {
		protected int minute;
		protected int time;
		protected float value;
		protected float topTripVal;
		protected Trip topTrip;

		private PeakInformation(int minute, int time, float value) {
			this.minute = minute;
			this.time = time;
			this.value = value;
		}

		public void setTopTrip(Trip topTrip) {
			this.topTrip = topTrip;
		}

		public void setTopTripVal(float topTripVal) {
			this.topTripVal = topTripVal;
		}

		@Override
		public int compareTo(PeakInformation peakInformation) {
			float diff = value - peakInformation.value;
			if (diff > 0) return -1;
			if (diff < 0) return +1;
			return 0;
		}
	}
}
