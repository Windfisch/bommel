/*
 * Copyright (c) 2014, Florian Jung, Benjamin Miller, Johannes Thürauf, David Wägner
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package de.abbaddie.bommel.improver;

import de.abbaddie.bommel.data.*;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

/**
 * Eine Auswahlstrategie, welche versucht, den Peak an eine freie Stelle zu verschieben.<br />
 * Dazu wird eine zunächst die festgelegte Richtung erste freie Stelle gesucht (jeweils in Minutenschritten), d.h. es
 * wird so weit gegangen, dass einiges an „Luft“ bleibt (zur Zeit: 100 MW).<br />
 * Dann wird von diesem freien Punkt ausgegangen und minutenweise „rückwärts“ (Richtung Peak) gegangen, und für jede
 * Minute eine Fahrt rausgesucht, welche möglichst gut Leistung von dieser Minute zur freien Stelle hin verschiebt.
 *
 * @see MultiPeakInfluenceSelectionStrategy
 */
public class MovingPeakSelectionStrategy extends BatchingSelectionStrategyAdapter {
	private Instance instance;

	private int slot;
	private int[] peaks;
	private Side side;
	private boolean useSecondClass;

	public MovingPeakSelectionStrategy(Instance instance, BigInteger maxFlexibility, Side side) {
		super(maxFlexibility);

		this.side = side;
		this.instance = instance;
	}

	public MovingPeakSelectionStrategy(Instance instance, BigInteger maxFlexibility, int maxNeighbours, Side side) {
		super(maxFlexibility, maxNeighbours);

		this.side = side;
		this.instance = instance;
	}

	@Override
	protected void prepare(List<Trip> trips, TripPlanning planning) {
		PowerProfile prof = planning.getPowerProfile();
		int maxTime = prof.getMaximumTime();
		float maxVal = prof.getPowerAt(maxTime);

		// Suche nach Peaks, welche verschoben werden müssen
		// „Start“ ist das Maximum, „Ende“ die erste Minute nach Start, bei der der Peak min. 10 MW unter Maximum liegt.
		slot = maxTime % 600;
		int freeTime = side == Side.RIGHT
				? instance.getSchedulingInterval() / 600 * 600 - 600 + slot
				: slot;
		int adder = side == Side.RIGHT ? 600 : -600;
		for (int i = maxTime + adder; side == Side.RIGHT ? i < instance.getSchedulingInterval() : i >= 0; i += adder) {
			// TODO: Bessere Grenze als die 100 MW aktuell
			if (prof.getPowerAt(i) < maxVal - 100_000_000) {
				freeTime = i;
				break;
			}
		}
		peaks = new int[Math.max(Math.abs(freeTime - maxTime) / 600, 1)];
		for (int i = 0; i < peaks.length; i++) {
			peaks[i] = maxTime + i * adder;
		}
		//peaks[peaks.length - 1] = maxTime - adder;
	}

	protected List<TripInformation> getNextBatch(TripPlanning planning, Collection<Trip> not) {
		TripInformation[] trips = new TripInformation[peaks.length];
		float[] freeVals = new float[peaks.length];
		boolean[] validAllocs = new boolean[peaks.length];
		int adder = side == Side.RIGHT ? 600 : -600;

		float lastFreed = Float.POSITIVE_INFINITY;
		float currentFreeing = 0;
		for (int i = peaks.length - 1; i >= 0; i--) {
			int peakTime = peaks[i];

			for (TripPlanning.Entry entry : planning) {
				Trip trip = entry.getTrip();

				if (!not.contains(trip)) {
					int departure = entry.getDeparture();
					PowerProfile prof = trip.getPowerProfile(departure);

					// Unseren Wert und die Werte drumherum berechnen
					float ourVal = prof.getPowerAt(peakTime);
					float targetVal = prof.getPowerAt(peakTime + adder);
					float beforeVal = prof.getPowerAt(peakTime - adder);

					// Wert, den wir bei unserem Punkt freigeben, und
					//  Wert, den wir beim nächsten belegen
					float alloc = ourVal - targetVal;
					float free = ourVal - beforeVal;

					// Berechne verschiedene Kriterien, ob der Trip
					//  interessant ist, und wenn ja, wie interessant.
					// SecondClass kann so ziemlich jeder Zug werden, der sich bewegen lässt
					// FirstClass-Züge sind besser: Kann für jeden Peak ein solcher Zug gefunden werden,
					//  wird sich im aktuellen Slot *garantiert* eine Verbesserung einstellen
					//  (das kann leider in anderen Slots kaputt gehen)
					// FirstClass-Züge werden normalerweise bevorzugt, auch wenn sie einen geringeren
					//  Vorteil bringen. Ist useSecondClass auf true gesetzt, werden sie nicht bevorzugt,
					//  denn manchmal sind SecondClass-Züge gar nicht mal so schlecht.
					int relevantBorder = side == Side.RIGHT
							? trip.getAllowedDepartureLatest()
							: trip.getAllowedDepartureEarliest();
					boolean freeIsBetter = free > freeVals[i];
					boolean allocValid = alloc < lastFreed;
					boolean movable = relevantBorder != departure;
					boolean increasingBefore = side == Side.RIGHT
							? increasesLeft(prof, peakTime)
							: decreasesRight(prof, peakTime);
					boolean increasesAfter = side == Side.RIGHT
							? increasesRight(prof, peakTime + adder)
							: decreasesLeft(prof, peakTime + adder);

					//noinspection UnnecessaryLocalVariable
					boolean secondClass = movable;
					boolean firstClass = secondClass && increasingBefore && increasesAfter && allocValid && free > 0;

					// Übernehme ggf. den Trip
					if (freeIsBetter && secondClass && !validAllocs[i]
							|| freeIsBetter && firstClass
							|| freeIsBetter && secondClass && useSecondClass
							|| firstClass && !validAllocs[i] && !useSecondClass) {
						freeVals[i] = free;
						trips[i] = new TripInformation(trip, departure, departure + adder);
						validAllocs[i] = firstClass;
						currentFreeing = free;
					}
				}
			}

			lastFreed = currentFreeing;
		}

		// Wenn wir eine „Überdeckung“ mit FirstClass-Trips gefunden haben, fixieren wir deren Abfahrtszeiten,
		//  denn wenn wir die nicht nehmen, wärs halt ziemlich dämlich.
		boolean great = true;
		for (int i = 0; i < peaks.length; i++) {
			if (!validAllocs[i] || trips[i] == null) {
				great = false;
				break;
			}
		}

		if (great) {
			for (TripInformation info : trips) {
				info.setConsideredDepartures(info.getConsideredDepartures()[1]);
			}
		}

		useSecondClass = !useSecondClass;
		return Arrays.asList(trips);
	}

	private boolean increasesLeft(PowerProfile prof, int leftFrom) {
		return fineLeft(prof, leftFrom, 1);
	}

	private boolean decreasesLeft(PowerProfile prof, int leftFrom) {
		return fineLeft(prof, leftFrom, -1);
	}

	private boolean fineLeft(PowerProfile prof, int leftFrom, int mult) {
		if (leftFrom < 600 + slot) return true;

		float last = prof.getPowerAt(slot) * mult;

		for (int i = slot + 600; i <= leftFrom; i += 600) {
			float current = prof.getPowerAt(i) * mult;

			if (current < last) {
				return false;
			}
			last = current;
		}

		return true;
	}

	private boolean increasesRight(PowerProfile prof, int rightFrom) {
		return fineRight(prof, rightFrom, 1);
	}

	private boolean decreasesRight(PowerProfile prof, int rightFrom) {
		return fineRight(prof, rightFrom, -1);
	}

	private boolean fineRight(PowerProfile prof, int rightFrom, int mult) {
		int time = instance.getSchedulingInterval() / 600 * 600 - 600 + slot;

		float last = prof.getPowerAt(time) * mult;

		for (time -= 600; time >= rightFrom; time -= 600) {
			float current = prof.getPowerAt(time) * mult;

			if (current > last) {
				return false;
			}
			last = current;
		}

		return true;
	}

	@Override
	public String toString() {
		return getClass().getSimpleName() + "(" + (getMaxFlexibility().toString().length() - 1) + "," + side.toString().toLowerCase() + "=" + peaks.length + ")";
	}

	/**
	 * Die Richtung, in welche verschoben werden soll.
	 */
	public enum Side {
		LEFT, RIGHT
	}
}
