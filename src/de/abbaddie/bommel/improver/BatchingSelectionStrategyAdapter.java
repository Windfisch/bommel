/*
 * Copyright (c) 2014, Florian Jung, Benjamin Miller, Johannes Thürauf, David Wägner
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package de.abbaddie.bommel.improver;

import de.abbaddie.bommel.data.Trip;
import de.abbaddie.bommel.data.TripInformation;
import de.abbaddie.bommel.data.TripPlanning;

import java.math.BigInteger;
import java.util.*;

/**
 * <p>Diese Klasse dient, ähnlich wie {@link SelectionStrategyAdapter}, v.a. der Code-Deduplikation.<br />
 * Diese Klasse umfasst alle Funktionen von {@link SelectionStrategyAdapter}, aber lädt die Fahrten in Gruppen. Dies
 * entspricht oft eher der Vorgehensweise der Auswahlstrategien.</p>
 *
 * @see de.abbaddie.bommel.improver.SelectionStrategyAdapter
 */
public abstract class BatchingSelectionStrategyAdapter extends SelectionStrategyAdapter {
	private List<TripInformation> currentBatch;
	private int entryNo;

	/**
	 * Erstellt ein neues {@link SelectionStrategyAdapter}-Objekt mit der gegebenen Flexibilität als Obergrenze.
	 *
	 * @param maxFlexibility Maximale Flexibilität
	 */
	public BatchingSelectionStrategyAdapter(BigInteger maxFlexibility) {
		super(maxFlexibility);
	}


	/**
	 * Erstellt ein neues {@link SelectionStrategyAdapter}-Objekt mit der gegebenen Flexibilität als Obergrenze und der
	 * gegebenen Grenze für die Zahl auszuwählender benachbarter Fahrten im selben Zug.
	 *
	 * @param maxFlexibility Maximale Flexibilität
	 * @param maxNeighbours  Maximale Zahl auszuwählender benachbarter Fahrten im selben Zug
	 */
	public BatchingSelectionStrategyAdapter(BigInteger maxFlexibility, int maxNeighbours) {
		super(maxFlexibility, maxNeighbours);
	}

	/**
	 * Führt interne Initialisierungen durch.<br />
	 * Stattdessen kann
	 * {@link #prepare(java.util.List, de.abbaddie.bommel.data.TripPlanning)}
	 * überschrieben werden.
	 *
	 * @param trips    Fahrten, aus denen die Auswahl getroffen werden soll
	 * @param planning Bisherige Lösung
	 */
	@Override
	protected final void selectBeforeHook(List<Trip> trips, TripPlanning planning) {
		currentBatch = Collections.emptyList();
		entryNo = 0;

		prepare(trips, planning);
	}

	/**
	 * Analog zu {@link SelectionStrategyAdapter#selectBeforeHook(java.util.List, de.abbaddie.bommel.data.TripPlanning)}.
	 *
	 * @param trips    Fahrten, aus denen die Auswahl getroffen werden soll
	 * @param planning Bisherige Lösung
	 */
	protected void prepare(List<Trip> trips, TripPlanning planning) {
	}

	/**
	 * Gibt eine Liste von Abfahrtsinformationen zurück, die am besten passt, aber keine Fahrten aus <code>not</code> enthält.
	 *
	 * @param planning Fahrten, aus denen die Auswahl getroffen werden soll
	 * @param not      Fahrten, welche nicht gewählt werden dürfen.
	 * @return Liste von Abfahrtsinformationen
	 */
	protected abstract List<TripInformation> getNextBatch(TripPlanning planning, Collection<Trip> not);

	/**
	 * Sorgt dafür, dass wenn nötig eine Liste angefordert wird, und gibt daraus die Fahrten zurück.
	 *
	 * @param trips    Fahrten, aus denen die Auswahl getroffen werden soll
	 * @param not      Fahrten, welche nicht mehr ausgewählt werden sollen
	 * @param planning Bisherige Lösung
	 * @return Ausgewählte Fahrt
	 */
	@Override
	protected final TripInformation selectOneExtended(List<Trip> trips, Collection<Trip> not, TripPlanning planning) {
		if (currentBatch.size() >= entryNo) {
			Set<TripInformation> set = new LinkedHashSet<>(getNextBatch(planning, not));
			currentBatch = new ArrayList<>(set.size());

			for (TripInformation info : set) {
				if (info != null && !currentBatch.contains(info)) {
					currentBatch.add(info);
				}
			}
			entryNo = 0;
		}
		if (currentBatch.isEmpty()) {
			return null;
		}
		return currentBatch.get(entryNo++);
	}
}
