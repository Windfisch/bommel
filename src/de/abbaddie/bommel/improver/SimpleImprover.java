/*
 * Copyright (c) 2014, Florian Jung, Benjamin Miller, Johannes Thürauf, David Wägner
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package de.abbaddie.bommel.improver;

import de.abbaddie.bommel.Bommel;
import de.abbaddie.bommel.data.*;
import de.abbaddie.bommel.solver.Solver;

import java.util.ArrayList;
import java.util.List;

/**
 * Der {@link SimpleImprover} ist zuständig eine einzelne Auswahlstrategie aufzurufen, für deren Auswahl den Löser
 * aufzurufen und das bessere Ergebnis zurückzugeben.<br />
 * Wird typischerweise von {@link SeriousImprover} und {@link RandomImprover} genutzt.
 *
 * @see SeriousImprover
 * @see RandomImprover
 */
public class SimpleImprover implements Improver {
	private SelectionStrategy selectionStrategy;
	private Solver solver;
	private Instance instance;
	private int lastTrainCount;

	/**
	 * Erstellt einen neuen {@link SimpleImprover} mit den gegebenen Daten.
	 *
	 * @param instance          Probleminstanz
	 * @param selectionStrategy Auswahlstrategie
	 * @param solver            Löser
	 */
	public SimpleImprover(Instance instance, SelectionStrategy selectionStrategy, Solver solver) {
		this.selectionStrategy = selectionStrategy;
		selectionStrategy.setInstance(instance);
		
		this.solver = solver;
		solver.setInstance(instance);
		
		this.instance = instance;
	}

	/**
	 * Gibt die Auswahlstrategie zurück.
	 *
	 * @return Auswahlstrategie
	 */
	public SelectionStrategy getSelectionStrategy() {
		return selectionStrategy;
	}

	public void setInstance(Instance instance) {
		this.instance = instance;
		solver.setInstance(instance);
		selectionStrategy.setInstance(instance);
	}

	public TripPlanning improve(List<Trip> trips, TripPlanning planning) {
		// Auswählen
		List<TripInformation> selectedTripInfos = selectionStrategy.select(trips, planning);

		// Eigene Datenstrukturen
		List<Trip> selectedTrips = TripInformation.asTripList(selectedTripInfos);
		List<Trip> notSelectedTrips = new ArrayList<>();

		// Debug
		Bommel.debug("Ausgewählte Fahrten für die Verbesserung:");
		for (TripInformation t : selectedTripInfos) {
			Bommel.debug("\t" + t);
		}
		Bommel.debug("\n\n");


		// Baue SuperTrip aus den restlichen Fahrten
		for (Trip t : trips) {
			if (!selectedTrips.contains(t)) {
				notSelectedTrips.add(t);
			}
		}
		SuperTrip otherTrips = new SuperTrip(notSelectedTrips, planning);
		selectedTripInfos.add(new TripInformation(otherTrips));

		// Protokoll (für den anschließenden Vergleich und die Ausgabe)
		double oldPower = planning.getPowerProfile().getPowerAt(planning.getPowerProfile().getMaximumTime());
		lastTrainCount = selectedTripInfos.size();
		TripPlanning improvedPlanning = planning;

		// Lösen
		TripPlanning tempPlanning = solver.solve(selectedTripInfos);
		if (tempPlanning == null) {
			// Lösen hat keine Lösung ergeben, dann nehmen wir einfach die alte
			return planning;
		}

		// Baue eine neue „flache“ Planung, d.h. mit den RealTrips (statt RealTrips + SuperTrip, wie vom Löser erzeugt)
		for (TripPlanning.Entry entry : tempPlanning) {
			Trip trip = entry.getTrip();
			if (trip != otherTrips) {
				improvedPlanning = improvedPlanning.withDeparture(trip, entry.getDeparture());
			} else {
				improvedPlanning = otherTrips.getPlanningWithOffset(improvedPlanning, entry.getDeparture());
			}
		}

		// Finde heraus, ob wir etwas verbessert haben
		double newPower = improvedPlanning.getPowerProfile().getPowerAt(improvedPlanning.getPowerProfile().getMaximumTime());
		if (newPower > oldPower) {
			// Wir haben uns verschlechtert, verwerfe die Lösung
			return planning;
		}

		// Protokoll, fürs Autospeichern und Plotten
		instance.setCurrentSolution(improvedPlanning);

		// Fertig \o/
		return improvedPlanning;
	}

	/**
	 * Gibt die Zahl der Fahrten zurück, die das letzte Mal ausgewählt wurden.
	 *
	 * @return Fahrtenzahl
	 */
	public int getLastTrainCount() {
		return lastTrainCount;
	}

	@Override
	public String toString() {
		return selectionStrategy.toString() + "/" + solver.toString();
	}
}
