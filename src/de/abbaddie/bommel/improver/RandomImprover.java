/*
 * Copyright (c) 2014, Florian Jung, Benjamin Miller, Johannes Thürauf, David Wägner
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package de.abbaddie.bommel.improver;

import de.abbaddie.bommel.data.Instance;
import de.abbaddie.bommel.data.MutableTripPlanning;
import de.abbaddie.bommel.data.Trip;
import de.abbaddie.bommel.data.TripPlanning;
import de.abbaddie.bommel.solver.Solver;

import java.math.BigInteger;
import java.util.List;

/**
 * Der {@link RandomImprover} führt beim Aufruf einen einmaligen Versuch durch, durch zufällige Fahrtenauswahl eine
 * Verbesserung herbeizuführen.<br />
 * Er ist typischerweise das Gegenstück zum {@link SeriousImprover}.<br />
 * Verwendet intern den {@link SimpleImprover}.<br />
 * Wird typischerweise von {@link ComplexImprover} verwendet.<br />
 *
 * @see SeriousImprover
 * @see ComplexImprover
 * @see SimpleImprover
 */
public class RandomImprover implements Improver {
	private BigInteger maxFlexibility;
	private SimpleImprover child;
	private Instance instance;

	@Override
	public void setInstance(Instance instance) {
		this.instance = instance;
		child.setInstance(instance);
	}

	/**
	 * Erzeugt einen neuen {@link RandomImprover} mit den gegebenen Daten.
	 *
	 * @param instance       Probleminstanz
	 * @param solver         Löser
	 * @param maxFlexibility Maximale  Flexibilitäts
	 */
	public RandomImprover(Instance instance, Solver solver, BigInteger maxFlexibility) {
		this.instance = instance;
		this.maxFlexibility = maxFlexibility;

		child = new SimpleImprover(instance, new RandomSelectionStrategy(maxFlexibility), solver);
	}

	/**
	 * Erzeugt einen neuen {@link RandomImprover} mit den gegebenen Daten.
	 *
	 * @param instance        Probleminstanz
	 * @param solver          Löser
	 * @param maxFlexibility  Maximale Flexibilität
	 * @param neighboursCount Maximale Anzahl benachbarter Fahrten im selben Zug
	 */
	public RandomImprover(Instance instance, Solver solver, BigInteger maxFlexibility, int neighboursCount) {
		this.instance = instance;
		this.maxFlexibility = maxFlexibility;

		child = new SimpleImprover(instance, new RandomSelectionStrategy(maxFlexibility, neighboursCount), solver);
	}

	/**
	 * Setzt die maximale Flexibilität.
	 *
	 * @param maxFlexibility Maximale Flexibilität
	 */
	public void setMaxFlexibility(BigInteger maxFlexibility) {
		this.maxFlexibility = maxFlexibility;

		child.getSelectionStrategy().setMaxFlexibility(maxFlexibility);
	}

	@Override
	public TripPlanning improve(List<Trip> trips, TripPlanning planning) {
		TripPlanning copy = new MutableTripPlanning(planning);

		planning = child.improve(trips, planning);

		float oldMaxPower = copy.getPowerProfileMaximumOnly().getPowerAt(copy.getPowerProfileMaximumOnly().getMaximumTime());
		float ourMaxPower = planning.getPowerProfileMaximumOnly().getPowerAt(planning.getPowerProfileMaximumOnly().getMaximumTime());

		if (ourMaxPower < oldMaxPower) {
			System.out.printf(child.toString() + " verbessert um %.3fMW auf %.3fMW. [!]\n", (oldMaxPower - ourMaxPower) / 1_000_000, ourMaxPower / 1_000_000);
			instance.setCurrentSolution(planning);
		} else {
			System.out.println(child.toString() + " verbessert nicht.");
		}

		return ourMaxPower < oldMaxPower ? planning : copy;
	}
}
