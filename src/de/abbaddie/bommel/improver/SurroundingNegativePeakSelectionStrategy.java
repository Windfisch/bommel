/*
 * Copyright (c) 2014, Florian Jung, Benjamin Miller, Johannes Thürauf, David Wägner
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package de.abbaddie.bommel.improver;

import de.abbaddie.bommel.data.PowerProfile;
import de.abbaddie.bommel.data.Trip;
import de.abbaddie.bommel.data.TripInformation;
import de.abbaddie.bommel.data.TripPlanning;

import java.math.BigInteger;
import java.util.Collection;
import java.util.List;


/**
 * Eine Auswahlstrategie, welche in den Minuten vor und nach dem aktuellen Peak nach Fahrten sucht, welche dort Leistung
 * einspeisen, und versucht die Einspeisung auf den Peak zu verschieben
 */
// TODO: Radius konfigurierbar machen
public class SurroundingNegativePeakSelectionStrategy extends SelectionStrategyAdapter {
	public static final int RADIUS = 3;

	public SurroundingNegativePeakSelectionStrategy(BigInteger maxFlexibility) {
		super(maxFlexibility);
	}

	public SurroundingNegativePeakSelectionStrategy(BigInteger maxFlexibility, int maxNeighbours) {
		super(maxFlexibility, maxNeighbours);
	}

	@Override
	protected TripInformation selectOneExtended(List<Trip> trips, Collection<Trip> not, TripPlanning planning) {
		int maxTime = planning.getPowerProfile().getMaximumTime();
		int leftLimit = Math.max(maxTime - RADIUS * 600, maxTime % 600);
		int rightLimit = Math.min(instance.getSchedulingInterval() / 600 * 600 - 600 + maxTime % 600, maxTime + RADIUS * 600);
		int size = (rightLimit - leftLimit) / 600;
		int[] times = new int[size];
		for (int i = 0, time = leftLimit; i < size; i++, time += 600) {
			if (time == maxTime) {
				time += 600;
			}
			times[i] = time;
		}

		TripInformation currentMinTripInfo = null;
		double currentMinVal = 0;

		for (TripPlanning.Entry entry : planning) {
			Trip trip = entry.getTrip();

			if (!not.contains(trip)) {
				int departure = entry.getDeparture();
				PowerProfile prof = trip.getPowerProfile(departure);

				for (int time : times) {
					int newDeparture = departure - (maxTime - time);
					if (prof.getPowerAt(time) < currentMinVal
							&& trip.getAllowedDepartureEarliest() <= newDeparture
							&& newDeparture <= trip.getAllowedDepartureLatest()) {
						currentMinTripInfo = new TripInformation(trip, newDeparture, departure);
						currentMinVal = prof.getPowerAt(time);
					}
				}
			}
		}

		return currentMinTripInfo;
	}
}
