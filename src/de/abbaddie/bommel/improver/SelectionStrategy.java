/*
 * Copyright (c) 2014, Florian Jung, Benjamin Miller, Johannes Thürauf, David Wägner
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package de.abbaddie.bommel.improver;

import de.abbaddie.bommel.data.Instance;
import de.abbaddie.bommel.data.Trip;
import de.abbaddie.bommel.data.TripInformation;
import de.abbaddie.bommel.data.TripPlanning;

import java.math.BigInteger;
import java.util.List;

/**
 * <p>Eine Auswahlstrategie.<br />
 * Implementierungen können im Verbesserungsprozess eingesetzt werden, um Fahrten auszuwählen, bei denen es besonders
 * sinnvoll erscheint, sich die Abfahrtszeiten nochmal genauer anzuschauen.</p>
 * <p>Diese Schnittstelle ist zu unterscheiden von {@link de.abbaddie.bommel.solver.DivisionStrategy}, welche nur im
 * Teile-und-Herrsche-Lösungsalgorithmus eingesetzt wird und das ganze Problem zerlegt (und nicht eine „kleine“ Menge an
 * Fahrten auswählt).</p>
 *
 * @see de.abbaddie.bommel.improver.Improver
 */
public interface SelectionStrategy {
	/**
	 * Setzt die maximale Flexibilität. Die Flexibilität errechnet sich hier als Produkt der Anzahl der Abfahrtszeiten
	 * der ausgewählten Züge (also die Zahl der Kombinationen, die {@link de.abbaddie.bommel.solver.BruteforceSolver}
	 * im schlimmsten Fall berechnen müsste).
	 *
	 * @param maxFlexibility Maximale Flexibilität
	 */
	public void setMaxFlexibility(BigInteger maxFlexibility);

	/**
	 * Setzt das Übersprunglevel.<br />
	 * Ist level gleich 0, werden einfach die ersten passenden Züge ausgewählt.<br />
	 * Ist level gleich 1, werden diese übersprungen und die nächstbesser passenden Züge ausgewählt.<br />
	 * Ist level gleich 2, werden auch diese übersprungen und die nächsten ausgewählt.<br />
	 * usw.
	 *
	 * @param level Übersprunglevel
	 */
	public void setSkipLevel(int level);

	/**
	 * Setzt die Probleminstanz, zu der die Lösung erstellt werden soll.
	 *
	 * @param instance Probleminstanz
	 */
	public void setInstance(Instance instance);

	/**
	 * Führt das eigentliche Auswählen aus.
	 *
	 * @param trips    Fahrten, aus denen die Auswahl getroffen werden soll
	 * @param planning Bisherige Lösung
	 * @return Abfahrtsinformationen
	 */
	public List<TripInformation> select(List<Trip> trips, TripPlanning planning);

}
