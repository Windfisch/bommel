/*
 * Copyright (c) 2014, Florian Jung, Benjamin Miller, Johannes Thürauf, David Wägner
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package de.abbaddie.bommel.improver;

import de.abbaddie.bommel.data.Instance;
import de.abbaddie.bommel.data.Trip;
import de.abbaddie.bommel.data.TripPlanning;
import de.abbaddie.bommel.solver.Solver;

import java.math.BigInteger;
import java.util.List;

/**
 * Der {@link ComplexImprover} ist normalerweise der „erste“ Improver. Er steuert alle anderen.<br />
 * Er steuert einen {@link SeriousImprover} und {@link RandomImprover}. Zunächst wird der <code>SeriousImprover</code>
 * gestartet. Bringt der keine Verbesserung mehr, wird <code>RandomImprover</code> ausprobiert. Bringt auch der nichts,
 * wird die Flexibilität erhöht und von vorne gestartet. Gegebenenfalls wird beendet, falls die maximale Flexibilität
 * erreicht wurde.
 *
 * @see SeriousImprover
 * @see RandomImprover
 */
public class ComplexImprover implements Improver {
	private Instance instance;
	private BigInteger startFlex = BigInteger.valueOf(10).pow(20);
	private BigInteger maxFlex = BigInteger.valueOf(10).pow(60);
	private BigInteger flexStep = BigInteger.valueOf(10).pow(5);
	private BigInteger currFlexibility = startFlex;
	private SeriousImprover serious = null;
	private RandomImprover random = null;
	private Solver solver;

	@Override
	public void setInstance(Instance instance) {
		this.instance = instance;
		if (serious != null) {
			serious.setInstance(instance);
		}
		if (random != null) {
			random.setInstance(instance);
		}
	}

	/**
	 * Erzeugt einen neuen {@link SeriousImprover} mit den gegebenen Daten.
	 *
	 * @param instance Probleminstanz
	 * @param solver   Löser
	 */
	public ComplexImprover(Instance instance, Solver solver) {
		this.instance = instance;
		this.solver = solver;
		solver.setInstance(instance);
		serious = new SeriousImprover(instance, solver, currFlexibility);
		random = new RandomImprover(instance, solver, currFlexibility);
	}

	/**
	 * Erzeugt einen neuen {@link SeriousImprover} mit den gegebenen Daten.
	 *
	 * @param instance        Probleminstanz
	 * @param solver          Löser
	 * @param neighboursCount Maximale Anzahl benachbarter Fahrten im selben Zug
	 */
	public ComplexImprover(Instance instance, Solver solver, int neighboursCount) {
		this.instance = instance;
		this.solver = solver;
		solver.setInstance(instance);
		serious = new SeriousImprover(instance, solver, currFlexibility, neighboursCount);
		random = new RandomImprover(instance, solver, currFlexibility, neighboursCount);
	}

	/**
	 * Setzt die Startflexibilität.
	 *
	 * @param f Startflexibilität
	 */
	public void setStartFlexibility(BigInteger f) {
		startFlex = f;

		currFlexibility = startFlex;
		serious = new SeriousImprover(instance, solver, currFlexibility);
		random = new RandomImprover(instance, solver, currFlexibility);
	}

	/**
	 * Setzt die Endflexibilität.
	 *
	 * @param f Endflexibilität
	 */
	public void setEndFlexibility(BigInteger f) {
		maxFlex = f;
	}

	/**
	 * Setzt Start- und Endflexibilität zugleich.
	 *
	 * @param f Feste Flexibilität
	 * @see #setStartFlexibility(java.math.BigInteger)
	 * @see #setEndFlexibility(java.math.BigInteger)
	 */
	public void setFixedFlexibility(BigInteger f) {
		setStartFlexibility(f);
		setEndFlexibility(f);
	}

	/**
	 * Setzt den Flexibilität, um die pro Schitt erhöht wird.
	 * 
	 * @param flexStep Flexibilitätserhöhung
	 */
	public void setFlexibilityStep(BigInteger flexStep) {
		this.flexStep = flexStep;
	}

	/**
	 * Setzt das maximale Übersprunglevel.
	 *
	 * @param level Maximales Übersprunglevel
	 */
	public void setMaxSkipLevel(int level) {
		serious.setMaxSkipLevel(level);
	}

	/**
	 * Gibt die aktuelle Flexibilität zurück.
	 *
	 * @return Aktuelle Flexibilität
	 */
	public BigInteger getCurrentFlexibility() {
		return currFlexibility;
	}

	@Override
	public TripPlanning improve(List<Trip> trips, TripPlanning planning) {
		return improveWithTimeLimit(trips, planning, 0);
	}

	/**
	 * Wie {@link #improve(java.util.List, de.abbaddie.bommel.data.TripPlanning)}, nur kann noch ein Zeitlimit angegeben
	 * werden, nach dessen Verstreichen keine weiteren Verbesserungsvorgänge mehr stattfinden.
	 *
	 * @param trips     Liste der Fahrten, die optimiert werden sollen
	 * @param planning  Aktuelle Lösung
	 * @param timeLimit Zeitlimit in Sekunden. <code>0</code> für kein Zeitlimit.
	 * @return Ggf. verbesserte Lösung   
	 */
	public TripPlanning improveWithTimeLimit(List<Trip> trips, TripPlanning planning, int timeLimit) {
		// Startzeit aufnehmen, wegen dem Zeitlimit
		long beginTime = System.nanoTime();

		// Wenn uns was nicht mehr passt, wird gebreakt.
		while (true) {

			float oldMax;
			float newMax;

			newMax = planning.getPowerProfileMaximumOnly().getPowerAt(planning.getPowerProfileMaximumOnly().getMaximumTime());
			do {
				// Alten Wert merken
				oldMax = newMax;

				// Zeitlimit beachten
				int timeElapsed = (int)((System.nanoTime() - beginTime) / 1_000_000_000);
				if (timeLimit != 0 && timeElapsed >= timeLimit)
					return planning;

				// Zuerst ernsthaft probieren
				planning = serious.improveWithTimeLimit(trips, planning,
						((timeLimit == 0) ? 0 : (timeLimit - timeElapsed)));

				// Zeitlimit nochmal beachten.
				if (serious.ranIntoTimeLimit()) {
					continue;
				}

				// Danach nochmal zufallsmäßig probieren
				planning = random.improve(trips, planning);

				// Maximum zum Vergleich ausrechnen
				newMax = planning.getPowerProfileMaximumOnly().getPowerAt(planning.getPowerProfileMaximumOnly().getMaximumTime());
			} while (serious.ranIntoTimeLimit() || newMax < oldMax);
			// Nach dieser Schleife haben einmal ernsthaft und einmal zufällig hinterander nichts gebracht.

			// Mini-Info anzeigen
			// Wollen wir das? Nein.
			//InstanceEstimator.showAverageIntervalPosition(planning);

			// Erhöhe die Flexibilität
			currFlexibility = currFlexibility.multiply(flexStep);
			if (currFlexibility.compareTo(maxFlex) > 0) {
				// Ende erreicht
				break;
			}

			System.out.println("## Erhöhe Flexibilität auf " + currFlexibility);
			serious.setMaxFlexibility(currFlexibility);
			random.setMaxFlexibility(currFlexibility);
		}

		// Wegen der zu hohen Flexibilität wurde gebreakt
		return planning;
	}
}
