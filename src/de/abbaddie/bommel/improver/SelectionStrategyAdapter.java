/*
 * Copyright (c) 2014, Florian Jung, Benjamin Miller, Johannes Thürauf, David Wägner
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package de.abbaddie.bommel.improver;

import de.abbaddie.bommel.Bommel;
import de.abbaddie.bommel.data.*;

import java.math.BigInteger;
import java.util.*;

/**
 * <p>Diese Klasse dient vor allem der Code-Deduplikation.<br />
 * Die Auswahlstrategien haben zwar immer einen speziellen Fokus, aber viele Gemeinsamkeiten, v.a. Behandlung des
 * Übersprunglevel und Auswahl benachbarter Fahrten im selben Zug. (Letzteres hat nichts mit dem
 * Übersprunglevel zu tun.).
 * Diese Klasse kümmert sich um diese Gemeinsamkeiten. Implementierer müssen dann etwas andere Methoden implementieren
 * als in {@link SelectionStrategy} vorgegeben.</p>
 * Es reicht, eine der beiden Methoden
 * {@link #selectOne(java.util.List, java.util.Collection, de.abbaddie.bommel.data.TripPlanning)}
 * und
 * {@link #selectOneExtended(java.util.List, java.util.Collection, de.abbaddie.bommel.data.TripPlanning)}
 * zu überschreiben.
 *
 * @see BatchingSelectionStrategyAdapter
 */
public abstract class SelectionStrategyAdapter implements SelectionStrategy {
	/**
	 * Standardanzahl für maximal auszuwählende Zahl benachbarter Fahrten im selben Zug.
	 */
	public static final int DEFAULT_MAX_NEIGHBOURS = 3;

	private BigInteger maxFlexibility;
	protected Instance instance;

	private int maxNeighbours; // bestimmt, wieviele benachbarte Trips maximal auch betrachtet werden sollen.
	private int skipLevel = 0; // sollen wir die top10 nehmen, oder die top10 verwerfen und die 10-20 nehmen (oder 20-30 usw)

	/**
	 * Erstellt ein neues {@link SelectionStrategyAdapter}-Objekt mit der gegebenen Flexibilität als Obergrenze.
	 *
	 * @param maxFlexibility Maximale Flexibilität
	 */
	public SelectionStrategyAdapter(BigInteger maxFlexibility) {
		this.maxFlexibility = maxFlexibility;
		this.maxNeighbours = DEFAULT_MAX_NEIGHBOURS;
	}

	/**
	 * Erstellt ein neues {@link SelectionStrategyAdapter}-Objekt mit der gegebenen Flexibilität als Obergrenze und der
	 * gegebenen Grenze für die Zahl auszuwählender benachbarter Fahrten im selben Zug.
	 *
	 * @param maxFlexibility Maximale Flexibilität
	 * @param maxNeighbours  Maximale Zahl auszuwählender benachbarter Fahrten im selben Zug
	 */
	public SelectionStrategyAdapter(BigInteger maxFlexibility, int maxNeighbours) {
		this.maxFlexibility = maxFlexibility;
		this.maxNeighbours = maxNeighbours;
	}

	/**
	 * Wird einmal pro Aufruf von
	 * {@link SelectionStrategy#select(java.util.List, de.abbaddie.bommel.data.TripPlanning)}
	 * aufgerufen. Diese Methode kann mit eigener Logik befüllt werden, die Methode muss in
	 * {@link SelectionStrategyAdapter} nicht nochmal aufgerufen werden.<br />
	 * Dies kann nützlich sein, wenn man bspw. alle Züge nach einem gewissen Kriterium sortieren will, nach dem man dann
	 * auswählt.
	 *
	 * @param trips    Fahrten, aus denen die Auswahl getroffen werden soll
	 * @param planning Bisherige Lösung
	 */
	protected void selectBeforeHook(List<Trip> trips, TripPlanning planning) {
	}

	@Override
	public final List<TripInformation> select(List<Trip> trips, TripPlanning planning) {
		// Hook ausführen
		selectBeforeHook(trips, planning);

		List<TripInformation> selected = new ArrayList<>();
		Set<Trip> not = new HashSet<>();
		BigInteger flexibility = BigInteger.ONE;
		int discarded = 0;
		// Wähle nach und nach Fahrten aus. Wenn das Fass voll ist, wird gebreakt.
		// Jeder Schleifendurchlauf umfasst einen Schritt. In jedem Schritt wird eine Fahrt ausgewählt und ggf. noch
		//  weitere benachbarte dazugepackt.
		while (true) {
			// Wähle die Fahrt aus
			TripInformation oneTripInfo = selectOneExtended(trips, not, planning);

			// Keine Fahrt mehr – huh!
			if (oneTripInfo == null) break;

			if (!(oneTripInfo.getTrip() instanceof RealTrip)) {
				throw new IllegalArgumentException("Es kann mit SelectionStrategyAdapter nur auf echten Fahrten verbessert werden!");
			}

			// Hole alle Fahrten des zugeordneten Zuges
			List<Trip> allTrips = ((RealTrip)oneTripInfo.getTrip()).getTrain().getTrips();

			// toAdd soll alle Fahrten enthalten, die wir in diesem Schritt hinzufügen möchten
			List<TripInformation> toAdd = new ArrayList<>();
			toAdd.add(oneTripInfo);

			// Betrachte noch eine gewisse Anzahl von Fahrten links und rechts vom ausgewählten.
			// Höre auf, wenn eine Fahrt den zuvor ausgewählten sowieso nicht mehr beeinflusst oder die Schranke
			// erreicht ist.
			// TODO: Hier ist noch einiges an Verbesserungspotential:
			// - Bessere Auswahl der benachbarten Züge
			// - consideredDepartures vernünftig setzen
			// Schaue zunächst nach rechts (anschließende Fahrten)
			ListIterator<Trip> oneTripIterator = allTrips.listIterator(allTrips.indexOf(oneTripInfo.getTrip()));
			if (oneTripIterator.hasNext()) oneTripIterator.next();
			TripInformation lastTripInfo = oneTripInfo;
			int nextCounter = 0;
			while (oneTripIterator.hasNext() && nextCounter < maxNeighbours) {
				RealTrip nextTrip = (RealTrip)oneTripIterator.next();

				if (nextTrip.affects(planning, lastTripInfo)) {
					// Es findet eine Beeinflussung statt, wähle die Fahrt aus,falls noch nicht geschehen
					TripInformation tripInfo = new TripInformation(nextTrip);

					if (!not.contains(tripInfo.getTrip())) {
						toAdd.add(tripInfo);
					}
					lastTripInfo = tripInfo;
					nextCounter++;
				} else {
					// Keine Beeinflussung, also interessiert uns die Fahrt eh nicht
					break;
				}
			}

			// Schaue zudem nach links (vorherige Fahrten)
			oneTripIterator = allTrips.listIterator(allTrips.indexOf(oneTripInfo.getTrip()));
			lastTripInfo = oneTripInfo;
			int prevCounter = 0;
			while (oneTripIterator.hasPrevious() && prevCounter < maxNeighbours) {
				RealTrip prevTrip = (RealTrip)oneTripIterator.previous();

				if (prevTrip.affects(planning, lastTripInfo)) {
					// Es findet eine Beeinflussung statt, wähle die Fahrt aus,falls noch nicht geschehen
					TripInformation tripInfo = new TripInformation(prevTrip);

					if (!not.contains(tripInfo.getTrip())) {
						toAdd.add(tripInfo);
					}
					lastTripInfo = tripInfo;
					prevCounter++;
				} else {
					// Keine Beeinflussung, also interessiert uns die Fahrt eh nicht
					break;
				}
			}

			// Nun überprüfen wir, ob wir die Flexibilität überschreiten.
			// Es wird immer nur das „Paket“ (toAdd) als ganzes ausgewählt oder gar nicht.
			BigInteger ourFlexibility = BigInteger.ONE;
			for (TripInformation info : toAdd) {
				ourFlexibility = ourFlexibility.multiply(BigInteger.valueOf(info.getConsideredDepartures().length));
				// TODO: sollte not.add() nicht weiter unten stattfinden?
				not.add(info.getTrip());
			}

			if (flexibility.multiply(ourFlexibility).compareTo(maxFlexibility) > 0) {
				// Das ginge ins Auge, wenn wir die auswählten.
				if (discarded < skipLevel) {
					// Wenn wir die Züge aber eh verwerfen sollen, ist das egal. Dann zählen wir einfach den
					// Nachbarschaftszähler eins hoch und schauen uns die nächste Welle an
					discarded++;
					flexibility = BigInteger.ONE;
				} else if (selected.size() >= 2) {
					// Wenn wir die nicht verwerfen sollen, nehmen wir die nur, falls noch keine vorhanden sind, sonst
					// setzen wir dem ganzen ein Ende.
					break;
				}
			}

			// Falls wir nicht mehr wegen dem Übersprung verwerfen, wählen wir die Züge aus 
			if (discarded == skipLevel) {
				selected.addAll(toAdd);
			}

			// Aktualisiere die Flexibiltität
			flexibility = flexibility.multiply(ourFlexibility);
		}

		// Bommel informiert.
		Bommel.debug("Verbessere mit " + selected.size() + " Zügen, Flexibilität: " + flexibility + "/" + maxFlexibility);

		return selected;
	}

	@Override
	public void setMaxFlexibility(BigInteger maxFlexibility) {
		this.maxFlexibility = maxFlexibility;
	}

	@Override
	public void setSkipLevel(int neighbourhoodLevel) {
		this.skipLevel = neighbourhoodLevel;
	}

	@Override
	public void setInstance(Instance instance) {
		this.instance = instance;
	}

	/**
	 * Wählt eine Fahrt aus, von der alle Abfahrtszeiten betrachtet werden sollen.
	 *
	 * @param trips    Fahrten, aus denen die Auswahl getroffen werden soll
	 * @param not      Fahrten, welche nicht mehr ausgewählt werden sollen
	 * @param planning Bisherige Lösung
	 * @return Ausgewählte Fahrt
	 */
	protected Trip selectOne(List<Trip> trips, Collection<Trip> not, TripPlanning planning) {
		throw new UnsupportedOperationException();
	}

	/**
	 * Wählt eine Fahrt aus, zu der noch Abfahrtszeitinformationen angegeben werden.
	 *
	 * @param trips    Fahrten, aus denen die Auswahl getroffen werden soll
	 * @param not      Fahrten, welche nicht mehr ausgewählt werden sollen
	 * @param planning Bisherige Lösung
	 * @return Ausgewählte Fahrt mit Abfahrtszeitinformationen
	 */
	protected TripInformation selectOneExtended(List<Trip> trips, Collection<Trip> not, TripPlanning planning) {
		Trip trip = selectOne(trips, not, planning);

		if (trip == null) {
			return null;
		}

		return new TripInformation(trip);
	}

	@Override
	public String toString() {
		return getClass().getSimpleName() + "(" + (maxFlexibility.toString().length() - 1) + ")";
	}

	/**
	 * Gibt die maximale Flexibilität zurück.
	 *
	 * @return Maximale Flexibilität
	 */
	public BigInteger getMaxFlexibility() {
		return maxFlexibility;
	}
}
