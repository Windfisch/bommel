/*
 * Copyright (c) 2014, Florian Jung, Benjamin Miller, Johannes Thürauf, David Wägner
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package de.abbaddie.bommel.improver;

import de.abbaddie.bommel.data.Instance;
import de.abbaddie.bommel.data.MutableTripPlanning;
import de.abbaddie.bommel.data.Trip;
import de.abbaddie.bommel.data.TripPlanning;
import de.abbaddie.bommel.solver.Solver;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.List;

/**
 * Der {@link SeriousImprover} umfasst mehrere {@link SimpleImprover} und damit normalerweise verschiedene
 * Auswahlstrategien.<br />
 * Er kümmert sich darum, zwischen den verschiedenen Auswahlstrategien zu wechseln und nach Möglichkeit die beste
 * auszuwählen. Das passiert bspw. dadurch, dass nach einem Parameterwechsel immer die Strategie verwendet wird, welche
 * als letztes Erfolg hatte.<br />
 * Er kümmert sich außerdem um die Steuerung des Übersprunglevel-Parameters. Es wird mit 0 gestartet, und falls
 * nacheinander einmal alle Strategien keinen Erfolg hatten, wird um 1 erhöht. Das wird dann wiederholt, bis die
 * obere Schranke (standardmäßig 10) erreicht wird. Gibt es zwischendurch einen Erfolg, wird wieder auf 0
 * zurückgestellt.<br />
 * Verwendet intern mehrere {@link SimpleImprover}.<br />
 * Wird typischerweise im {@link ComplexImprover} verwendet (dort wird die Flexibilität gesteuert).
 *
 * @see SeriousImprover
 * @see ComplexImprover
 * @see SimpleImprover
 */
public class SeriousImprover implements Improver {
	private BigInteger maxFlexibility;

	private List<SimpleImprover> improvers;
	private int currentImproverIdx = 0;
	private int lastSuccessImproverIdx = 0;
	private Instance instance;
	private boolean deeperSkipSuccess = false;
	private int skipLevel = 0;
	private boolean timeLimited = false;
	private int maxSkipLevel = 10;

	/**
	 * Erzeugt einen neuen {@link SeriousImprover} mit den gegebenen Daten.
	 *
	 * @param instance       Probleminstanz
	 * @param solver         Löser
	 * @param maxFlexibility Maximale  Flexibilitäts
	 */
	public SeriousImprover(Instance instance, Solver solver, BigInteger maxFlexibility) {
		this(instance, solver, maxFlexibility, SelectionStrategyAdapter.DEFAULT_MAX_NEIGHBOURS);
	}

	/**
	 * Erzeugt einen neuen {@link SeriousImprover} mit den gegebenen Daten.
	 *
	 * @param instance        Probleminstanz
	 * @param solver          Löser
	 * @param maxFlexibility  Maximale Flexibilität
	 * @param neighboursCount Maximale Anzahl benachbarter Fahrten im selben Zug
	 */
	public SeriousImprover(Instance instance, Solver solver, BigInteger maxFlexibility, int neighboursCount) {
		this.maxFlexibility = maxFlexibility;
		this.instance = instance;

		// TODO: Konfigurierbar machen?
		improvers = Arrays.asList(
				new SimpleImprover(instance, new MostInfluenceSelectionStrategy(maxFlexibility, neighboursCount), solver),
				new SimpleImprover(instance, new SurroundingNegativePeakSelectionStrategy(maxFlexibility, neighboursCount), solver),
				new SimpleImprover(instance, new MostFlexibleSelectionStrategy(maxFlexibility, neighboursCount), solver),
				new SimpleImprover(instance, new LongestTripSelectionStrategy(maxFlexibility, neighboursCount), solver),
				new SimpleImprover(instance, new MostExtremeTripSelectionStrategy(maxFlexibility, neighboursCount), solver),
				new SimpleImprover(instance, new MultiPeakInfluenceSelectionStrategy(maxFlexibility, neighboursCount), solver),
				new SimpleImprover(instance, new MovingPeakSelectionStrategy(instance, maxFlexibility, neighboursCount, MovingPeakSelectionStrategy.Side.LEFT), solver),
				new SimpleImprover(instance, new MovingPeakSelectionStrategy(instance, maxFlexibility, neighboursCount, MovingPeakSelectionStrategy.Side.RIGHT), solver)
		);
	}

	/**
	 * Setzt eine obere Schranke für das Übersprunglevel.
	 *
	 * @param maxSkipLevel Maximales Übersprunglevel
	 */
	public void setMaxSkipLevel(int maxSkipLevel) {
		this.maxSkipLevel = maxSkipLevel;
	}

	/**
	 * Setzt die Probleminstanz.
	 *
	 * @param instance Probleminstanz
	 */
	public void setInstance(Instance instance) {
		this.instance = instance;
		for (SimpleImprover si : improvers)
			si.setInstance(instance);
	}

	/**
	 * Setzt eine obere Schranke für die Flexibilität in jedem Schritt.
	 *
	 * @param maxFlexibility Maximale Flexibilität
	 */
	public void setMaxFlexibility(BigInteger maxFlexibility) {
		this.maxFlexibility = maxFlexibility;

		for (SimpleImprover improver : improvers) {
			improver.getSelectionStrategy().setMaxFlexibility(maxFlexibility);
		}

		deeperSkipSuccess = false;
		skipLevel = 0;
	}

	/**
	 * Gibt zurück, ob der letzte Aufruf von
	 * {@link #improveWithTimeLimit(java.util.List, de.abbaddie.bommel.data.TripPlanning, int)} am Zeitlimit gescheitert
	 * ist.
	 *
	 * @return Zeitlimit
	 */
	public boolean ranIntoTimeLimit() {
		return timeLimited;
	}

	@Override
	public TripPlanning improve(List<Trip> trips, TripPlanning planning) {
		return improveWithTimeLimit(trips, planning, 0);
	}

	/**
	 * Wie {@link #improve(java.util.List, de.abbaddie.bommel.data.TripPlanning)}, nur kann noch ein Zeitlimit angegeben
	 * werden, nach dessen Verstreichen keine weiteren Verbesserungsvorgänge mehr stattfinden. Ob das der Abbruchgrund
	 * war oder nicht, kann mit {@link #ranIntoTimeLimit()} abgefragt werden.
	 *
	 * @param trips     Liste der Fahrten, die optimiert werden sollen
	 * @param planning  Aktuelle Lösung
	 * @param timeLimit Zeitlimit in Sekunden. <code>0</code> für kein Zeitlimit.
	 * @return Ggf. verbesserte Lösung
	 */
	public TripPlanning improveWithTimeLimit(List<Trip> trips, TripPlanning planning, int timeLimit) {
		timeLimited = false;

		// Merke die Leistungsaufnahme der gegebenen Startlösung
		TripPlanning currentPlanning = planning;
		double currentMaxPower = planning.getPowerProfileMaximumOnly().getPowerAt(planning.getPowerProfileMaximumOnly().getMaximumTime());

		// Startzeit aufnehmen, wegen dem Zeitlimit
		long beginTime = System.nanoTime();

		// Übersprunglevels durchgehen
		for (; skipLevel <= maxSkipLevel; skipLevel++) {
			System.out.println("# Verwende Übersprunglevel " + skipLevel);
			deeperSkipSuccess = false;

			// Wir wollen das Übersprunglevel eigentlich nicht antasten, deswegen versuchen wir möglichst lange
			// die einzelnen Improver durch
			while (true) {
				// Zeitbeschränkung beachten
				int timeElapsed = (int)((System.nanoTime() - beginTime) / 1_000_000_000);
				if (timeLimit != 0 && timeElapsed >= timeLimit) {
					timeLimited = true;
					return currentPlanning;
				}

				// Daten initialisieren & Versuch starten
				planning = new MutableTripPlanning(currentPlanning);
				SimpleImprover currentImprover = improvers.get(currentImproverIdx);
				currentImprover.getSelectionStrategy().setSkipLevel(skipLevel);
				try {
					currentImprover.improve(trips, planning);
				} catch (Exception e) {
					planning = currentPlanning;
					e.printStackTrace();
				}

				// Neue Leistungsaufnahme ausrechnen
				int ourMaxTime = planning.getPowerProfileMaximumOnly().getMaximumTime();
				double ourMaxPower = planning.getPowerProfileMaximumOnly().getPowerAt(ourMaxTime);

				if (ourMaxPower < currentMaxPower) {
					// Neue Leistung ist besser
					System.out.printf(currentImprover.toString() + " verbessert um %.3fMW auf %.3fMW. (%d Streckenfahrten, Maximalstelle: %d)\n",
							(currentMaxPower - ourMaxPower) / 1_000_000,
							ourMaxPower / 1_000_000,
							currentImprover.getLastTrainCount(),
							ourMaxTime);

					// Merke die neue Lösung
					currentMaxPower = ourMaxPower;
					currentPlanning = planning;
					instance.setCurrentSolution(currentPlanning);

					// Diesen Improver als gut markieren
					lastSuccessImproverIdx = currentImproverIdx;

					// Übersprunglevel darf nun zurückgesetzt werden, falls es größer als 0 ist
					if (skipLevel > 0) {
						deeperSkipSuccess = true;
					}
				} else {
					// Neue Leistung ist nicht besser
					System.out.printf(currentImprover.toString() + " verbessert nicht. (%d Streckenfahrten)\n",
							currentImprover.getLastTrainCount());

					// Nächster Improver
					currentImproverIdx++;
					currentImproverIdx %= improvers.size();

					if (currentImproverIdx == lastSuccessImproverIdx) {
						// Wir sind einmal durch => andere Parameter sind notwendig
						break;
					}
				}
			}

			// skipLevel wird gleich erhöht. Wenn wir mit 0 starten wollen, müssen wir es hier auf -1 setzen.
			if (deeperSkipSuccess && skipLevel > 0) {
				skipLevel = -1;
			}
		}

		// Alle Übersprunglevel sind ohne Erfolg durch :/
		return currentPlanning;
	}
}
