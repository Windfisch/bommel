/*
 * Copyright (c) 2014, Florian Jung, Benjamin Miller, Johannes Thürauf, David Wägner
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package de.abbaddie.bommel.improver;

import de.abbaddie.bommel.data.Trip;
import de.abbaddie.bommel.data.TripInformation;
import de.abbaddie.bommel.data.TripPlanning;

import java.math.BigInteger;
import java.util.*;


/**
 * Auswahlstrategie, welche Fahrten auswählt, welche die die höchste Leistungsaufnahme haben.
 */
public class MostExtremeTripSelectionStrategy extends BatchingSelectionStrategyAdapter {
	public MostExtremeTripSelectionStrategy(BigInteger maxFlexibility) {
		super(maxFlexibility);
	}

	public MostExtremeTripSelectionStrategy(BigInteger maxFlexibility, int maxNeighbours) {
		super(maxFlexibility, maxNeighbours);
	}

	@Override
	protected List<TripInformation> getNextBatch(TripPlanning planning, Collection<Trip> not) {
		List<Trip> sorted = new ArrayList<>(instance.getTrips());
		sorted.removeAll(not);
		Collections.sort(sorted, new Comparator<Trip>() {
			@Override
			public int compare(Trip o1, Trip o2) {
				float cmp = o1.getPowerProfile(0).getPowerAt(o1.getPowerProfile(0).getMaximumTime()) - o2.getPowerProfile(0).getPowerAt(o2.getPowerProfile(0).getMaximumTime());
				if (cmp > 0) return -1;
				if (cmp < 0) return +1;
				return 0;
			}
		});

		return TripInformation.asTripInformationList(sorted);
	}
}
