/*
 * Copyright (c) 2014, Florian Jung, Benjamin Miller, Johannes Thürauf, David Wägner
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package de.abbaddie.bommel.data;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Speichert einen {@link Trip} zzgl. einer Menge von Abfahrtszeiten.
 * Wird z.B. von den Verbesserern
 * (z.B. {@link de.abbaddie.bommel.improver.SelectionStrategy#select(java.util.List, TripPlanning)})
 * verwendet, die dadurch nicht nur eine Menge von Streckenfahrten auswählen, sondern auch die Abfahrtsmöglichkeiten der
 * ausgewählten Fahrten (bspw. anhand Verschieberichtung oder -weite).
 */
public class TripInformation {
	private Trip trip;
	private int[] consideredDepartures;

	/**
	 * Erstellt ein {@link TripInformation} mit den gegebenen Abfahrtszeiten.
	 *
	 * @param trip                 Streckenfahrt
	 * @param consideredDepartures Abfahrtszeiten
	 */
	public TripInformation(Trip trip, int... consideredDepartures) {
		this.trip = trip;
		this.consideredDepartures = consideredDepartures;
	}

	/**
	 * Erstellt ein {@link TripInformation}, wobei alle möglichen Abfahrtszeiten ({@link Trip#getAllowedDepartures()}
	 * übernommen werden.
	 *
	 * @param trip Streckenfahrt
	 */
	public TripInformation(Trip trip) {
		this.trip = trip;
		this.consideredDepartures = trip.getAllowedDepartures();
	}

	/**
	 * Gibt die zugehörige Streckenfahrt zurück.
	 *
	 * @return Streckenfahrt
	 */
	public Trip getTrip() {
		return trip;
	}

	/**
	 * Gibt die betrachteten Abfahrtszeiten zurück.
	 *
	 * @return Abfahrtszeiten
	 */
	public int[] getConsideredDepartures() {
		return consideredDepartures;
	}

	/**
	 * Gibt das arithmetische Mittel der möglichen Abfahrtszeiten zurück.
	 *
	 * @return Abfahrtszeitenmittel
	 */
	public int getConsideredDeparturesMid() {
		int sum = 0;

		for (int i : getConsideredDepartures())
			sum += i;

		return sum / getConsideredDepartures().length;
	}

	/**
	 * Überschreibt die betrachteten Abfahrtszeiten
	 *
	 * @param consideredDepartures Abfahrtszeiten
	 */
	public void setConsideredDepartures(int... consideredDepartures) {
		this.consideredDepartures = consideredDepartures;
	}

	@Override
	public String toString() {
		return "{" + trip.toString() + ":" + Arrays.toString(consideredDepartures) + "}";
	}

	/**
	 * Gibt eine Liste von Streckenfahrten zurück als Liste von {@link TripInformation}, wobei alle möglichen
	 * Abfahrtszeiten betrachtet werden.
	 *
	 * @param trips Abfahrtszeiten
	 * @return „Gewrappte“ Liste
	 */
	public static List<TripInformation> asTripInformationList(List<Trip> trips) {
		List<TripInformation> tripInformations = new ArrayList<>(trips.size());

		for (Trip trip : trips) {
			tripInformations.add(new TripInformation(trip));
		}

		return tripInformations;
	}

	/**
	 * Gibte eine Liste mit den „gewrappten“ Streckenfahrten zurück.
	 *
	 * @param tripInformations Liste
	 * @return „Entwrappte“ Liste
	 */
	public static List<Trip> asTripList(List<TripInformation> tripInformations) {
		List<Trip> trips = new ArrayList<>(tripInformations.size());

		for (TripInformation tripInformation : tripInformations) {
			trips.add(tripInformation.getTrip());
		}

		return trips;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		TripInformation that = (TripInformation)o;

		if (!Arrays.equals(consideredDepartures, that.consideredDepartures)) return false;
		if (!trip.equals(that.trip)) return false;

		return true;
	}

	@Override
	public int hashCode() {
		int result = trip.hashCode();
		result = 31 * result + Arrays.hashCode(consideredDepartures);
		return result;
	}
}
