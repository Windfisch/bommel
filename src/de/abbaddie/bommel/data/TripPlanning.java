/*
 * Copyright (c) 2014, Florian Jung, Benjamin Miller, Johannes Thürauf, David Wägner
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package de.abbaddie.bommel.data;

import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/**
 * <p>
 * {@link TripPlanning} ist dazu da, die Abfahrtszeiten der Züge zu speichern.<br />
 * Dies wäre eigentlich auch einfacher in der Trip-Klasse möglich, so ergeben sich aber verschiedene Vorteile:<ul>
 * <li>Multithreadingfähigkeit (jeder Thread bekommt ein eigenes TripPlanning)</li>
 * <li>„Beiseiteschieben“ von Planungen (damit ältere, bessere Lösungen gemerkt werden können)</li>
 * <li>Verschiedene Implementierungen sind möglich, jeweils angepasst an die Verwendungsmuster der Lösungsalgorithmen.</li></ul><br />
 * TripPlanning ist an eine Probleminstanz gebunden und enthält Einträge, die Streckenfahrten ihre Abfahrtszeiten zuordnen.<br />
 * </p>
 * <p>
 * Man beachte, dass jede Lösung durch {@link TripPlanning} repräsentiert ist, aber nicht umgekehrt. In einer Planung gespeicherte Werte sind nicht
 * notwendigerweise vollständig, einzelne Züge oder gültig (vgl. bspw. Verwendung in {@link SuperTrip}).
 * </p>
 * <p>
 * Aus Optimierungsgründen (zur Organisierung der Wiederverwendung) ist {@link TripPlanning} außerdem dafür zuständig,
 * die Leistungsprofile zu erstellen.
 * </p>
 */
public interface TripPlanning extends Iterable<TripPlanning.Entry> {
	/**
	 * Wird zurückgegeben, wenn die Abfahrtszeit nicht in dieser Planung enthalten ist.
	 */
	public int INVALID_DEPARTURE = Integer.MIN_VALUE;

	/**
	 * Gibt die Probleminstanz zurück, an die diese Planung gebunden ist.
	 *
	 * @return Probleminstanz
	 */
	public Instance getInstance();

	/**
	 * Fügt dieser Planung eine Abfahrt hinzu. Nur der Rückgabewert enthält die übergebenen Werte garantiert. Je nach Implementierung
	 * kann das aktuelle Objekt verändert werden ({@link MutableTripPlanning}), oder auch nicht ({@link LinkedTripPlanning}). <br />
	 * Bereits gesetzte Werte werden ggfs. überschrieben
	 *
	 * @param trip      Streckenfahrt
	 * @param departure Abfahrtszeit
	 * @return Planung, welche diese Daten enthält.
	 */
	public TripPlanning withDeparture(Trip trip, int departure);

	/**
	 * Gibt die Abfahrtszeit einer Streckenfahrt zurück. Man beachte, dass der Zugriff ggfs. sehr ineffizient sein kann
	 * ({@link LinkedTripPlanning}). Um alle Werte durchzugehen, sollte man den Iterator verwenden.
	 *
	 * @param trip Streckenfahrt
	 * @return Abfahrtszeit
	 */
	public int getDeparture(Trip trip);

	/**
	 * Gibt eine neue Planung zurück, welche auf die übergebenen Streckenfahrten eingeschränkt wurde.
	 *
	 * @param trips Streckenfahrt
	 * @return Eingeschränkte Planung
	 */
	public TripPlanning retain(Set<Trip> trips);

	/**
	 * Gibt das Leistungsprofil über alle Streckenfahrten dieser Planung zurück.<br />
	 *
	 * @return Leistungsprofil
	 */
	public PlanningPowerProfile getPowerProfile();

	/**
	 * Gibt ein Leistungsprofil zurück, welches nur dafür gedacht ist, dass darauf
	 * {@link PowerProfile#getMaximumTime()} und {@link PowerProfile#getPowerAt(int)} mit dem Rückgabewert des ersteren
	 * aufgerufen werden.<br />
	 * Ist typischerweise schneller als {@link #getPowerProfile()}
	 *
	 * @return Optimiertes Leitungsprofil.
	 */
	public PlanningPowerProfile getPowerProfileMaximumOnly();

	@Override
	public Iterator<Entry> iterator();

	/**
	 * Gibt die Planung als Java-{@link java.util.Map} zurück. Für den normalen Zugriff sollten die anderen Methoden verwendet werden.
	 * Dies ist v.a. eine Hilfsmethode, um equals() und hashCode() effizient zu implementieren.
	 *
	 * @return Map
	 */
	public Map<Trip, Integer> asMap();

	/**
	 * Ein Eintrag in einer Planung.
	 */
	public static class Entry {
		private Trip trip;
		private int departure;

		/**
		 * Erstellt einen neuen Planungseintrag.
		 *
		 * @param trip      Streckenfahrt
		 * @param departure Abfahrtszeit
		 */
		public Entry(Trip trip, int departure) {
			this.trip = trip;
			this.departure = departure;
		}

		/**
		 * Gibt die Streckenfahrt zurück.
		 *
		 * @return Streckenfahrt
		 */
		public Trip getTrip() {
			return trip;
		}

		/**
		 * Gibt die Abfahrtszeit zurück.
		 *
		 * @return Abfahrtszeit
		 */
		public int getDeparture() {
			return departure;
		}
	}
}
