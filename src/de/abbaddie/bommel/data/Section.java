/*
 * Copyright (c) 2014, Florian Jung, Benjamin Miller, Johannes Thürauf, David Wägner
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package de.abbaddie.bommel.data;

import java.util.HashSet;
import java.util.Set;

/**
 * Eine Strecke.<br />
 * Ist eigentlich erst für AS3 relevant, damit der Einbau aber später nicht schwierig wird, bereits ab AS1
 * berücksichtigt.<br />
 * Evtl. könnte analog zu {@link Trip} und {@link Train} eine Abstrahierung sinnvoll sein.
 */
public final class Section {
	private int id;

	private Set<RealTrip> trips = new HashSet<>();

	/**
	 * Gibt die laufende Nummer zurück.
	 *
	 * @return Nummer
	 */
	public int getId() {
		return id;
	}

	/**
	 * Setzt die laufende Nummer. Sollte nur von {@link de.abbaddie.bommel.util.InstParser} aufgerufen werden!
	 *
	 * @param id Nummber
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * Gibt die Fahrten, die auf dieser Strecke verkehren, zurück.
	 *
	 * @return Fahrten
	 */
	public Set<RealTrip> getTrains() {
		return trips;
	}

	/**
	 * Fügt einee Fahrt zu dieser Strecke hinzu. Sollte nur von {@link de.abbaddie.bommel.util.InstParser} aufgerufen werden!
	 *
	 * @param trip Fahrt
	 */
	public void addTrip(RealTrip trip) {
		trips.add(trip);
	}

	public String toString() {
		return "Section" + Integer.toString(id);
	}
}
