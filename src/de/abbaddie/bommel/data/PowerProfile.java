/*
 * Copyright (c) 2014, Florian Jung, Benjamin Miller, Johannes Thürauf, David Wägner
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package de.abbaddie.bommel.data;

/**
 * Ein Leistungsprofil. Da Leistungsprofile aus verschiedenen Quellen stammen können
 * (einzelner Zug vs. mehrere Züge) ist dies nur eine Schnittstelle.<br />
 * Leistungsprofile werden v.a. von den Streckenfahrten ({@link Trip#getPowerProfile(int)}) und von den Planungen
 * ({@link TripPlanning#getPowerProfile()} und {@link TripPlanning#getPowerProfileMaximumOnly()}) erzeugt.
 *
 * @see RawPowerProfile
 */
public interface PowerProfile {
	/**
	 * Gibt den Leistungswert zu einem gewissen Zeitpunkt zurück.
	 *
	 * @param time Zeitpunkt
	 * @return Leistungswert
	 */
	public float getPowerAt(int time);

	/**
	 * Gibt den Zeitpunkt zurück, zu dem die höchste Leitungsaufnahme stattfindet.
	 *
	 * @return Zeitpunkt der maximalen Leistungsaufnahme
	 */
	public int getMaximumTime();

	/**
	 * Gibt den Zeitpunkt zurück, zu dem die niedrigste Leitungsaufnahme stattfindet. Die Leistungsaufnahme wird zu
	 * dieser Zeit in aller Regel negativ sein, d.h. dort eine Leistungsrückspeißung stattfinden.
	 *
	 * @return Zeitpunkt der minimalen Leistungsaufnahme
	 */
	public int getMinimumTime();

	/**
	 * Gibt den ersten Zeitpunkt zurück, zu dem das Leitungsprofil explizit definiert ist
	 * (Startzeit der Streckenfahrt bspw.)
	 *
	 * @return Startzeitpunkt (ein durch 600 teilbarer Wert)
	 */
	public int getStart();

	/**
	 * Gibt den ersten Zeitpunkt zurück, zu dem das Leitungsprofil explizit definiert ist
	 * (Endzeit der Streckenfahrt bspw.)
	 *
	 * @return Endzeitpunkt (ein durch 600 teilbarer Wert)
	 */
	public int getEnd();

	/**
	 * Gibt das Leistungsprofil als Array zurück. Es wird erwartet, dass in das Array nicht geschrieben wird.<br/>
	 * Der an Stelle 0 befindliche Wert ist der Leistungswert, der zur durch {@link #getStart()} definierten Zeit aufgenommen wird.<br />
	 * Der an letzter Stelle befindliche Wert ist der Leistungswert, der zur durch {@link #getEnd()} definierten Zeit aufgenommen wird.<br />
	 * Die Länge ist kongruent 1 modulo 600.
	 *
	 * @return Leistungsprofil als Array
	 */
	public float[] getArray();
}
