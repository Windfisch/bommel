/*
 * Copyright (c) 2014, Florian Jung, Benjamin Miller, Johannes Thürauf, David Wägner
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package de.abbaddie.bommel.data;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

/**
 * Eine Vereinigung mehrerer, relativ zueinander schon fertig geplanter Streckenfahrten.
 * Zur relativen Planung wird {@link TripPlanning} verwendet.
 */
public final class SuperTrip implements Trip {
	private TripPlanning relPlanning;

	// Gecachte Werte
	private int earliest;
	private int latest;
	private int myHash;

	/**
	 * Erstellt einen {@link SuperTrip} mit den gegeben Zügen under gegebenen relativen Planung.
	 *
	 * @param trips       Züge
	 * @param relPlanning Planung
	 */
	public SuperTrip(List<Trip> trips, TripPlanning relPlanning) {
		this.relPlanning = new MutableTripPlanning(relPlanning).retain(new HashSet<>(trips));

		earliest = 0;
		latest = relPlanning.getInstance().getSchedulingInterval();

		myHash = 0;
		for (TripPlanning.Entry entry : relPlanning) {
			int tripEarliest = entry.getTrip().getAllowedDepartureEarliest() - entry.getDeparture();
			int tripLatest = entry.getTrip().getAllowedDepartureLatest() - entry.getDeparture();

			earliest = Math.max(earliest, tripEarliest);
			latest = Math.min(latest, tripLatest);

			myHash = 31 * myHash + (entry.getTrip().hashCode() + 17 * entry.getDeparture());
		}
	}

	@Override
	public int hashCode() {
		return myHash;
	}

	@Override
	public int[] getAllowedDepartures() {
		return TripSupport.getAllowedDepartures(this);
	}

	@Override
	public boolean affects(TripPlanning planning, TripInformation other) {
		return TripSupport.affects(this, other, planning);
	}

	@Override
	public boolean collides(int thisDeparture, Trip other, int otherDeparture) {
		for (TripPlanning.Entry entry : relPlanning) {
			Trip trip = entry.getTrip();
			if (trip.collides(thisDeparture + relPlanning.getDeparture(trip), other, otherDeparture))
				return true;
		}

		return false;
	}

	@Override
	public List<Constraint> constraints(Trip other) {
		List<Constraint> result = new ArrayList<>();

		for (TripPlanning.Entry entry : relPlanning) {
			Trip trip = entry.getTrip();
			List<Constraint> temp = trip.constraints(other);
			for (Constraint c : temp) {
				// c:  trip.dept + foo < other.dept + bar
				// das bedeutet für uns (also this):
				// c': this.dept + relPlanning.getDept(trip) + foo < other.dept + bar + 0
				if (c.getLeftTrip() == trip)
					result.add(new Constraint(this, c.leftOffset() + relPlanning.getDeparture(trip), c.getRightTrip(), c.rightOffset()));
				else if (c.getRightTrip() == trip)
					result.add(new Constraint(c.getLeftTrip(), c.leftOffset(), this, c.rightOffset() + relPlanning.getDeparture(trip)));
				else
					throw new RuntimeException("This cannot happen"); // ja, genau flo!
			}
		}

		return result;
	}

	public int getAllowedDepartureEarliest() {
		return earliest;
	}

	public int getAllowedDepartureLatest() {
		return latest;
	}

	public TripPowerProfile getPowerProfile(int departure) {
		return new SuperTripPowerProfile(relPlanning, departure);
	}

	/**
	 * Nimmt alle (Unter-)Trips, die in diesem SuperTrip vereinigt sind, und schreibt ihre
	 * Abfahrtszeit – unter Berücksichtigung der relativen Planung und der Abfahrtszeit dieses
	 * SuperTrips – in eine {@link TripPlanning}-Struktur.
	 * Im Endeffekt wird hiermit eine „Wrapperschicht“ wieder abgelegt; der Aufrufer wird
	 * typischerweise diese Funktion von mehreren SuperTrips kurz nacheinander aufrufen,
	 * dabei wird eine „Liste enthält SuperTrips enthält Trips“-Hierarchie abgeflacht zu einer
	 * "Liste enthält Trips"-Hierarchie.
	 * 
	 * @param planning Planung, zu der die Fahrten hinzugefügt werden sollen.
	 * @param departure Abfahrtszeit dieses SuperTrips
	 * @return Planung, zu der die Fahrten dieses SuperTrips hinzugefügt wurden   
	 */
	public TripPlanning getPlanningWithOffset(TripPlanning planning, int departure) {
		for (TripPlanning.Entry entry : relPlanning) {
			planning = planning.withDeparture(entry.getTrip(), entry.getDeparture() + departure);
		}

		return planning;
	}

	@Override
	public int getDwell() {
		// Eventuell ist hier ein anderer Wert nötig, wenn mehrere SuperTrips geplant werden.
		// Taucht zur Zeit nicht auf, da Teile-und-Herrsche eh nur für AS1 funktioniert
		return 0;
	}

	@Override
	public int getJourneytime() {
		return relPlanning.getPowerProfile().getEnd() - relPlanning.getPowerProfile().getStart();
	}

	@Override
	public int getOccupancyTime() {
		return getDwell() + getJourneytime();
	}

	@Override
	public String toString() {
		return "SuperTrip:" + String.valueOf(relPlanning);
	}

}
