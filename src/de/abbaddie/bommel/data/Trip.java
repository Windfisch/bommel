/*
 * Copyright (c) 2014, Florian Jung, Benjamin Miller, Johannes Thürauf, David Wägner
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package de.abbaddie.bommel.data;

import java.util.List;

/**
 * Eine Streckenfahrt.
 * Sie ist spezifiert durch:<ul>
 * <li>das erlaubte Abfahrtszeitintervall,</li>
 * <li>das Leistungsprofil und</li>
 * <li>(ab AS2) die Mindesthaltezeit.</li></ul>
 * Die konkret errechnete Abfahrtszeit wird von TripPlanning verwaltet.
 *
 * @see de.abbaddie.bommel.data.Train
 */
public interface Trip {
	/**
	 * Gibt die frühstestmögliche Abfahrtszeit zurück.
	 *
	 * @return Abfahrtszeit
	 */
	public int getAllowedDepartureEarliest();

	/**
	 * Gibt die spätestmögliche Abfahrtszeit zurück.
	 *
	 * @return Abfahrtszeit
	 */
	public int getAllowedDepartureLatest();

	/**
	 * Gibt ein Array mit den möglichen Abfahrtszeiten zurück.
	 *
	 * @return Abfahrtszeiten
	 */
	public int[] getAllowedDepartures();

	/**
	 * Gibt die Mindesthaltezeit nach Ankunft an.
	 *
	 * @return Mindesthaltezeit
	 */
	public int getDwell();

	/**
	 * Gibt die Fahrtzeit zurück.
	 *
	 * @return Fahrtzeit.
	 */
	public int getJourneytime();

	/**
	 * Gibt die Belegungszeit zurück, d.h. die Fahrtzeit zzgl. Mindesthaltezeit.
	 *
	 * @return Belegungszeit
	 */
	public int getOccupancyTime();

	/**
	 * Gibt das Leistungsprofil für eine gegebene Abfahrtszeit zurück.
	 *
	 * @param departure Abfahrtszeit
	 * @return Leistungsprofil
	 */
	public TripPowerProfile getPowerProfile(int departure);


	/**
	 * Gibt zurück, ob die Abfahrtsmöglichkeiten der anderen Streckenfahrt durch diese Fahrt eingeschränkt werden, wenn
	 * diese Fahrt wie in <code>planning</code> angegeben abfährt.
	 *
	 * @param planning Aktuelle Planung (dieser Zug wird auf diese fixiert)
	 * @param other    Abfahrtsinformation der anderen Streckenfahrt
	 * @return Wert, ob eine Einschränkung stattfindet.
	 */
	public boolean affects(TripPlanning planning, TripInformation other);

	/**
	 * Gibt zurück, ob die Abfahrt der anderen Fahrt möglich ist, wenn diese Streckenfahrt so abfährt wie angegeben.
	 *
	 * @param thisDeparture  Abfahrtszeit dieser Streckenfahrt
	 * @param other          Andere Streckenfahrt
	 * @param otherDeparture Abfahrtszeit der anderen Streckenfahrt
	 * @return Wert, ob die Abfahrten so erlaubt sind.
	 */
	public boolean collides(int thisDeparture, Trip other, int otherDeparture);

	/**
	 * Gibt eine Liste von Constraints zurück, die sich aus dieser Streckenfahrt mit einer anderen ergeben.<br />
	 * Die Funktion ist symmetrisch, d.h.
	 * <code>a.constraints(b).equals(b.contraints(a))</code> ist immer <code>true</code>.
	 *
	 * @param other Andere Streckenfahrt
	 * @return Liste der Constraints
	 */
	public List<Constraint> constraints(Trip other);
}
