/*
 * Copyright (c) 2014, Florian Jung, Benjamin Miller, Johannes Thürauf, David Wägner
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package de.abbaddie.bommel.data;

import java.util.Collections;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/**
 * Rekursive, unveränderliche Implementierung für Planungen.<br />
 * Ursprünglich entwickelt und optimiert für {@link de.abbaddie.bommel.solver.BruteforceSolver}, verwendet es bislang
 * berechnete Leistungsprofile wieder, wenn Werte hinzugefügt wurden oder „frische“ verändert wurden.
 * Seine individuellen Stärken spielt es aber auch im {@link de.abbaddie.bommel.solver.DivideAndConquerSolver} und v.a.
 * {@link de.abbaddie.bommel.solver.LinearSolver} aus.
 *
 * @see LinkedTripPlanningNode
 * @see MutableTripPlanning
 */
public final class LinkedTripPlanning implements TripPlanning {
	private final Instance instance;
	private final LinkedTripPlanningNode first;

	/**
	 * Erstellt ein neues leeres {@link LinkedTripPlanning}.
	 *
	 * @param instance Probleminstanz
	 */
	public LinkedTripPlanning(Instance instance) {
		this.instance = instance;
		first = new LinkedTripPlanningNode();
	}

	/**
	 * Erstellt ein neues {@link LinkedTripPlanning} als Kopie einer anderen Planung.
	 *
	 * @param other Andere Planung
	 */
	public LinkedTripPlanning(TripPlanning other) {
		instance = other.getInstance();

		LinkedTripPlanningNode node = new LinkedTripPlanningNode();

		for (Entry entry : other) {
			node = new LinkedTripPlanningNode(entry.getTrip(), entry.getDeparture(), node);
		}

		first = node;
	}

	/**
	 * Erstellt ein neues {@link LinkedTripPlanning}, mit dem übergebenen Startknoten.
	 *
	 * @param instance Probleminstanz
	 * @param first    Startknoten
	 */
	public LinkedTripPlanning(Instance instance, LinkedTripPlanningNode first) {
		this.instance = instance;

		this.first = first;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || !(o instanceof TripPlanning)) return false;

		TripPlanning that = (TripPlanning)o;

		return asMap().equals(that.asMap());
	}

	@Override
	public int hashCode() {
		throw new UnsupportedOperationException();
	}

	@Override
	public Instance getInstance() {
		return instance;
	}

	@Override
	public TripPlanning withDeparture(Trip trip, int departure) {
		if (departure < trip.getAllowedDepartureEarliest())
			throw new IllegalArgumentException(trip.toString() + " has illegal departure (-" + Integer.toString(trip.getAllowedDepartureEarliest() - departure) + ")!");
		if (departure > trip.getAllowedDepartureLatest())
			throw new IllegalArgumentException(trip.toString() + " has illegal departure (+" + Integer.toString(departure - trip.getAllowedDepartureLatest()) + ")!");

		LinkedTripPlanningNode result = first.update(trip, departure);

		if (result == null) {
			result = new LinkedTripPlanningNode(trip, departure, first);
		}

		return new LinkedTripPlanning(instance, result);
	}

	@Override
	public int getDeparture(Trip trip) {
		return first.get(trip);
	}

	@Override
	public TripPlanning retain(Set<Trip> trips) {
		LinkedTripPlanningNode newFirst = first.retain(trips);

		return new LinkedTripPlanning(instance, newFirst);
	}

	@Override
	public BinaryPowerProfile getPowerProfile() {
		return first.getPowerProfile(getInstance());
	}

	@Override
	public BinaryPowerProfile getPowerProfileMaximumOnly() {
		return first.getPowerProfileMaximumOnly(getInstance());
	}

	@Override
	public Iterator<TripPlanning.Entry> iterator() {
		return first.iterator();
	}

	@Override
	public Map<Trip, Integer> asMap() {
		return Collections.unmodifiableMap(first.asMap());
	}

	@Override
	public String toString() {
		return "[" + first.toString().substring(2) + "]";
	}

}
