/*
 * Copyright (c) 2014, Florian Jung, Benjamin Miller, Johannes Thürauf, David Wägner
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package de.abbaddie.bommel.data;

import java.util.Arrays;

/**
 * Addiert zwei Leistungsprofile zu einem zusammen.
 * Dabei muss das erste ebenfalls ein {@link BinaryPowerProfile} sein (aus Optimierungsgründen).
 * Für den Rekursionsanfang steht {@link #EMPTY} bereit.
 * <p/>
 * Wird in {@link LinkedTripPlanning} genutzt.
 */
public final class BinaryPowerProfile implements PlanningPowerProfile {
	/**
	 * Ein {@link BinaryPowerProfile}, welches überall 0 ist.
	 */
	public static final BinaryPowerProfile EMPTY = new BinaryPowerProfile();

	private Instance instance;

	private float[] profile;
	private int maxTime = -1;

	/**
	 * Erstellt das leere {@link BinaryPowerProfile}.
	 */
	private BinaryPowerProfile() {
		profile = new float[0];
		maxTime = 1;
	}

	/**
	 * Erstellt ein neues {@link BinaryPowerProfile}.
	 * Es kann übergeben werden, ob das Profil nur zur Maximumsberechnung (und nicht für weitere Profile) genutzt wird.
	 * Wenn ja, braucht kein neues vollständiges Array erstellt zu werden.
	 *
	 * @param instance    Probleminstanz
	 * @param profile1    Existierendes {@link BinaryPowerProfile}
	 * @param profile2    Zusätzliches Leistungsprofil
	 * @param maximumOnly Gibt an, ob das Profil nur zur Maximumsberechnung genutzt wird.
	 */
	public BinaryPowerProfile(Instance instance, BinaryPowerProfile profile1, TripPowerProfile profile2, boolean maximumOnly) {
		this.instance = instance;

		if (maximumOnly) {
			createCachedMaximumOnly(profile1, profile2);
		} else {
			createCached(profile1, profile2);
		}
	}

	private void createCached(BinaryPowerProfile profile1, TripPowerProfile profile2) {
		int end = instance.getSchedulingInterval();

		// Erstelle unser Array
		if (profile1 == EMPTY) profile = new float[end];
		else profile = Arrays.copyOf(profile1.profile, end);

		int offset = profile2.getStart();
		float[] profile2Array = profile2.getArray();

		// Setze Standardwerte des Maximums.
		int maxTime = 0;
		double maxVal = Double.NEGATIVE_INFINITY;
		int i;

		// Versuche, das Maximum aus dem vorherigen Binärprofil zu übernehmen.
		// Dies ist dann möglich, wenn es innerhalb des explizit definierten Bereichs des zusätzlichen Profils liegt
		// und der Wert des zusätzlichen Profils dort positiv ist. Dann können die Werte des neuen Gesamtprofils im Bereich,
		// in dem das zusätzliche Profil nur implizit definiert ist, nicht höher sein als das alte Maximum.
		// Nur im explizit definierten Bereich das zusätzlichen Profils können sich evtl. neue Maxima ergeben.
		int inheritedMaxTime = profile1.maxTime;
		boolean inheritMaximum = inheritedMaxTime >= offset
				&& inheritedMaxTime < offset + profile2Array.length
				&& profile2Array[inheritedMaxTime - offset] >= 0;

		if (inheritMaximum) {
			// Übernehme das Maximum
			maxTime = inheritedMaxTime;
			maxVal = profile[maxTime];

			// Überspringe die vordere Schleife
			i = offset;
		} else {
			i = 0;
			// Erste Schleife: Einfach nur Maximumsberechnung, keine Werteaddition
			for (; i < offset; i++) {
				if (profile[i] > maxVal) {
					maxVal = profile[i];
					maxTime = i;
				}
			}
		}
		// Zweite Schleife: Werteaddition + Maximumsberechnung
		for (int j = 0; j < profile2Array.length && i < end; i++, j++) {
			profile[i] += profile2Array[j];
			if (profile[i] > maxVal) {
				maxVal = profile[i];
				maxTime = i;
			}
		}
		if (!inheritMaximum) {
			// Dritte Schleife: Wieder nur Maximumsberechnung
			for (; i < end; i++) {
				if (profile[i] > maxVal) {
					maxVal = profile[i];
					maxTime = i;
				}
			}
		}
		this.maxTime = maxTime;
	}

	private void createCachedMaximumOnly(BinaryPowerProfile profile1, TripPowerProfile profile2) {
		int end = instance.getSchedulingInterval();

		// Erstelle unser Array
		if (profile1 == EMPTY) {
			profile = new float[1];
			maxTime = 0;
			return;
		} else profile = profile1.profile;

		int offset = profile2.getStart();
		float[] profile2Array = profile2.getArray();

		// Setze Standardwerte des Maximums.
		float maxVal;

		// Versuche, das Maximum aus dem vorherigen Binärprofil zu übernehmen.
		// Dies ist dann möglich, wenn es innerhalb des explizit definierten Bereichs des zusätzlichen Profils liegt
		// und der Wert des zusätzlichen Profils dort positiv ist. Dann können die Werte des neuen Gesamtprofils im Bereich,
		// in dem das zusätzliche Profil nur implizit definiert ist, nicht höher sein als das alte Maximum.
		// Nur im explizit definierten Bereich das zusätzlichen Profils können sich evtl. neue Maxima ergeben.
		int inheritedMaxTime = profile1.maxTime;
		boolean inheritMaximum = inheritedMaxTime >= offset
				&& inheritedMaxTime < offset + profile2Array.length
				&& profile2Array[inheritedMaxTime - offset] >= 0;

		int i = 0;
		if (inheritMaximum) {
			// Übernehme das Maximum
			maxVal = profile[inheritedMaxTime];

			i = offset;
		} else {
			maxVal = Float.NEGATIVE_INFINITY;
			// Erste Schleife: Einfach nur Maximumsberechnung, keine Werteaddition
			for (; i < offset; i++) {
				if (profile[i] > maxVal) {
					maxVal = profile[i];
				}
			}
		}

		// Zweite Schleife: Werteaddition + Maximumsberechnung
		for (int j = 0; j < profile2Array.length && i < end; i++, j++) {
			float val = profile[i] + profile2Array[j];
			if (val > maxVal) {
				maxVal = val;
			}
		}
		if (!inheritMaximum) {
			// Dritte Schleife: Wieder nur Maximumsberechnung
			for (; i < end; i++) {
				if (profile[i] > maxVal) {
					maxVal = profile[i];
				}
			}
		}

		// Hacky, funktioniert aber und ist schnell.
		profile = new float[]{maxVal};
		this.maxTime = 0;
	}

	@Override
	public final float getPowerAt(int time) {
		if (time < 0 || time >= profile.length) return 0;

		return profile[time];
	}

	@Override
	public final int getMaximumTime() {
		return maxTime;
	}

	@Override
	public int getMinimumTime() {
		throw new UnsupportedOperationException();
	}

	@Override
	public int getStart() {
		return 0;
	}

	@Override
	public int getEnd() {
		return instance.getSchedulingInterval();
	}

	@Override
	public float[] getArray() {
		if (profile.length == 1) {
			throw new IllegalStateException(); // maximum only.
		}
		return profile;
	}
}
