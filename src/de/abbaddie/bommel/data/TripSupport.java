/*
 * Copyright (c) 2014, Florian Jung, Benjamin Miller, Johannes Thürauf, David Wägner
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package de.abbaddie.bommel.data;

/**
 * Gemeinsame Funktionen für Streckenfahrtimplementierungen..
 */
public final class TripSupport {
	/**
	 * Gibt ein Array mit den möglichen Abfahrtszeiten zurück.<br />
	 * Verwendet hierfür {@link de.abbaddie.bommel.data.Trip#getAllowedDepartureEarliest()} und
	 * {@link de.abbaddie.bommel.data.Trip#getAllowedDepartureLatest()}.
	 *
	 * @return Abfahrtszeiten
	 * 
	 * @see Trip#getAllowedDepartures() 
	 */
	public static int[] getAllowedDepartures(Trip trip) {
		int allowedDepartureEarliest = trip.getAllowedDepartureEarliest();
		int allowedDepartureLatest = trip.getAllowedDepartureLatest();

		int size = (allowedDepartureLatest - allowedDepartureEarliest) / 600 + 1;
		if (size <= 0) return new int[0];
		int[] arr = new int[size];
		for (int i = 0; i < size; i++) {
			arr[i] = allowedDepartureEarliest + i * 600;
		}

		return arr;
	}


	/**
	 * Gibt zurück, ob die Abfahrtsmöglichkeiten der anderen Streckenfahrt durch diese Fahrt eingeschränkt werden, wenn
	 * diese Fahrt wie in <code>planning</code> angegeben abfährt.<br />
	 * Verwendet hierfür {@link TripInformation#getConsideredDepartures()} und {@link Trip#collides(int, Trip, int)}.
	 *
	 * @param planning Aktuelle Planung (dieser Zug wird auf diese fixiert)
	 * @param other    Abfahrtsinformation der anderen Streckenfahrt
	 * @return Wert, ob eine Einschränkung stattfindet.
	 * 
	 * @see Trip#affects(TripPlanning, TripInformation)
	 * @see Trip#collides(int, Trip, int)
	 */
	public static boolean affects(Trip trip, TripInformation other, TripPlanning planning) {
		int thisDept = planning.getDeparture(trip);

		for (int otherDept : other.getConsideredDepartures())
			if (trip.collides(thisDept, other.getTrip(), otherDept))
				return true;

		return false;
	}

}
