/*
 * Copyright (c) 2014, Florian Jung, Benjamin Miller, Johannes Thürauf, David Wägner
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package de.abbaddie.bommel.data;

/**
 * Ein einfaches Leistungsprofil, welches einfach aus den gegebenen Daten gelesen wurde.
 * Es ist zu unterscheiden von den „richtigen“ (angewendeten) Leistungsprofilen ({@link PowerProfile}), welche
 * sich einbezüglich der Streckenfahrtsplanung (also den Abfahrtszeiten) ergeben.
 *
 * @see PowerProfile
 */
public final class RawPowerProfile {
	private float[] profile;
	private int max;
	private int min;

	/**
	 * Erstellt ein neues rohes Leistungsprofil
	 *
	 * @param profile Rohdaten
	 */
	public RawPowerProfile(float[] profile) {
		this.profile = profile;

		for (int i = 1; i < profile.length; i++) {
			if (profile[i] > profile[max]) max = i;
			if (profile[i] < profile[min]) min = i;
		}
	}

	/**
	 * Gibt die Länge des Leistungsprofils zurück.
	 *
	 * @return Länge
	 */
	public int getLength() {
		return profile.length;
	}

	/**
	 * Das Leistungsprofil als Array.
	 *
	 * @return Leistungsprofil
	 */
	float[] getProfile() {
		return profile;
	}

	/**
	 * Gibt die Leistungsaufnahme zu einem gewissen Zeitpunkt zurück.
	 *
	 * @param time Zeitpunkt
	 * @return Leistungsaufnahme
	 */
	public float getPowerAt(int time) {
		if (time < 0 || time >= profile.length) return 0;
		return profile[time];
	}

	/**
	 * Gibt den Zeitpunkt der höchsten Leistungsaufnahme zurück.
	 *
	 * @return Zeitpunkt
	 */
	public int getMaximumTime() {
		return max;
	}

	/**
	 * Gibt den Zeitpunkt der minimalen Leistungsaufnahme zurück.
	 *
	 * @return Zeitpunkt
	 */
	public int getMinimumTime() {
		return min;
	}
}
