/*
 * Copyright (c) 2014, Florian Jung, Benjamin Miller, Johannes Thürauf, David Wägner
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package de.abbaddie.bommel.data;

import java.util.ArrayList;
import java.util.List;

/**
 * Eine Streckenfahrt eines echten Zuges.<br />
 * Sie ist spezifiert durch:<ul>
 * <li>den Zug,</li>
 * <li>die Strecke,</li>
 * <li>das Abfahrtszeitintervall,</li>
 * <li>das Leistungsprofil und</li>
 * <li>(ab AS2) die Mindesthaltezeit.</li>
 * </ul>
 * Die konkret errechnete Abfahrtszeit wird nicht hier gespeichert, sondern von {@link TripPlanning} verwaltet.
 *
 * @see Train
 * @see de.abbaddie.bommel.data.TripPlanning
 */
public final class RealTrip implements Trip {
	private Train train;
	private Section section;
	private int allowedDepartureEarliest, allowedDepartureLatest;
	private int dwell;
	private RawPowerProfile rawPowerProfile;

	@Override
	public int hashCode() {
		return train.getId();
	}

	@Override
	public int[] getAllowedDepartures() {
		return TripSupport.getAllowedDepartures(this);
	}

	@Override
	public boolean affects(TripPlanning planning, TripInformation other) {
		return TripSupport.affects(this, other, planning);
	}

	@Override
	public boolean collides(int thisDeparture, Trip other, int otherDeparture) {
		if (!(other instanceof RealTrip))
			return other.collides(otherDeparture, this, thisDeparture);

		RealTrip otherR = (RealTrip)other;

		if (this.getTrain() == otherR.getTrain()) {
			//noinspection UnnecessaryLocalVariable
			int a1 = thisDeparture;
			int a2 = a1 + this.getOccupancyTime();

			//noinspection UnnecessaryLocalVariable
			int b1 = otherDeparture;
			int b2 = b1 + otherR.getOccupancyTime();

			if ((a1 <= b1 && b1 < a2) || (a1 < b2 && b2 <= a2) ||
					(b1 <= a1 && a1 < b2) || (b2 < a2 && a2 <= b2))
				return true;
		}

		return false;
	}

	private int indexInTrainCache = -1;

	private int cachedIndexInParentalTrain() {
		if (indexInTrainCache == -1)
			indexInTrainCache = train.getTrips().indexOf(this);

		return indexInTrainCache;
	}

	@Override
	public List<Constraint> constraints(Trip other) {
		if (!(other instanceof RealTrip))
			return other.constraints(this);

		RealTrip otherR = (RealTrip)other;

		List<Constraint> result = new ArrayList<>();

		if (this.getTrain() == otherR.getTrain()) {
			int myIdx = this.cachedIndexInParentalTrain();
			int otherIdx = otherR.cachedIndexInParentalTrain();

			if (myIdx == -1 || otherIdx == -1)
				throw new RuntimeException("This cannot happen.");

			if (myIdx == otherIdx - 1 && this.getAllowedDepartureLatest() + this.getOccupancyTime() > otherR.getAllowedDepartureEarliest())
				result.add(new Constraint(this, this.getOccupancyTime(), otherR, 0));

			if (myIdx == otherIdx + 1 && otherR.getAllowedDepartureLatest() + otherR.getOccupancyTime() > this.getAllowedDepartureEarliest())
				result.add(new Constraint(otherR, otherR.getOccupancyTime(), this, 0));
		}

		return result;
	}

	/**
	 * Gibt den zugeordneten Zug zurück.
	 *
	 * @return Zug
	 */
	public Train getTrain() {
		return train;
	}

	/**
	 * Setzt den zugeordneten Zug. Sollte nur von {@link de.abbaddie.bommel.util.InstParser} aufgerufen werden!
	 *
	 * @param train Zug
	 */
	public void setTrain(Train train) {
		this.train = train;
	}

	/**
	 * Gibt die zugeordnete Strecke zurück. Sollte nur von {@link de.abbaddie.bommel.util.InstParser} aufgerufen werden!
	 *
	 * @return Strecke
	 */
	public Section getSection() {
		return section;
	}

	/**
	 * Setzt die zugeorndete Strecke. Sollte nur von {@link de.abbaddie.bommel.util.InstParser} aufgerufen werden!
	 *
	 * @param section Strecke
	 */
	public void setSection(Section section) {
		this.section = section;
	}

	public int getAllowedDepartureEarliest() {
		return allowedDepartureEarliest;
	}

	/**
	 * Setzt die frühestmögliche Abfahrtszeit. Sollte nur von {@link de.abbaddie.bommel.util.InstParser} aufgerufen werden!
	 *
	 * @param allowedDepartureEarliest Abfahrtszeit
	 */
	public void setAllowedDepartureEarliest(int allowedDepartureEarliest) {
		this.allowedDepartureEarliest = allowedDepartureEarliest;
	}

	public int getAllowedDepartureLatest() {
		return allowedDepartureLatest;
	}

	/**
	 * Setzt die frühestmögliche Abfahrtszeit. Sollte nur von {@link de.abbaddie.bommel.util.InstParser} aufgerufen werden!
	 *
	 * @param allowedDepartureLatest Abfahrtszeit
	 */
	public void setAllowedDepartureLatest(int allowedDepartureLatest) {
		this.allowedDepartureLatest = allowedDepartureLatest;
	}

	/**
	 * Gibt das rohe Leistungsprofil zurück.
	 *
	 * @return Leistungsprofil.
	 */
	public RawPowerProfile getRawPowerProfile() {
		return rawPowerProfile;
	}

	/**
	 * Setzt das rohe Leistungsprofil. Sollte nur von {@link de.abbaddie.bommel.util.InstParser} aufgerufen werden!
	 *
	 * @param rawPowerProfile Leistungsprofil
	 */
	public void setRawPowerProfile(RawPowerProfile rawPowerProfile) {
		this.rawPowerProfile = rawPowerProfile;
	}

	/**
	 * Setzt die Mindesthaltezeit. Sollte nur von {@link de.abbaddie.bommel.util.InstParser} aufgerufen werden!
	 *
	 * @param dwell Mindesthaltezeit.
	 */
	public void setDwell(int dwell) {
		this.dwell = dwell;
	}

	@Override
	public int getDwell() {
		return dwell;
	}

	@Override
	public int getJourneytime() {
		return rawPowerProfile.getLength() - 1;
	}

	@Override
	public int getOccupancyTime() {
		return getDwell() + getJourneytime();
	}

	@Override
	public TripPowerProfile getPowerProfile(int departure) {
		return new RealTripPowerProfile(this, departure);
	}

	@Override
	public String toString() {
		return "RealTrip:[" + train.toString() + "/" + section.toString() + "]";
	}
}
