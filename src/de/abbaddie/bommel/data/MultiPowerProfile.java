/*
 * Copyright (c) 2014, Florian Jung, Benjamin Miller, Johannes Thürauf, David Wägner
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package de.abbaddie.bommel.data;

/**
 * Ein Leistungsprofil, welches (iterativ) mehrere Leistungsprofile aufaddiert.
 * <br />
 * Wird in {@link MutableTripPlanning} genutzt.
 */
public class MultiPowerProfile implements PlanningPowerProfile {
	private Instance instance;

	private float[] profile;
	private int maxTime;

	/**
	 * Erstellt ein neues {@link MultiPowerProfile}
	 *
	 * @param instance Probleminstanz
	 * @param trips    Menge an Zügen mit Abfahrtszeiten
	 */
	public MultiPowerProfile(Instance instance, Iterable<TripPlanning.Entry> trips) {
		this.instance = instance;

		int end = instance.getSchedulingInterval();
		profile = new float[end];

		for (TripPlanning.Entry entry : trips) {
			Trip trip = entry.getTrip();
			int departure = entry.getDeparture();
			PowerProfile prof = trip.getPowerProfile(departure);
			float[] profile2 = prof.getArray();
			int offset = prof.getStart();

			int end2 = Math.min(profile2.length, profile.length - offset);
			for (int i = 0; i < end2; i++) {
				profile[i + offset] += profile2[i];
			}
		}

		int maxTime = 0;
		float maxVal = profile[0];
		for (int i = 1; i < profile.length; i++) {
			if (profile[i] > maxVal) {
				maxTime = i;
				maxVal = profile[i];
			}
		}
		this.maxTime = maxTime;
	}

	@Override
	public float getPowerAt(int time) {
		if (time < 0 || time >= profile.length) return 0;
		return profile[time];
	}

	@Override
	public int getMaximumTime() {
		return maxTime;
	}

	@Override
	public int getMinimumTime() {
		throw new UnsupportedOperationException();
	}

	@Override
	public int getStart() {
		return 0;
	}

	@Override
	public int getEnd() {
		return instance.getSchedulingInterval();
	}

	@Override
	public float[] getArray() {
		return profile;
	}

}
