/*
 * Copyright (c) 2014, Florian Jung, Benjamin Miller, Johannes Thürauf, David Wägner
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package de.abbaddie.bommel.data;

import java.util.*;

/**
 * Eine veränderliche Planungsimplementierung (verwendet intern {@link java.util.Map}).
 * Zeichnet sich v.a. durch einen schnellen Zugriff/schnelles Schreiben der Abfahrtszeiten aus.
 * Einmal berechnete Leistungsprofile können nicht wiederverwendet werden, sobald auch nur irgendein Wert verändert wurde.
 *
 * @see LinkedTripPlanning
 */
public final class MutableTripPlanning implements TripPlanning {
	private final Instance instance;
	private Map<Trip, Integer> map = new HashMap<>();
	private PlanningPowerProfile profile;

	/**
	 * Erstellt ein leeres {@link MutableTripPlanning}.
	 *
	 * @param instance Probleminstanz
	 */
	public MutableTripPlanning(Instance instance) {
		this.instance = instance;
	}

	/**
	 * Erstellt ein {@link MutableTripPlanning} als Kopie einer bestehenden Planung.
	 *
	 * @param other Andere Planung
	 */
	public MutableTripPlanning(TripPlanning other) {
		this.instance = other.getInstance();

		for (Entry entry : other) {
			map.put(entry.getTrip(), entry.getDeparture());
		}
	}

	@Override
	public Instance getInstance() {
		return instance;
	}

	@Override
	public TripPlanning withDeparture(Trip trip, int departure) {
		if (departure < trip.getAllowedDepartureEarliest())
			throw new IllegalArgumentException(trip.toString() + " has illegal departure (-" + Integer.toString(trip.getAllowedDepartureEarliest() - departure) + ")!");
		if (departure > trip.getAllowedDepartureLatest())
			throw new IllegalArgumentException(trip.toString() + " has illegal departure (+" + Integer.toString(departure - trip.getAllowedDepartureLatest()) + ")!");

		map.put(trip, departure);
		profile = null;

		return this;
	}

	@Override
	public int getDeparture(Trip trip) {
		Integer res = map.get(trip);
		if (res == null) return INVALID_DEPARTURE;
		return res;
	}

	@Override
	public TripPlanning retain(Set<Trip> trips) {
		map.keySet().retainAll(trips);
		profile = null;

		return this;
	}

	@Override
	public PlanningPowerProfile getPowerProfile() {
		if (profile == null) {
			profile = new MultiPowerProfile(getInstance(), this);
		}

		return profile;
	}

	@Override
	public PlanningPowerProfile getPowerProfileMaximumOnly() {
		return getPowerProfile();
	}

	@Override
	public Iterator<Entry> iterator() {
		return new Iterator<Entry>() {
			Iterator<Map.Entry<Trip, Integer>> childIter = map.entrySet().iterator();

			@Override
			public boolean hasNext() {
				return childIter.hasNext();
			}

			@Override
			public Entry next() {
				Map.Entry<Trip, Integer> mapEntry = childIter.next();

				return new Entry(mapEntry.getKey(), mapEntry.getValue());
			}

			@Override
			public void remove() {
				throw new UnsupportedOperationException();
			}

		};
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || !(o instanceof TripPlanning)) return false;

		TripPlanning that = (TripPlanning)o;

		return asMap().equals(that.asMap());
	}

	@Override
	public int hashCode() {
		throw new UnsupportedOperationException();
	}

	@Override
	public String toString() {
		return "TODO";
	}

	@Override
	public Map<Trip, Integer> asMap() {
		return Collections.unmodifiableMap(map);
	}
}
