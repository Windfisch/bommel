/*
 * Copyright (c) 2014, Florian Jung, Benjamin Miller, Johannes Thürauf, David Wägner
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package de.abbaddie.bommel.data;


import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/**
 * Ein einzelner Knoten der verkettenen Liste, welche die Werte von {@link LinkedTripPlanning} speichert.
 */
public class LinkedTripPlanningNode {
	private final Trip myTrip;
	private final int myDeparture;
	private final LinkedTripPlanningNode next;

	private BinaryPowerProfile powerProfile;

	/**
	 * Erstellt das „Ende“ einer verketteten Liste.
	 * Schöner wäre eine polymorphe Implementierung mittels Kompositum, darauf wird hier aber aus Optimierungsgründen verzichtet. (Polymorphie kostet)
	 */
	public LinkedTripPlanningNode() {
		myTrip = null;
		myDeparture = TripPlanning.INVALID_DEPARTURE;
		next = null;
	}

	/**
	 * Erstellt ein neues Glied der verketteten Liste.
	 *
	 * @param myTrip      Streckenfahrt dieses Glieds
	 * @param myDeparture Abfahrtszeit der Streckenfahrt
	 * @param next        Nächstes Glied
	 */
	public LinkedTripPlanningNode(Trip myTrip, int myDeparture, LinkedTripPlanningNode next) {
		this.myTrip = myTrip;
		this.myDeparture = myDeparture;
		this.next = next;
	}

	/**
	 * Ersetzt den Wert zum Trip, evtl. rekursiv. Wird weiter hinten ersetzt, wird genau dort ein neues Glied erzeugt
	 * und für alle weiter vorne liegenden ein neues erzeugt, damit die alte Kette erhalten bleibt und die hinteren
	 * Glieder wiederverwendet werden können (inkl. ihrer Leistungsprofile!). <br />
	 * Gibt entweder ein neues Objekt zurück (falls irgendwo ersetzt wurde), oder <code>null</code> (falls nichts ersetzt wurde)
	 *
	 * @param trip Streckenfahrt
	 * @param departure Abfahrtszeit
	 * @return Knoten der aktuellen ersetzt; oder <code>null</code>, falls keine Veränderung
	 */
	public LinkedTripPlanningNode update(Trip trip, int departure) {
		if (myTrip == trip) { // Annahme: == ist ok, sonst halt equals()
			return new LinkedTripPlanningNode(trip, departure, next);
		}
		if (next != null) {
			LinkedTripPlanningNode result = next.update(trip, departure);

			if (result != null) {
				return new LinkedTripPlanningNode(myTrip, myDeparture, result);
			}
		}
		return null;
	}

	/**
	 * Sucht nach einem Abfahrtswert. Wird er gefunden, wird er zurückgegeben, sonst wird
	 * {@link de.abbaddie.bommel.data.TripPlanning#INVALID_DEPARTURE} zurückgegeben.<br />
	 * Schöner wäre das Werfen einer Ausnahme, aber darauf wird aus Effizienzgründen verzichtet.
	 * 
	 * @param trip Fahrt
	 * @return Abfahrtszeit; Evtl. {@link de.abbaddie.bommel.data.TripPlanning#INVALID_DEPARTURE}.   
	 */
	public int get(Trip trip) {
		if (myTrip == trip) {
			return myDeparture;
		}
		if (next != null) {
			return next.get(trip);
		}
		return TripPlanning.INVALID_DEPARTURE;
	}

	/**
	 * Belässt nur die übergebenen Streckenfahrten in der Liste.<br />
	 * Das Prinzip ist ähnlich wie bei {@link #update(Trip, int)}: Wird weder am aktuellen noch an den hinteren Gliedern
	 * was verändert, wird nichts verändert. Sonst wird genau am hintersten Punkt zuerst ein neues Glied erstellt,
	 * woraufhin auch alle weiter vorne liegenden Glieder neu erstellt werden müssen (damit die alte Liste unverändert
	 * bleibt). Weiter hinten liegende Glieder werden wiederverwendet.
	 * D.h. findet tatsächlich keine Einschränkung statt, bleibt die Liste unverändert.
	 *
	 * @param trips Zu behaltende Streckenfahrten
	 * @return Das Glied, welches an Stelle des aktuellen tritt (ggfs. <code>this</code>)
	 */
	public LinkedTripPlanningNode retain(Set<Trip> trips) {
		if (next == null) {
			return this;
		}
		if (!trips.contains(myTrip)) {
			return next.retain(trips);
		}
		LinkedTripPlanningNode newNext = next.retain(trips);
		if (newNext == next) {
			return this;
		}
		return new LinkedTripPlanningNode(myTrip, myDeparture, newNext);
	}

	/**
	 * Gibt das Leistungsprofil für alle Streckenfahrten, welche sich aber hier in der Liste befinden, zurück.<br />
	 * Dieses wird nach Erstellung gespeichert, damit es in Zukunft schnell abrufbar.
	 *
	 * @param instance Probleminstanz
	 * @return Leistungsprofil
	 */
	public BinaryPowerProfile getPowerProfile(Instance instance) {
		if (powerProfile == null) {
			if (next == null) {
				powerProfile = BinaryPowerProfile.EMPTY;
			} else {
				powerProfile = new BinaryPowerProfile(instance, next.getPowerProfile(instance), myTrip.getPowerProfile(myDeparture), false);
			}
		}
		return powerProfile;
	}


	/**
	 * Gibt das Leistungsprofil für Maximumsanalyse für alle Streckenfahrten, welche sich aber hier in der Liste
	 * befinden, zurück. <br />
	 * Dieses wird nach Erstellung gespeichert, damit es in Zukunft schnell abrufbar.
	 *
	 * @param instance Probleminstanz
	 * @return Leistungsprofil
	 */
	public BinaryPowerProfile getPowerProfileMaximumOnly(Instance instance) {
		if (powerProfile == null) {
			if (next == null) {
				return BinaryPowerProfile.EMPTY;
			} else {
				return new BinaryPowerProfile(instance, next.getPowerProfile(instance), myTrip.getPowerProfile(myDeparture), true);
			}
		}
		return powerProfile;
	}

	/**
	 * Gibt einen Iterator zurück, der diesen und alle folgenden Knoten durchläuft.
	 *
	 * @return Iterator
	 */
	public Iterator<TripPlanning.Entry> iterator() {
		return new Iterator<TripPlanning.Entry>() {
			private LinkedTripPlanningNode current = LinkedTripPlanningNode.this;

			@Override
			public boolean hasNext() {
				return current.myTrip != null;
			}

			@Override
			public TripPlanning.Entry next() {
				TripPlanning.Entry e = new TripPlanning.Entry(current.myTrip, current.myDeparture);

				current = current.next;

				return e;
			}

			@Override
			public void remove() {
				throw new UnsupportedOperationException();
			}
		};
	}

	/**
	 * Gibt das ganze als Map zurück.
	 * Nur für den internen Gebrauch! ;-)
	 */
	Map<Trip, Integer> asMap() {
		Map<Trip, Integer> map;
		if (next != null) {
			map = next.asMap();
		} else {
			map = new HashMap<>();
		}
		map.put(myTrip, myDeparture);

		return map;
	}

	@Override
	public String toString() {
		return ", " + (myTrip != null ? (myTrip.toString() + "=" + myDeparture) : "") + (next != null ? next.toString() : "");
	}
}
