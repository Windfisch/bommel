/*
 * Copyright (c) 2014, Florian Jung, Benjamin Miller, Johannes Thürauf, David Wägner
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package de.abbaddie.bommel.data;

import java.util.*;

/**
 * Beschreibt alle Parameter der gegebenen Problemdaten.
 */
// TODO: InstanceBuilder
public class Instance {
	private int schedulingInterval;

	private List<Train> trains = new ArrayList<>();

	private List<Trip> trips = new ArrayList<>();
	private List<Trip> returnTrips = Collections.unmodifiableList(trips); // Optimierung ermöglichen in MostExtremeTripSelectionStrategy

	private Set<Section> sections = new LinkedHashSet<>();

	private ExtensionLevel level = ExtensionLevel.AS1;

	private TripPlanning currentSolution;

	/**
	 * Gibt die Länge des Planungsintervalls in Zehntelsekunden zurück.
	 *
	 * @return Intervalllänge
	 */
	public int getSchedulingInterval() {
		return schedulingInterval;
	}

	/**
	 * Setzt die Länge des Planungsintervalls. Sollte nur von {@link de.abbaddie.bommel.util.InstParser} aufgerufen werden!
	 *
	 * @param schedulingInterval Länge
	 */
	public void setSchedulingInterval(int schedulingInterval) {
		this.schedulingInterval = schedulingInterval;
	}

	/**
	 * Fügt eine Streckenfahrt hinzu. Sollte nur von {@link de.abbaddie.bommel.util.InstParser} aufgerufen werden!
	 *
	 * @param trip Streckenfahrt
	 */
	public void addTrip(Trip trip) {
		trips.add(trip);
	}

	/**
	 * Fügt einen Zug hinzu. Sollte nur von {@link de.abbaddie.bommel.util.InstParser} aufgerufen werden!
	 *
	 * @param train Zug
	 */
	public void addTrain(Train train) {
		trains.add(train);
	}

	/**
	 * Fügt eine Strecke hinzu. Sollte nur von {@link de.abbaddie.bommel.util.InstParser} aufgerufen werden!
	 *
	 * @param section Strecke.
	 */
	public void addSection(Section section) {
		sections.add(section);
	}

	/**
	 * Gibt die Streckenfahrten der Instanz zurück.
	 *
	 * @return Streckenfahrten
	 */
	public List<Trip> getTrips() {
		return returnTrips;
	}

	/**
	 * Gibt die Züge der Instanz zurück.
	 *
	 * @return Züge
	 */
	public List<Train> getTrains() {
		return Collections.unmodifiableList(trains);
	}

	/**
	 * Gibt eine Strecke anhand ihrer Id zurück. Sollte nur von {@link de.abbaddie.bommel.util.InstParser} aufgerufen werden!
	 *
	 * @param id Id
	 * @return Strecke. <code>null</code>, falls nicht gefunden.
	 */
	public Section getSectionById(int id) {
		for (Section s : sections) {
			if (s.getId() == id)
				return s;
		}

		return null;
	}


	/**
	 * Gibt einen Zug anhand seiner Id zurück. Sollte nur von {@link de.abbaddie.bommel.util.InstParser} aufgerufen werden!
	 *
	 * @param id Id
	 * @return Strecke. <code>null</code>, falls nicht gefunden.
	 */
	public Train getTrainById(int id) {
		for (Train t : trains) {
			if (t.getId() == id)
				return t;
		}

		return null;
	}


	/**
	 * Gibt eine Streckenfahrt anhand Zug und Strecke zurück. Sollte nur von {@link de.abbaddie.bommel.util.InstParser} aufgerufen werden!
	 *
	 * @param train   Zug
	 * @param section Strecke
	 * @return Streckenfahrt. <code>null</code>, falls nicht gefunden.
	 */
	public RealTrip getTripByTrainAndSection(Train train, Section section) {
		for (Trip t_ : trips) {
			RealTrip t = (RealTrip)t_;

			if (t.getTrain() == train && t.getSection() == section)
				return t;
		}

		return null;
	}

	/**
	 * Gibt die Ausbaustufe zurück.
	 *
	 * @return Ausbaustufe
	 */
	public ExtensionLevel getLevel() {
		return level;
	}

	/**
	 * Setzt die verwendete Ausbaustufe. Sollte nur von {@link de.abbaddie.bommel.util.InstParser} aufgerufen werden!
	 *
	 * @param level Ausbaustufe
	 */
	public void setLevel(ExtensionLevel level) {
		this.level = level;
	}

	/**
	 * Gibt die aktuell beste Lösung zurück.
	 *
	 * @return Lösung
	 */
	public TripPlanning getCurrentSolution() {
		return currentSolution;
	}

	/**
	 * Setzt die aktuell beste Lösung.
	 *
	 * @param currentSolution Lösung
	 */
	public void setCurrentSolution(TripPlanning currentSolution) {
		this.currentSolution = currentSolution;
	}

	/**
	 * Überprüft, ob eine Lösung gültig ist. Es wird auf <i>syserr</i> eine Meldung ausgegeben, wenn die Lösung gültig ist.
	 * Wenn sie ungültig ist, werden die ersten 10 Fehler ausgegeben.
	 *
	 * @param p Lösung
	 * @return Wert, ob die Lösung gültig ist.
	 */
	public boolean verifyPlanning(TripPlanning p) {
		if (verifyPlanning(p, "Die Lösung ist ungültig: ", true, 10)) {
			System.err.println("Die Lösung ist gültig.");
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Überprüft, ob eine Lösung gültig ist.
	 * Wenn sie ungültig ist, können Fehler auf <i>syserr</i> ausgegeben werden.
	 *
	 * @param planning   Die zu prüfende Lösung
	 * @param prefix     Text, der vor jede Zeile der Fehlerausgabe geschrieben werden soll.
	 * @param output     Wert, ob die Fehlerausgabe erfolgen soll.
	 * @param maxIllegal Maximale Anzahl von Fehlern, die ausgegeben werden soll.
	 * @return Wert, ob die Lösung gültig ist.
	 */
	public boolean verifyPlanning(TripPlanning planning, String prefix, boolean output, int maxIllegal) {
		int n_illegal = 0;

		// Teste auf Legalität
		for (Trip trip : trips) {
			boolean illegal = false;
			int departure = planning.getDeparture(trip);

			// AS1
			if (departure == TripPlanning.INVALID_DEPARTURE) {
				if (n_illegal < maxIllegal && output)
					System.err.println(prefix + "Abfahrtszeit für " + trip + " ist nicht enthalten.");
				illegal = true;
			} else {
				if (departure < trip.getAllowedDepartureEarliest()) {
					if (n_illegal < maxIllegal && output)
						System.err.println(prefix + "Abfahrtszeit für " + trip + " ist zu früh (" + departure + ", frühesterlaubt: " + trip.getAllowedDepartureEarliest() + ")");
					illegal = true;
				}
				if (departure > trip.getAllowedDepartureLatest()) {
					if (n_illegal < maxIllegal && output)
						System.err.println(prefix + "Abfahrtszeit für " + trip + " ist zu spät (" + departure + ", spätesterlaubt: " + trip.getAllowedDepartureLatest() + ")");
					illegal = true;
				}
				if (departure % 600 != 0) {
					if (n_illegal < maxIllegal && output)
						System.err.println(prefix + "Abfahrtszeit für " + trip + " ist nicht zu Minutenanfang (" + departure + ")");
					illegal = true;
				}
			}

			// AS2
			List<Trip> otherInTrain = ((RealTrip)trip).getTrain().getTrips();
			int ourIdx = otherInTrain.indexOf(trip);

			if (ourIdx > 0) {
				Trip before = otherInTrain.get(ourIdx - 1);
				if (planning.getDeparture(before) + before.getOccupancyTime() > departure) {
					if (n_illegal < maxIllegal && output)
						System.err.println(prefix + "Abfahrtszeit für " + trip + " überschneidet sich mit Vorgängerfahrt " + before + ". Geplant: " + departure + ", frühestmöglich: " + (planning.getDeparture(before) + before.getOccupancyTime()));
					illegal = true;
				}
			}

			if (illegal)
				n_illegal++;
		}

		if (n_illegal > maxIllegal && output)
			System.err.println("\t\t(und " + (n_illegal - maxIllegal) + " weitere Fehler)");

		return n_illegal == 0;
	}

	/**
	 * Die Ausbaustufe. Momentan AS1 und AS2. AS3 wäre ggf. nachrüstbar.
	 */
	public enum ExtensionLevel {
		AS1, AS2
	}
}
