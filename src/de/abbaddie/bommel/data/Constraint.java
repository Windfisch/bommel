/*
 * Copyright (c) 2014, Florian Jung, Benjamin Miller, Johannes Thürauf, David Wägner
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package de.abbaddie.bommel.data;

/**
 * Ein Constraint setzt mehrere Streckenfahrten eines Zuges in Verbindung (nur für AS2 relevant).<br />
 * <br />
 * Genauer gesagt ist es eine Ungleichung der Form:<br />
 * <i>leftTrip.departure + leftOffset &lt;= rightTrip.departure + rightOffset</i>.<br />
 * <br />
 * Gespeichert werden nur die Streckenfahrten sowie die Differenz <i>rightOffset - leftOffset</i>, also eigentlich:<br />
 * <i>leftTrip.departure &lt;= rightTrip.departure + (rightOffset - leftOffset)</i>.
 */
public class Constraint {
	private final Trip leftTrip;
	private final Trip rightTrip;
	private final int rightOffsetMinusLeftOffset;

	/**
	 * Erstellt {@link Constraint}.
	 *
	 * @param leftTrip    Eher verkehrende Streckenfahrt
	 * @param leftOffset  Notwendige Zeitlücke zwischen den beiden Abfahrten
	 * @param rightTrip   Später verkehrende Streckenfahrt
	 * @param rightOffset Später
	 */
	public Constraint(Trip leftTrip, int leftOffset, Trip rightTrip, int rightOffset) {
		this.leftTrip = leftTrip;
		this.rightTrip = rightTrip;
		this.rightOffsetMinusLeftOffset = rightOffset - leftOffset;
	}

	/**
	 * Gibt die eher verkehrende Streckenfahrt zurück.
	 *
	 * @return Eher verkehrende Streckenfahrt
	 */
	public Trip getLeftTrip() {
		return leftTrip;
	}

	/**
	 * Gibt die später verkehrende Streckenfahrt zurück.
	 *
	 * @return Später verkehrende Streckenfahrt
	 */
	public Trip getRightTrip() {
		return rightTrip;
	}

	/**
	 * Gibt <i>rightOffset - leftOffset</i> zurück.
	 *
	 * @return Gibt obigen Wert zurück.
	 */
	public int rightOffset() {
		return rightOffsetMinusLeftOffset;
	}

	/**
	 * Gibt 0 zurück. (Flo? FLO?!)
	 *
	 * @return 0
	 */
	public int leftOffset() {
		return 0;
	}

	/**
	 * Wertet aus, ob der Constraint noch erfüllbar ist, wenn die Abfahrt des eher verkehrenden Zuges fixiert wurde und
	 * nur noch gewisse Abfahrtszeiten für den später verkehrenden frei wählbar sind.
	 *
	 * @param leftDeparture Fixierte Abfahrtszeit des eher verkehrenden Zuges
	 * @param rightTripInfo Fahrtinformation des später verkehrenden Zuges
	 * @return Wert, ob der Constraint noch erfüllbar ist.
	 */
	public boolean fulfillable(int leftDeparture, TripInformation rightTripInfo) {
		int rightLatestDeparture = rightTripInfo.getConsideredDepartures()[rightTripInfo.getConsideredDepartures().length - 1];
		return (leftDeparture <= rightLatestDeparture + rightOffsetMinusLeftOffset);
	}


	/**
	 * Wertet aus, ob der Constraint noch erfüllbar ist, wenn die Abfahrt des später verkehrenden Zuges fixiert wurde und
	 * nur noch gewisse Abfahrtszeiten für den eher verkehrenden frei wählbar sind.
	 *
	 * @param leftTripInfo   Fahrtinformation des eher verkehrenden Zuges
	 * @param rightDeparture Fixierte Abfahrtszeit des später verkehrenden Zuges
	 * @return Wert, ob der Constraint noch erfüllbar ist.
	 */
	public boolean fulfillable(TripInformation leftTripInfo, int rightDeparture) {
		int leftEarliestDeparture = leftTripInfo.getConsideredDepartures()[0];
		return (leftEarliestDeparture <= rightDeparture + rightOffsetMinusLeftOffset);
	}

	@Override
	public String toString() {
		return "Constraint: " + leftTrip + ".departure() < " + rightTrip + ".departure() + " + rightOffsetMinusLeftOffset;
	}
}
